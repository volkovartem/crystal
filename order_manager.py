import time
import utils
import traceback
import trade_utils
import restWrapper as wp
from database.services.arbitrage import *
from database.services.logging import *

# settings
expiration_time = 20  # seconds


def allow_premoving(data):
    result = True
    pref_coins = utils.get_preffered() + utils.get_history()
    for exchange in list(data.keys())[:-1]:
        if data[exchange][0] not in pref_coins and data[exchange][-1] not in pref_coins:
            result = False
    return result


def calc_profit(results, tokens, achecking):
    chain_type = len(results)
    if chain_type == 1:
        return utils.round_dwn(results[0]['amount'] - achecking[1], 6)
    client = Client(('127.0.0.1', 11211))
    params = {'exchanges': [], 'amounts': [], 'wfees': [], 'coeffs': []}
    for x in range(len(results)):
        exchange = results[x]['trades'][0]['exchange']
        params['exchanges'].append(exchange)
        params['amounts'].append(achecking[3][exchange])
        params['amounts'].append(results[x]['amount'])
        params['wfees'].append(utils.get_fee_fast(exchange, tokens[x]))
        if x > 0:
            rm = client.get(exchange + 'Markets')
            params['coeffs'].append(rm) if rm is None else params['coeffs'].append(
                trade_utils.convert(params['exchanges'][x - 1], tokens[x - 1], tokens[x], json.loads(rm.decode())))

    for param in params:
        for x in params[param]:
            if x is None:
                return

    profit = 0
    ii = 0
    for i in range(1, chain_type + 1):
        a = params['amounts'][i + ii]
        w = params['wfees'][i - 1]
        b = params['amounts'][0] if i == chain_type else params['amounts'][i + ii + 1]
        profit += a - w - b
        ii += 1
    return utils.round_dwn(profit, 6)


def statement(gross, _mark, stake, fees, profit, token, _op, wfees, status, trades):
    description = ('Chain = ' + _mark).replace("'", "").replace('[', '').replace(']', '').replace(', ', ' ')

    _data = {'gross': gross,
             'stake': stake,
             'fees': str(fees),
             'wfees': wfees,
             'profit': str(profit) + ' ' + token,
             'trades1': trades,
             'trades2': []}

    opid = _op + str(int(time.time()))
    print(opid, description, json.dumps(_data), utils.get_time(), status)
    add_arbitrage(opid, description, json.dumps(_data), utils.get_time(), status)


def check_status(exchange, orderId, symbol):
    for x in range(expiration_time):
        status = wp.get_order_status(exchange, orderId, symbol)
        if status == 'closed' or status == 'filled':
            return True
        time.sleep(1)
    return False


def get_wfees(exchange1, token1, exchange2, token2, exchange3='', token3=''):
    wfee1 = utils.get_fee_fast(exchange1, token1)
    wfee2 = utils.get_fee_fast(exchange2, token2)
    if exchange3 != '' and token3 != '':
        wfee3 = utils.get_fee_fast(exchange3, token3)
        if wfee3 is not None:
            return '{} {} & {} {} & {} {}'.format(wfee1, token1, wfee2, token2, wfee3, token3)
    else:
        if wfee1 is not None and wfee2 is not None:
            return '{} {} & {} {}'.format(wfee1, token1, wfee2, token2)
    return 'None'


def get_amount(data, mode=False, percent=False):
    type = len(data) - 1
    params = {'exchange': [], 'token': [], 'wfee': [], 'coeff': [], 'limit': [], 'hw': []}
    for exchange in list(data.keys())[:-1]:
        wfee = utils.get_fee_fast(exchange, data[exchange][-1])
        coeff = trade_utils.calc_coeff_chain(exchange, data[exchange])
        limit = trade_utils.get_amount_limits(exchange, data[exchange][0])
        print('limit', limit, data[exchange][0])

        if limit is None or coeff is None or wfee is None:
            # log('Traceback ' + '  ' + exchange + data[exchange][0] + '  ' + json.dumps(data))
            return
        hw = trade_utils.get_amount_wallet(exchange, data[exchange][0], percent=percent)
        params['exchange'].append(exchange)
        params['token'].append(data[exchange][0])
        params['wfee'].append(wfee)
        params['coeff'].append(coeff)
        params['limit'].append(limit)
        if mode and params['hw'] != []:
            params['hw'].append(limit['amount_min'])
        else:
            params['hw'].append(hw)
    for param in params:
        for x in params[param]:
            if x is None:
                return
    minXMR = params['limit'][-1]['amount_min']
    print('min ' + params['token'][-1], minXMR)
    print(params['hw'][-1], '>=', minXMR)
    if params['hw'][-1] >= minXMR:
        maxAmountVEE = (params['hw'][-1] + params['wfee'][-2]) / params['coeff'][-2]
        print('maxAmount' + params['token'][-2], maxAmountVEE)
        minAmountVEE = (minXMR + params['wfee'][-2]) / params['coeff'][-2]
        print('minAmount' + params['token'][-2], minAmountVEE)
        print('here')
        # if mode:
        #     hwVEE = minAmountVEE
        if mode:
            if type == 2:
                print('return amount mode', {params['exchange'][0]: utils.round_dwn(minAmountVEE, 5),
                                             params['exchange'][1]: utils.round_dwn(params['hw'][1], 5)})
                return {params['exchange'][0]: utils.round_dwn(minAmountVEE, 5),
                        params['exchange'][1]: utils.round_dwn(params['hw'][1], 5)}
            if type == 3:
                params['hw'][-2] = minAmountVEE
        print(params['hw'][-2], '>=', minAmountVEE)
        if params['hw'][-2] >= minAmountVEE:
            if type == 2:
                return utils.round_dwn(min(maxAmountVEE, params['hw'][-2]), 5)
            maxAmountUSDT = (maxAmountVEE + params['wfee'][-3]) / params['coeff'][-3]
            # print('maxAmount' + params['token'][-3], maxAmountUSDT)
            minAmountUSDT = (minAmountVEE + params['wfee'][-3]) / params['coeff'][-3]
            # print('minAmount' + params['token'][-3], minAmountUSDT)
            if mode:
                return {params['exchange'][0]: utils.round_dwn(minAmountUSDT, 5),
                        params['exchange'][1]: utils.round_dwn(params['hw'][1], 5),
                        params['exchange'][2]: utils.round_dwn(params['hw'][2], 5)}
            if params['hw'][-3] >= minAmountUSDT:
                return utils.round_dwn(min(maxAmountUSDT, params['hw'][-3]), 5)


def simulation(exchange, nodes, amount, mark, single=False):
    try:
        print('simulation started')
        real_prices = {}
        init_amount = amount
        trade_fees_sum = 0

        wfee = 0 if single else utils.get_fee_fast(exchange, nodes[-1])
        print('wfee', exchange, nodes[-1], wfee)
        if wfee is None:
            print('wfee is None', exchange, nodes[-1])
            return

        fees = trade_utils.get_trading_fees(exchange)
        real_prices.update({exchange: []})
        for n in range(len(nodes) - 1):
            symbol = utils.normalize_pair(nodes[n], nodes[n + 1], 'symbol')
            norm_amount = trade_utils.normalize_amount(exchange, amount, nodes[n], nodes[n + 1])
            print('get_real_price', exchange, symbol, norm_amount)

            rp = trade_utils.get_real_price(exchange, symbol, norm_amount)
            if rp is not None:
                rp = rp['realBid'] if symbol == nodes[n] + nodes[n + 1] else rp['realAsk']
                print('real price', rp)
                real_prices[exchange].append(rp)
                price = trade_utils.get_price_universal(exchange, nodes[n], nodes[n + 1], rp)
                print('get_price_universal', price)
                trade_fee = float(fees['sellFees'] if symbol == nodes[n] + nodes[n + 1] else fees['buyFees']) / 100
                trade_fees_sum = trade_fees_sum + trade_fee
                print('trade_fee & trade_fees_sum', trade_fee, trade_fees_sum)
                print('amount (current)', amount)
                amount = amount * (1 - trade_fee)
                print('amount - fee', amount)
                amount = price * amount
                print('amount * price', amount)
            else:
                print_arb_log(['Arbitrage failed', 'Reason: Getting real price failed'], mark)
                return

        print('simulation finished')
        actual_return = float(amount) - float(wfee)
        if single:
            actual_return = actual_return - float(init_amount)
        print('actual return', actual_return)
        return [real_prices, utils.round_dwn(actual_return, 5), trade_fees_sum]

    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


# def writefile(test):
#     test.append(' ')
#     f = open("real.txt", "a+")
#     for n in test:
#         f.write(str(n) + '\n')


def simulation_v2(exchange, nodes, amount, mark, single=False):
    try:
        orderbooks = []
        real_prices = {}
        init_amount = amount
        trade_fees_sum = 0

        wfee = 0 if single else utils.get_fee_fast(exchange, nodes[-1])
        if wfee is None:
            return

        fees = trade_utils.get_trading_fees(exchange)
        real_prices.update({exchange: []})
        for n in range(len(nodes) - 1):
            pair = utils.normalize_pair(nodes[n], nodes[n + 1])
            symbol = pair[1] + pair[2]

            rp = trade_utils.get_real_price_v2(exchange, symbol, amount, pair[0])
            if rp is not None:
                orderbooks.append(rp[1])
                rp = rp[0]
                real_prices[exchange].append(rp)
                price = trade_utils.get_price_universal(exchange, nodes[n], nodes[n + 1], rp)
                trade_fee = float(fees['sellFees'] if pair[0] == 'sell' else fees['buyFees']) / 100
                trade_fees_sum = trade_fees_sum + trade_fee
                amount = amount * (1 - trade_fee)
                amount = price * amount
            else:
                # print_arb_log(['Arbitrage failed', 'Reason: Getting real price failed'], mark)
                return

        actual_return = float(amount) - float(wfee)
        if single:
            if float(init_amount) / 2 > actual_return:
                print('real prices', str(real_prices))
                print('actual_return', actual_return, 'init_amount', init_amount)
            actual_return = actual_return - float(init_amount)

        if utils.round_dwn(actual_return, 6) > 0:
            for o in orderbooks:
                print(o)

        return [real_prices, utils.round_dwn(actual_return, 6), trade_fees_sum]

    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def execute(exchange, nodes, real_prices, amount, mark, response):
    try:
        print(exchange, nodes, real_prices, amount, mark)

        trades = []

        for i in range(len(nodes) - 1):
            pair = utils.normalize_pair(nodes[i], nodes[i + 1])
            symbol = pair[1] + pair[2]
            side = pair[0]
            amount = trade_utils.normalize_amount(exchange, amount, nodes[i], nodes[i + 1], real_prices[i])
            if amount is None:
                print_arb_log(['Arbitrage failed', 'Reason: normalize amount failed ('
                               + str(amount) + ' ' + nodes[i] + ' ' + nodes[i + 1] + ')'], mark)
                response.update({'trades': trades, 'success': False, 'amount': 0})
                return
            print(exchange + ': ' + 'normalize_amount', amount, symbol)
            balance = wp.get_balance(exchange, nodes[i + 1])
            if balance is None:
                print_arb_log(['Arbitrage failed', 'Reason: getting balance failed (' + nodes[i + 1] + ')'], mark)
                response.update({'trades': trades, 'success': False, 'amount': 0})
                return
            print(exchange, symbol, side, amount, real_prices[i])
            orderId = wp.place_order(exchange, symbol, side, amount, real_prices[i])
            orderTime = utils.get_time()
            if orderId is not None and 'Error' not in orderId and 'code' not in orderId:
                print_arb_log(['Order {} is opened'.format(side.upper()), 'ID {}'.format(orderId)], mark)
                time.sleep(.5)
                status = check_status(exchange, orderId, symbol)
                if status:
                    trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
                                   'side': side.upper(), 'status': 'closed', 'time': orderTime})
                    print(exchange + ': ' + 'old balance', balance, nodes[i + 1])
                    new_balance = wp.get_balance(exchange, nodes[i + 1])
                    print(exchange + ': ' + 'new balance', new_balance, nodes[i + 1])
                    balance = new_balance - balance
                    amount = balance
                    print(exchange + ': ' + 'next amount', amount, nodes[i + 1])
                else:
                    corderId = wp.cancel_order(exchange, orderId, symbol)
                    wp.cancel_order(exchange, orderId, symbol)
                    trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
                                   'side': side.upper(), 'status': 'expired', 'time': orderTime})
                    trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': str(corderId), 'amount': amount,
                                   'side': side.upper(), 'status': 'cancel order', 'time': orderTime})
                    print_arb_log(['Arbitrage failed',
                                   'Reason: order is not closed within ' + str(expiration_time) + ' sec'], mark)
                    response.update({'trades': trades, 'success': False, 'amount': 0})
                    return
            else:
                trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': str(orderId), 'amount': amount,
                               'side': side.upper(), 'status': 'failed', 'time': orderTime})
                print_arb_log(['Arbitrage failed', 'Reason: order is not opened'], mark)
                response.update({'trades': trades, 'success': False, 'amount': 0})
                return
        response.update({'trades': trades, 'success': True, 'amount': amount})

    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def execute_v2(exchange, nodes, real_prices, amount, mark, response):
    try:
        print('execute_v2', exchange, nodes, real_prices, amount, mark)
        init_amount = amount

        infos = []
        trades = []

        for i in range(len(nodes) - 1):
            pair = utils.normalize_pair(nodes[i], nodes[i + 1])
            symbol = pair[1] + pair[2]
            side = pair[0]

            balance = float(utils.get_balance_fast(exchange, nodes[i + 1]))
            if balance is None:
                print_arb_log(['Arbitrage failed', 'Reason: getting balance failed (' + nodes[i + 1] + ')'], mark)
                response.update({'trades': trades, 'success': False, 'amount': 0})
                return
            # print(exchange, symbol, side, amount, real_prices[i])
            print('wp.Transfer', exchange, amount, nodes[i], nodes[i + 1])
            orderbook = utils.get_order_book_fast(exchange, symbol)
            print('orderbook', 'time gap', round(time.time() - (float(orderbook['timestamp']) / 1000), 4), 'timestamp',
                  (float(orderbook['timestamp']) / 1000), symbol, 'time now', time.time(), 'amount', amount, orderbook)

            t = time.time()
            info = wp.transfer(exchange, amount, nodes[i], nodes[i + 1])
            print('order execution time', symbol, time.time() - t)

            orderbook = utils.get_order_book_fast(exchange, symbol)
            print('orderbook', 'time gap', round(time.time() - (float(orderbook['timestamp']) / 1000), 4), 'timestamp',
                  (float(orderbook['timestamp']) / 1000), symbol, 'time now', time.time(), 'amount', amount, orderbook)

            print('info', info)
            infos.append(info)
            # {'symbol': 'LTC_ETH', 'balances':
            # {'ETH': 0.0012141218362616, 'LTC': 0.0468210985},
            # 'status': 'filled', 'order_id': 1891924085835776, 'side': 'buy'}

            orderTime = utils.get_time()
            if info is not None and info['status'] != 'failed':
                orderId = info['order_id']
                print_arb_log(['Order {} is opened'.format(side.upper()), 'ID {}'.format(orderId)], mark)
                status = info['status']

                if status == 'filled':
                    trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
                                   'side': side.upper(), 'status': 'closed', 'time': orderTime})
                    # print(exchange + ': ' + 'old balance', balance, nodes[i + 1])
                    if nodes[i + 1] not in info['balances']:
                        new_balance = float(utils.get_balance_fast(exchange, nodes[i + 1]))
                    else:
                        new_balance = float(info['balances'][nodes[i + 1]])
                    # print(exchange + ': ' + 'new balance', new_balance, nodes[i + 1])
                    print('Balance', balance, 'new balance', new_balance,  nodes[i + 1])
                    balance = new_balance - balance
                    amount = balance
                    # print(exchange + ': ' + 'next amount', amount, nodes[i + 1])
                else:
                    trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
                                   'side': side.upper(), 'status': status, 'time': orderTime})
                    response.update({'trades': trades, 'success': False, 'amount': 0})
                    return
            else:
                trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': '-', 'amount': amount,
                               'side': side.upper(), 'status': 'failed', 'time': orderTime})
                print_arb_log(['Arbitrage failed', 'Reason: order failed'], mark)
                response.update({'trades': trades, 'success': False, 'amount': 0})
                return
        print('real prices', str(real_prices))
        actual_prices = []
        for inf in infos:
            actual_prices.append(inf['price'])
        print('actual_prices', str(actual_prices))
        response.update({'trades': trades, 'success': True, 'amount': amount, 'init_amount': init_amount, 'info': infos})

    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())
