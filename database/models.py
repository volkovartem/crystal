from sqlalchemy import Column, Integer, String
from database.database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '<User %r>' % (self.username)

class ApiEx(Base):
    __tablename__ = "apiBinding"
    id = Column(Integer, primary_key=True)
    exchange = Column(String)
    apiKey = Column(String)
    secretKey = Column(String)
    buyFees = Column(String)
    sellFees = Column(String)
    fundPwd = Column(String)
    passPhrase = Column(String)
    status = Column(String)

    def __init__(self, exchange, apiKey, secretKey, buyFees, sellFees, fundPwd, passPhrase, status):
        self.exchange = exchange
        self.apiKey = apiKey
        self.secretKey = secretKey
        self.buyFees = buyFees
        self.sellFees = sellFees
        self.fundPwd = fundPwd
        self.passPhrase = passPhrase
        self.status = status


class Setting(Base):
    __tablename__ = "settings"
    id = Column(Integer, primary_key=True)
    param = Column(String)
    value = Column(String)

    __mapper_args__ = {
        'confirm_deleted_rows': False
    }

    def __init__(self, param, value):
        self.param = param
        self.value = value


# class Trade(Base):
#     __tablename__ = "trades"
#     id = Column(Integer, primary_key=True)
#     exchange = Column(String)
#     pair = Column(String)
#     orderId = Column(String)
#     amount = Column(String)
#     side = Column(String)
#     status = Column(String)
#     time = Column(String)
#     # comment = Column(String)
#
#     def __init__(self, exchange, pair, orderId, amount, side, status, time, comment = ''):
#         self.exchange = exchange
#         self.pair = pair
#         self.orderId = orderId
#         self.amount = amount
#         self.side = side
#         self.status = status
#         self.time = time
#         # self.comment = comment

class Withdrawal(Base):
    __tablename__ = "withdrawals"
    id = Column(Integer, primary_key=True)
    timestamp = Column(String)
    exchange = Column(String)
    amount = Column(String)
    token = Column(String)

    def __init__(self, timestamp, exchange, amount, token):
        self.timestamp = timestamp
        self.exchange = exchange
        self.amount = amount
        self.token = token

class Arbitrage(Base):
    __tablename__ = "arbitrage"
    id = Column(Integer, primary_key=True)
    opid = Column(String)
    description = Column(String)
    data = Column(String)
    time = Column(String)
    status = Column(String)

    def __init__(self, opid, description, data, time, status):
        self.opid = opid
        self.description = description
        self.data = data
        self.time = time
        self.status = status


class Transfer(Base):
    __tablename__ = "transfers"
    id = Column(Integer, primary_key=True)
    description = Column(String)
    token = Column(String)
    amount = Column(String)
    txid = Column(String)
    time = Column(String)
    status = Column(String)

    def __init__(self, description, token, amount, txid, time, status):
        self.description = description
        self.token = token
        self.amount = amount
        self.txid = txid
        self.time = time
        self.status = status


class Log(Base):
    __tablename__ = "log"
    id = Column(Integer, primary_key=True)
    time = Column(String)
    line = Column(String)

    def __init__(self, time, line):
        self.time = time
        self.line = line

class Stats(Base):
    __tablename__ = "stats"
    id = Column(Integer, primary_key=True)
    timestamp = Column(Integer)
    data = Column(String)

    def __init__(self, timestamp, data):
        self.timestamp = timestamp
        self.data = data


class Control(Base):
    __tablename__ = "control"
    id = Column(Integer, primary_key=True)
    command = Column(String)
    text = Column(String)

    def __init__(self, command, text):
        self.command = command
        self.text = text


# class Store(Base):
#     __tablename__ = "store"
#     id = Column(Integer, primary_key=True)
#     text = Column(String)
#
#     def __init__(self, text):
#         self.text = text


