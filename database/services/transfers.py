from database.models import Transfer
from database.database import db_session



def get_transfers():
    data = []
    transfer = db_session.query(Transfer).all()
    for s in transfer:
        str = {
            "id": s.id,
            "description": s.description,
            "token": s.token,
            "amount": s.amount,
            "txid": s.txid,
            "time": s.time,
            "status": s.status
        }
        data.append(str)
    return data


def add_transfer(description, token, amount, txid, time, status):
    sets = Transfer(description, token, amount, txid, time, status)
    db_session.add(sets)
    db_session.commit()

