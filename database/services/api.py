from database.models import ApiEx
from database.database import db_session



def add_api(exchange, apiKey, secretKey, buyFees, sellFees, fundPwd, passPhrase):
    exists = db_session.query(ApiEx).filter_by(exchange=exchange).scalar()
    if exists is not None:
        db_session.delete(exists)
    api = ApiEx(exchange, apiKey, secretKey, buyFees, sellFees, fundPwd, passPhrase, 'False')
    db_session.add(api)
    db_session.commit()


def get_api(exchange=''):
    exists = db_session.query(ApiEx).filter_by(exchange=exchange).scalar()
    if exists is not None:
        return [exists.apiKey, exists.secretKey, exists.buyFees, exists.sellFees, exists.fundPwd, exists.passPhrase,
                exists.status]
    return ['', '', '', '', '', '', '']


def remove_api(exchange):
    exists = db_session.query(ApiEx).filter_by(exchange=exchange).scalar()
    if exists is not None:
        db_session.delete(exists)
        db_session.commit()


def set_status_api(exchange, text):
    exists = db_session.query(ApiEx).filter_by(exchange=exchange).scalar()
    if exists is not None:
        _api = [exists.apiKey, exists.secretKey, exists.buyFees, exists.sellFees, exists.fundPwd, exists.passPhrase,
                exists.status]
        api = ApiEx(exchange, _api[0], _api[1], _api[2], _api[3], _api[4], _api[5], text)
        db_session.delete(exists)
        db_session.add(api)
        db_session.commit()
