import time
from database.models import Withdrawal
from database.database import db_session


def get_withdrawals():
    data = []
    withdrawals = db_session.query(Withdrawal).all()
    for s in withdrawals:
        str = {
            "id": s.id,
            "timestamp": s.timestamp,
            "exchange": s.exchange,
            "amount": s.amount,
            "token": s.token
        }
        data.append(str)
    return data


def add_withdrawal(exchange, amount, token):
    timestamp = str(time.time())
    sets = Withdrawal(timestamp, exchange, amount, token)
    db_session.add(sets)
    db_session.commit()


def remove_withdrawal(id):
    exists = db_session.query(Withdrawal).filter_by(id=id).scalar()
    if exists is not None:
        db_session.delete(exists)
        db_session.commit()
