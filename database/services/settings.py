import json
from database.models import Setting
from database.database import db_session


def add_setting(param, value):
    exists = Setting.query.filter_by(param=param).one()
    if exists is not None:
        exists.value = value
    else:
        db_session.add(Setting(param, value))
    db_session.commit()


def get_settings(param=''):
    data = []
    if param == '':
        sets = db_session.query(Setting).all()
        for s in sets:
            str = {
                "param": s.param,
                "value": s.value
            }
            data.append(str)
    else:
        exists = db_session.query(Setting).filter_by(param=param).scalar()
        if exists is not None:
            str = {
                "param": exists.param,
                "value": exists.value
            }
            data.append(str)
    return data


def remove_setting(param):
    exists = db_session.query(Setting).filter_by(param=param).scalar()
    if exists is not None:
        db_session.delete(exists)
        db_session.commit()


def add_coin(token):
    coins = json.loads(get_settings('coins')[0]['value'])
    if token not in list(x['coin'] for x in coins):
        coins.append({'coin': token, 'withdrawal': False, 'preferred': False})
        add_setting('coins', json.dumps(coins))


def update_coin(token, param, status):
    coins = json.loads(get_settings('coins')[0]['value'])
    for c in coins:
        if c['coin'] == token:
            c[param] = status
    add_setting('coins', json.dumps(coins))
