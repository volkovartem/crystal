import json
import datetime as dt
from database.models import Log
from database.database import db_session
from pymemcache.client.base import Client

# settings
logLenth = 300
threadLogLenth = 50
TimeRangeStatsSec = 86400


def get_log():
    data = []
    lines = db_session.query(Log).all()
    for l in lines:
        str = {
            "id": l.id,
            "time": l.time,
            "line": l.line
        }
        data.append(str)
    return data


def get_threads_log():
    client = Client(('127.0.0.1', 11211))
    r = client.get('ThreadsLog')
    if r is not None:
        return json.loads(r.decode())
    return []


def log(text):
    if 'Traceback' in text:
        lines = db_session.query(Log).all()
        if (len(lines) > logLenth):
            a = len(lines) - logLenth
            newlist = lines[:a]
            for l in newlist:
                db_session.delete(l)

        dtt = dt.datetime.utcnow().isoformat().replace('T', '  ').split('.')[0]
        line = Log(dtt.split('-')[1] + '.' + dtt.split('-')[2], str(text))
        db_session.add(line)
        db_session.commit()
    else:
        client = Client(('127.0.0.1', 11211))
        r = client.get('ThreadsLog')
        lines = []
        dtt = dt.datetime.utcnow().isoformat().replace('T', '  ').split('.')[0]
        dtt = dtt.split('-')[1] + '.' + dtt.split('-')[2]
        if r is not None:
            lines = json.loads(r.decode())
        lines.append({"time": dtt, "line": text})
        if len(lines) > threadLogLenth:
            lines = lines[len(lines) - threadLogLenth:]
        client.set('ThreadsLog', json.dumps(lines))


def print_arb_log(text, mark):
    xtext = ''
    for x in text:
        xtext = xtext + '\n                             ' + x
    log('{}{}'.format(mark, xtext))
    print('{}{}'.format(mark, xtext))


def clearLog():
    db_session.query(Log).delete()
    db_session.commit()
