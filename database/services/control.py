from database.models import Control
from database.database import db_session


def get_state():
    state = db_session.query(Control).filter_by(command='state').scalar()
    # print(state.text)
    return state.text


def set_state(text):
    exists = db_session.query(Control).filter_by(command='state').scalar()
    if exists is not None:
        db_session.delete(exists)
    state = Control('state', str(text))
    db_session.add(state)
    db_session.commit()
