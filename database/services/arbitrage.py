from database.models import Arbitrage
from database.database import db_session


def get_arbitrages():
    data = []
    arbitrage = db_session.query(Arbitrage).all()
    for s in arbitrage:
        str = {
            "id": s.id,
            "opid": s.opid,
            "description": s.description,
            "data": s.data,
            "time": s.time,
            "status": s.status
        }
        data.append(str)
    return data


def add_arbitrage(opid, description, data, time, status):
    sets = Arbitrage(opid, description, data, time, status)
    db_session.add(sets)
    db_session.commit()
