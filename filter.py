import time
import restWrapper as wp
from database.services.settings import *

# settings
AllExchanges = ['Binance', 'Okex', 'Bittrex', 'Hitbtc']

print('filter start')
while True:
    time.sleep(1)
    currencies = ['BTCUSDT', 'ETHUSDT']
    prices = {}
    for exchange in AllExchanges:
        prcs = {}
        for currency in currencies:
            result = wp.get_the_price(exchange, currency)
            if result is None:
                prcs.clear()
                break
            prcs.update({currency: result})
        if prcs == {}:
            prices = {}
            break
        prices.update({exchange: prcs})

    if prices != {}:
        # Volume24hThresholdUSDT = int(get_settings('volume')[0]['value'])
        Volume24hThresholdUSDT = 0

        lists = {}
        for exchange in AllExchanges:
            exchangeList = []
            volumes = wp.get_24hVolume(exchange)
            if volumes is not None:
                for volume in volumes:
                    market = volume.split('_')[1]
                    if market == 'BTC':
                        if float(volumes[volume]) * float(prices[exchange][currencies[0]]) > Volume24hThresholdUSDT:
                            exchangeList.append(volume)
                    if market == 'ETH':
                        if float(volumes[volume]) * float(prices[exchange][currencies[1]]) > Volume24hThresholdUSDT:
                            exchangeList.append(volume)
                    if market == 'USDT':
                        if float(volumes[volume]) > Volume24hThresholdUSDT:
                            exchangeList.append(volume)

            lists.update({exchange: exchangeList})

        add_setting('filter', json.dumps(lists))
        print('Updated')
        for keys, values in lists.items():
            print(keys, len(values))
        time.sleep(int(get_settings('timeout')[0]['value']) * 3600)
    time.sleep(10)

# print(okex.get_24hVolume())
# print(bittrex.get_24hVolume())

# print(binance.get_the_price('ETHUSDT'))
# print(okex.get_the_price('eth_usdt'))
# print(bittrex.get_the_price('USDT-ETH'))
#
# print(binance.get_the_price('BTCUSDT'))
# print(okex.get_the_price('btc_usdt'))
# print(bittrex.get_the_price('USDT-BTC'))
