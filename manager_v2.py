import threading
import traceback

import order_manager as om
import order_manager_double as om_double
import order_manager_single as om_single
import order_manager_triple as om_triple
import trade_utils
import utils
from database.services.control import *
from database.services.logging import *
from database.services.withdrawal import *


def to_log_file(text):
    f = open("log.txt", "a+")
    f.write(text + '\n')


try:
    print('manager started')
    while True:
        try:
            client = Client(('127.0.0.1', 11211))
            set_state('stop')
            set_state('run')
            while True:
                if True:  # get_state() == 'run'
                    req = client.get('Arbitrage')
                    if req is not None and req.decode() != '':
                        opps = json.loads(req.decode())
                        for op in opps:
                            if trade_utils.is_relevant(op):  # get_state() == 'run' and
                                data = opps[op]

                                # data = {'Okex': ['BTC', 'USDT', 'NEO', 'ETH', 'BTC'], 'profit': '1.00691'}
                                # data = {'Binance': ['LTC', 'USDT', 'NEO'], 'Okex': ['NEO', 'USDT', 'ETH', 'LTC'], 'profit': '1.00691'}
                                # data = {'Okex': ['USDT', 'ETH', 'VEE'], 'Bittrex': ['VEE', 'BTC', 'XMR', 'USDT'], 'profit': '1.04333'}

                                ### Single chain ###

                                if len(data) == 2:
                                    exchange = list(data.keys())[0]
                                    mark = '{}: {}'.format(exchange, str(data[exchange]))

                                    # t = time.time()
                                    checking = om_single.check(data, mark)
                                    # print(time.time() - t)
                                    if checking is not None:
                                        if checking[1] > 0:
                                            if trade_utils.is_relevant(op):
                                                threads = []
                                                result = {}
                                                t = time.time()
                                                th = threading.Thread(target=om.execute_v2,
                                                                      kwargs={"exchange": exchange,
                                                                              "nodes": data[exchange],
                                                                              "real_prices": checking[0][exchange],
                                                                              "amount": checking[3],
                                                                              "mark": mark,
                                                                              "response": result})
                                                th.start()
                                                threads.append(th)

                                                threads[0].join()

                                                print('execute time (sec)', time.time() - t, result)

                                                if result:
                                                    # profit = om.calc_profit([result], [data[exchange][0]], checking)
                                                    status = 'failed'
                                                    profit = 'No'

                                                    if result['success']:
                                                        status = 'completed'

                                                    if status != 'failed':
                                                        profit = result['amount'] - result['init_amount']
                                                        print('PROFIT', profit)

                                                    om.statement(data['profit'], mark, checking[3],
                                                                 checking[2], profit,
                                                                 data[exchange][0], op, 'No',
                                                                 'completed' if result['success'] else 'failed',
                                                                 result['trades'])
                                                    # infos = result['info']
                                                    # for info in infos:
                                                    #     to_log_file(str(info))
                                                    print('Arbitrage completed')
                                                else:
                                                    print('Arbitrage failed')

                                ### Double chain ###

                                if len(data) == 33:
                                    exchange1 = list(data.keys())[0]
                                    exchange2 = list(data.keys())[1]
                                    mark = '{}: {} | {}: {}'.format(exchange1, str(data[exchange1]), exchange2,
                                                                    str(data[exchange2]))
                                    checking = om_double.check(data, mark, op)
                                    if checking is not None:
                                        print(checking)
                                        print('Arbitrage')
                                        threads = []
                                        result1 = {}
                                        th1 = threading.Thread(target=om.execute,
                                                               kwargs={"exchange": exchange1,
                                                                       "nodes": data[exchange1],
                                                                       "real_prices": checking[0][exchange1],
                                                                       "amount": checking[3][exchange1],
                                                                       "mark": mark.split(' | ')[0],
                                                                       "response": result1})
                                        th1.start()
                                        threads.append(th1)

                                        result2 = {}
                                        th2 = threading.Thread(target=om.execute,
                                                               kwargs={"exchange": exchange2,
                                                                       "nodes": data[exchange2],
                                                                       "real_prices": checking[0][exchange2],
                                                                       "amount": checking[3][exchange2],
                                                                       "mark": mark.split(' | ')[1],
                                                                       "response": result2})
                                        th2.start()
                                        threads.append(th2)

                                        threads[0].join()
                                        threads[1].join()

                                        print(result1, result2)

                                        status = 'failed'
                                        if result1['success'] and result2['success']:
                                            status = 'completed'
                                            print("WITHDRAWAL", exchange1, data[exchange1][-1],
                                                  utils.round_dwn(result1['amount'], 5),
                                                  exchange2, data[exchange2][-1],
                                                  utils.round_dwn(result2['amount'], 5))
                                            add_withdrawal(exchange1, data[exchange1][-1],
                                                           utils.round_dwn(result1['amount'], 5))
                                            add_withdrawal(exchange2, data[exchange2][-1],
                                                           utils.round_dwn(result2['amount'], 5))

                                        print('status', status)
                                        profit = om.calc_profit([result1, result2], [data[exchange1][-1],
                                                                                     data[exchange2][-1]], checking)
                                        if status == 'failed':
                                            profit = 'No'
                                        print('profit', profit)
                                        stake = '{} {} & {} {}'.format(checking[3][exchange1],
                                                                       data[exchange1][0],
                                                                       checking[3][exchange2],
                                                                       data[exchange2][0])

                                        om.statement(data['profit'], mark, stake, checking[2], profit,
                                                     data[exchange1][0], op,
                                                     checking[4], status, result1['trades'] + result2['trades'])
                                        print('Arbitrage completed')

                                ### Triple chain ###

                                if len(data) == 44:
                                    exchange1 = list(data.keys())[0]
                                    exchange2 = list(data.keys())[1]
                                    exchange3 = list(data.keys())[2]
                                    mark = '{}: {} | {}: {} | {}: {}'.format(exchange1, str(data[exchange1]),
                                                                             exchange2, str(data[exchange2]),
                                                                             exchange3, str(data[exchange3]))
                                    print(mark)
                                    checking = om_triple.check(data, mark, op)
                                    if checking is not None:
                                        print(checking)
                                        print('Arbitrage')
                                        threads = []
                                        result1 = {}
                                        th1 = threading.Thread(target=om.execute,
                                                               kwargs={"exchange": exchange1,
                                                                       "nodes": data[exchange1],
                                                                       "real_prices": checking[0][exchange1],
                                                                       "amount": checking[3][exchange1],
                                                                       "mark": mark.split(' | ')[0],
                                                                       "response": result1})
                                        th1.start()
                                        threads.append(th1)

                                        result2 = {}
                                        th2 = threading.Thread(target=om.execute,
                                                               kwargs={"exchange": exchange2,
                                                                       "nodes": data[exchange2],
                                                                       "real_prices": checking[0][exchange2],
                                                                       "amount": checking[3][exchange2],
                                                                       "mark": mark.split(' | ')[1],
                                                                       "response": result2})
                                        th2.start()
                                        threads.append(th2)

                                        result3 = {}
                                        th3 = threading.Thread(target=om.execute,
                                                               kwargs={"exchange": exchange3,
                                                                       "nodes": data[exchange3],
                                                                       "real_prices": checking[0][exchange3],
                                                                       "amount": checking[3][exchange3],
                                                                       "mark": mark.split(' | ')[1],
                                                                       "response": result3})
                                        th3.start()
                                        threads.append(th3)

                                        threads[0].join()
                                        threads[1].join()
                                        threads[2].join()

                                        print(result1, result2, result3)

                                        status = 'failed'
                                        if result1['success'] and result2['success'] and result3['success']:
                                            status = 'completed'
                                            print("WITHDRAWAL", exchange1, data[exchange1][-1],
                                                  utils.round_dwn(result1['amount'], 5),
                                                  exchange2, data[exchange2][-1],
                                                  utils.round_dwn(result2['amount'], 5),
                                                  exchange3, data[exchange3][-1],
                                                  utils.round_dwn(result3['amount'], 5))
                                            add_withdrawal(exchange1, data[exchange1][-1],
                                                           utils.round_dwn(result1['amount'], 5))
                                            add_withdrawal(exchange2, data[exchange2][-1],
                                                           utils.round_dwn(result2['amount'], 5))
                                            add_withdrawal(exchange3, data[exchange3][-1],
                                                           utils.round_dwn(result3['amount'], 5))

                                        print('status', status)
                                        profit = om.calc_profit([result1, result2, result3], [data[exchange1][-1],
                                                                                              data[exchange2][-1],
                                                                                              data[exchange3][-1]],
                                                                checking)
                                        if status == 'failed':
                                            profit = 'No'
                                        print('profit', profit)
                                        stake = '{} {} & {} {} & {} {}'.format(checking[3][exchange1],
                                                                               data[exchange1][0],
                                                                               checking[3][exchange2],
                                                                               data[exchange2][0],
                                                                               checking[3][exchange3],
                                                                               data[exchange3][0])

                                        om.statement(data['profit'], mark, stake, checking[2], profit,
                                                     data[exchange1][0], op,
                                                     checking[4], status, result1['trades']
                                                     + result2['trades'] + result3['trades'])
                                        print('Arbitrage completed')

                time.sleep(.1)
        except KeyboardInterrupt:
            pass
        except:
            log(traceback.format_exc())
            print(traceback.format_exc())

except KeyboardInterrupt:
    pass
except:
    log(traceback.format_exc())
    print(traceback.format_exc())
