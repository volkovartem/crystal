import json
import time
import utils
import traceback
import restWrapper as wp
from database.services.logging import log
from database.services.api import get_api
from pymemcache.client.base import Client
from database.services.settings import get_settings

AllExchanges = ['Binance', 'Okex', 'Bittrex', 'Hitbtc']


def get_trading_fees(exchange):
    try:
        buyFees = get_api(exchange)[2]
        sellFees = get_api(exchange)[3]
        return {'buyFees': buyFees, 'sellFees': sellFees}
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def normalize_amount(exchange, amount, s1, s2, price=0.0):
    try:
        pair = utils.normalize_pair(s1, s2)
        if pair[0] == 'buy':
            if price != 0:
                amount = amount / price
                return utils.round_dwn(amount, 5)
            price = wp.get_price(exchange, pair[1] + pair[2])
            if price is not None:
                price = price['ask']
                amount = amount / float(price)
                return utils.round_dwn(amount, 5)
        if pair[0] == 'sell':
            return utils.round_dwn(amount, 5)
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def is_relevant(opportunity):
    try:
        client = Client(('127.0.0.1', 11211))
        req = client.get('Arbitrage')
        if req is not None:
            opps = json.loads(req.decode())
            if opportunity in opps:
                return True
            else:
                return False
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())
        return False


def get_price_universal(exchange, s1, s2, xprice=0.0, mode=False):
    try:
        symbol = utils.normalize_pair(s1, s2, 'symbol')
        if symbol == s1 + s2:
            if xprice != 0:
                return float(xprice)
            price = wp.get_price(exchange, symbol)
            if price is None and mode:
                price = wp.get_the_price(exchange, symbol)
                if price is not None:
                    return float(price)
            if price is not None:
                return float(price['bid'])

        else:
            if xprice != 0:
                return 1 / float(xprice)
            price = wp.get_price(exchange, symbol)
            if price is None and mode:
                price = wp.get_the_price(exchange, symbol)
                return 1 / float(price)
            if price is not None:
                return 1 / float(price['ask'])
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_amount_limits(exchange, token):
    try:
        lims = json.loads(get_settings('trade_limits')[0]['value'])
        amount_min = float(lims['amount_min_usdt'])  # in USDT
        amount_max = float(lims['amount_max_usdt'])  # in USDT

        if token == 'USDT':
            return {'amount_min': amount_min, 'amount_max': amount_max}

        priceUSDT = wp.get_price(exchange, token + 'USDT')
        if priceUSDT is not None:
            priceUSDT = priceUSDT['bid']
            return {'amount_min': amount_min / float(priceUSDT),
                    'amount_max': amount_max / float(priceUSDT)}

        priceBTC = wp.get_price(exchange, token + 'BTC')
        priceBTCUSDT = wp.get_price(exchange, 'BTCUSDT')
        if priceBTC is not None and priceBTCUSDT is not None:
            priceBTC = priceBTC['bid']
            priceBTCUSDT = priceBTCUSDT['bid']
            return {'amount_min': amount_min / float(priceBTCUSDT) / float(priceBTC),
                    'amount_max': amount_max / float(priceBTCUSDT) / float(priceBTC)}

        priceETH = wp.get_price(exchange, token + 'ETH')
        priceETHUSDT = wp.get_price(exchange, 'ETHUSDT')
        if priceETH is not None and priceETHUSDT is not None:
            priceETH = priceETH['bid']
            priceETHUSDT = priceETHUSDT['bid']
            return {'amount_min': amount_min / float(priceETHUSDT) / float(priceETH),
                    'amount_max': amount_max / float(priceETHUSDT) / float(priceETH)}
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_real_price(exchange, symbol, amount):
    try:
        orderbook = wp.get_order_book(exchange, symbol)
        if orderbook is not None:
            result = {}
            bid_stack = 0
            for bid in orderbook['bids']:
                if float(bid[1]) + bid_stack > amount:
                    result.update({'realBid': float(bid[0])})
                    break
                else:
                    bid_stack += float(bid[1])
            ask_stack = 0
            for ask in orderbook['asks']:
                if float(ask[1]) + ask_stack > amount:
                    result.update({'realAsk': float(ask[0])})
                    break
                else:
                    ask_stack += float(ask[1])
            if result != {}:
                return result
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_real_price_v2(exchange, symbol, amount, side):
    try:
        orderbook = utils.get_order_book_fast(exchange, symbol)
        gap = round(time.time() - (float(orderbook['timestamp'] / 1000)), 4)
        ob = str('{} {} {} {} {} {} {} {} {} {} {}'.format('orderbook', 'time gap', gap,
                                                           'timestamp', (float(orderbook['timestamp']) / 1000), symbol,
                                                           'amount', amount, 'time now', time.time(), orderbook))
        if gap > 2:
            return
        if orderbook is not None:
            result = {}
            if side == 'buy':
                amount_dom = 0
                pos = 0
                orderbook['asks'].reverse()
                for ask in orderbook['asks']:
                    if amount / float(ask[0]) < float(ask[1]) + amount_dom:
                        return [float(ask[0]), ob]
                    else:
                        amount_dom += float(ask[1])
                        pos += 1
                print('Getting real price. Iteration DOM ended {}'.format(str(pos)))
            if side == 'sell':
                amount_dom = 0
                pos = 0
                for bid in orderbook['bids']:
                    if amount < amount_dom + float(bid[1]):
                        return [float(bid[0]), ob]
                    else:
                        amount_dom += float(bid[1])
                        pos += 1
                print('Getting real price. Iteration DOM ended {}'.format(str(pos)))
            if result:
                return result
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_real_price_v3(exchange, symbol, amount, side):
    try:
        orderbook = utils.get_order_book_fast(exchange, symbol)
        if orderbook is not None:
            result = {}
            if side == 'buy':
                amount_dom = 0
                pos = 0
                orderbook['asks'].reverse()
                for ask in orderbook['asks']:
                    if amount / float(ask[0]) < float(ask[1]) + amount_dom:
                        return float(ask[0])
                    else:
                        amount_dom += float(ask[1])
                        pos += 1
                return float(orderbook['asks'][-1][0])
            if side == 'sell':
                amount_dom = 0
                pos = 0
                for bid in orderbook['bids']:
                    if amount < amount_dom + float(bid[1]):
                        return float(bid[0])
                    else:
                        amount_dom += float(bid[1])
                        pos += 1
                return float(orderbook['bids'][-1][0])
            if result:
                return result
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def calc_coeff_chain(exchange, nodes, mode=False):
    try:
        coeff = 1
        for x in range(len(nodes) - 1):
            price = get_price_universal(exchange, nodes[x], nodes[x + 1], 0, mode)
            if price is None:
                return
            coeff = coeff * price
        return coeff
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_amount_wallet(exchange, token, limits=False, percent=False):
    try:
        balance = utils.get_balance_fast(exchange, token)
        if balance is not None:
            walletPercent = float(get_settings('walletPercent')[0]['value']) if percent else 100
            if limits:
                limits = get_amount_limits(exchange, token)
                if limits is not None:
                    amount_min = limits['amount_min']
                    amount_max = limits['amount_max']
                    amount = float(balance) / 100 * walletPercent
                    if amount > amount_min:
                        # print('get am wal', min(amount, amount_max))
                        return utils.round_dwn(min(amount, amount_max), 5)
            else:
                return utils.round_dwn(float(balance) / 100 * walletPercent, 5)
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_markets(market, token):
    markets = []
    if token + '_BTC' in market or 'BTC_' + token in market:
        markets.append('BTC')
    if token + '_ETH' in market or 'ETH_' + token in market:
        markets.append('ETH')
    if token + '_USDT' in market or 'USDT_' + token in market:
        markets.append('USDT')
    return markets


def get_shared_token(market, token1, token2):
    token1m = get_markets(market, token1)
    token2m = get_markets(market, token2)

    if token1m != [] and token2m != []:
        for x in token1m:
            for y in token2m:
                if x == y:
                    return [x]
        if token1m[0] + '_' + token2m[0] in market or token2m[0] + '_' + token1m[0] in market:
            return [token1m[0], token2m[0]]


def make_achain(exchange, token1, token2, market=None):
    if market is None:
        market = json.loads(get_settings('filter')[0]['value'])[exchange]
    if token1 + '_' + token2 in market or token2 + '_' + token1 in market:
        return [token1, token2]
    nodes = get_shared_token(market, token1, token2)
    if nodes is not None:
        return [token1] + nodes + [token2]


def convert(exchange, from_token, to_token, market, mode=True):
    chain = make_achain(exchange, from_token, to_token, market)
    if chain is not None:
        coeff = calc_coeff_chain(exchange, chain, mode)
        if coeff is not None:
            return utils.round_dwn(float(coeff), 8)


def get_total_balance(available, token, markets):
    head = {'token': 'Total ({})'.format(token)}
    for data in available:
        for k in data.keys():
            if k != 'token' and data[k] != '-':
                coeff = 1
                if token != data['token']:
                    coeff = convert(k, data['token'], token, markets[k])
                if coeff is not None:
                    if k not in head:
                        head.update({k: float(data[k]) * coeff})
                    else:
                        head.update({k: head[k] + float(data[k]) * coeff})

    for k in head.keys():
        if k != 'token' and head[k] != '-':
            head.update({k: utils.round_dwn(float(head[k]), 2)})

    for m in markets.keys():
        if m not in head:
            head.update({m: '-'})
    return head


def get_available_balance():
    response = []
    available = {}
    markets = {}
    client = Client(('127.0.0.1', 11211))
    for exchange in AllExchanges:
        r = client.get(exchange + 'Balance')
        rm = client.get(exchange + 'Markets')
        if r is not None and rm is not None:
            rm = json.loads(rm.decode())
            if exchange not in markets:
                markets.update({exchange: rm})
            r = json.loads(r.decode())
            available.update({exchange: dict((x['token'], x['quantity']) for x in r if float(x['quantity']) > 0)})

    all_tokens = list(set(list(sum([list(available[x].keys()) for x in available], []))))
    for token in all_tokens:
        d = {'token': token}
        d.update(dict((ex, utils.norm(available[ex][token]) if token in available[ex] else '-') for ex in AllExchanges))
        response.append(d)

    response.insert(0, get_total_balance(response, 'USDT', markets))
    # print(markets)
    return response


def get_available_balance2(exchange):
    client = Client(('127.0.0.1', 11211))
    r = client.get(exchange + 'Balance')
    if r is not None:
        response = []
        r = json.loads(r.decode())
        for balance in r:
            if utils.round_dwn(float(balance['quantity']), 6) > 0:
                response.append(balance)
        return response


# get_available_balance2('Okex')


def get_trade_limits_fast_chain(exchange, chain):
    try:
        slippage = 3  # %
        mins = []
        coeff = 1
        fees = get_trading_fees(exchange)
        for x in range(len(chain) - 1):
            symbol = utils.normalize_pair(chain[x], chain[x + 1])
            price = wp.get_price(exchange, symbol[1] + symbol[2])
            minLimit = utils.get_trade_limits_fast(exchange, symbol[1] + symbol[2])
            price = float(price['bid']) if symbol[0] == 'sell' else 1 / float(price['ask'])
            # print(minLimit, symbol[1] + symbol[2], price)
            mins.append(minLimit['market'] / coeff if symbol[0] == 'sell' else minLimit['market'] / (coeff * price))
            # print(mins, coeff)
            coeff *= price
        fee = 1 + ((max(float(fees['sellFees']), float(fees['buyFees'])) * len(mins) + slippage) / 100)
        return round(max(mins) * fee, 6)
    except:
        pass

# print(get_trade_limits_fast_chain('Okex', ['USDT', 'POE', 'BTC', 'ETH', 'MDA', 'BTC', 'USDT']))

# print(get_amount_limits('Okex', 'ETH'))
# print(get_trade_limits_fast_chain('Okex', ['USDT', 'SALT', 'ETH', 'LIGHT', 'BTC', 'USDT']))

# print(get_amount_wallet('Okex', 'INSUR'))
# print(get_amount_wallet('Okex', 'INSUR', limits=True))
# print(get_amoupstpstnt_wallet('Okex', 'INSUR', percent=True))
# print(get_amount_wallet('Okex', 'INSUR', limits=True, percent=True))
