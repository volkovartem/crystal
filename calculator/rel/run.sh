#!/bin/sh
[ -d build ] || mkdir build

cd build
cmake ../
make
cd ..


#echo "------------------------------------------------------------------------"
#echo "Dual Network Test: "
#cat test1.csv

#echo ""
#echo "------------------------------------------------------------------------"

#echo "./build/2darray test1.csv output.csv 5 100000 0.0001 AAA XXX \"*\""

#./build/2darray test1.csv output.csv 8 100000 0.0001 AAA XXX "*"
#cat output.csv

#echo ""
#echo "------------------------------------------------------------------------"
#echo "./build/2darray test1.csv output.csv 5 100000 0.0001 AAA XXX \"*\" 5:2"

#./build/2darray test1.csv output.csv 8 100000 0.0001 AAA XXX "*" 5:2
#cat output.csv

#echo ""
#echo "------------------------------------------------------------------------"
#echo "./build/2darray test1.csv output.csv 5 100000 0.0001 AAA XXX \"*\" 2:0"

#./build/2darray test1.csv output.csv 8 100000 0.0001 AAA XXX "*" 2:0
#cat output.csv

#./build/2darray input.csv output.csv 6 20 1.003

#cat output.csv

