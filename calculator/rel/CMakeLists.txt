PROJECT ( dude )
CMAKE_MINIMUM_REQUIRED ( VERSION 2.8.8 )

set ( SRC_FILES
	 run_fast.cpp
)

set ( CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}  -Werror-implicit-function-declaration  -std=c++11 -fopenmp" )

include_directories (
    ./
)


add_executable (2darray ${SRC_FILES})
