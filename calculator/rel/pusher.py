#!/usr/bin/env

import subprocess
# sys.path.append('/root/crystal')
import time
import traceback

from pymemcache.client.base import Client

import table
import utils
from database.services.settings import *
from restWrapper import get_price

# AllExchanges = json.loads(utils.get_settings('activeExchanges')[0]['value'])
# AllExchanges = ['Binance', 'Okex', 'Bittrex', 'Hitbtc']

# settings
timeout = 10  # min
orderbook_expiration_time = 2  # sec


def to_log_file(text):
    with open('log.txt') as f:
        content = f.readlines()
        text = text + '\n'
        print(text in content)
        if text not in content:
            f = open("log.txt", "a+")
            f.write(text)


def update_history(client, chains):
    history = client.get('UpdatesHistory')
    history = json.loads(history.decode()) if history is not None else []
    history.append({'timestamp': time.time(), 'chains': chains})
    for item in history:
        if float(item['timestamp']) < time.time() - (timeout * 60):
            history.remove(item)
    client.set('UpdatesHistory', json.dumps(history))
    print('history', len(history), history)


def get_id(opp, client):
    xopp = json.dumps(opp).split('profit')[0]
    _arbs = client.get('Arbitrage')
    if _arbs is not None:
        _arbs = json.loads(_arbs)
        for _arb in _arbs:
            yopp = json.dumps(_arbs[_arb]).split('profit')[0]
            if xopp == yopp:
                return _arb
    return str(time.time())


def parse_exchanges(op):
    uniq = []
    c = op.split('( ')[1].split(' )')[0].split(' ')
    if 4 <= len(c) <= 7:
        exchanges = [cc.split('_')[1] for cc in c]
        for exchange in exchanges:
            if exchange not in uniq:
                uniq.append(exchange)
            else:
                if exchange != uniq[-1]:
                    # return
                    uniq.append(exchange)

        if 1 <= len(uniq) <= 3:
            return uniq


# print(parse_exchanges('1.03598 ( BTC_Okex SNC_Okex SNC_Hitbtc ETH_Hitbtc BQX_Hitbtc BQX_Binance BTC_Binance )'))
# print(parse_exchanges('1.03598 ( BTC_Okex SNC_Okex SNC_Hitbtc ETH_Hitbtc BQX_Hitbtc BQX_Okex BTC_Okex )'))
# print(parse_exchanges('1.03598 ( BTC_Okex SNC_Okex SNC_Hitbtc ETH_Hitbtc )'))
# print(parse_exchanges('1.03598 ( BTC_Okex SNC_Okex )'))
# time.sleep(1111)


import csv


def get_output(token1, token2, exchange):
    # /root/crystal/calculator/rel/output.csv
    pair = utils.normalize_pair(token1, token2)
    token1 = pair[1]
    token2 = pair[2]
    with open('input.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        result = {}
        result.update({'symbol': token1 + token2, 'side': pair[0]})
        if reader.fieldnames is not None:
            for row in reader:
                if dict(row)[' '] == token1 + '_' + exchange:
                    result.update({'bid': dict(row)['{}_{}'.format(token2, exchange)]})
                if dict(row)[' '] == token2 + '_' + exchange:
                    result.update({'ask': dict(row)['{}_{}'.format(token1, exchange)]})
        return result


def test_func(chain, exch):
    result = []
    for x in range(len(chain) - 1):
        result.append(get_output(chain[x], chain[x + 1], exch))
    return result


# print(test_func(['ETH', 'NULS', 'BTC', 'LTC'], 'Okex'))

lims = json.loads(get_settings('trade_limits')[0]['value'])
amount_min_usdt = float(lims['amount_min_usdt'])
amount_min_btc = round(amount_min_usdt / float(get_price('Okex', 'BTCUSDT')['ask']), 6)
amount_min_eth = round(amount_min_usdt / float(get_price('Okex', 'ETHUSDT')['ask']), 6)
print('amount_min_usdt', amount_min_usdt)
print('amount_min_btc', amount_min_btc)
print('amount_min_eth', amount_min_eth)


def get_min_amount(base, quote, price):
    if base == 'USDT':
        return amount_min_usdt
    if base == 'BTC':
        return amount_min_btc
    if base == 'ETH':
        return amount_min_eth
    if quote == 'USDT':
        return amount_min_usdt / price
    if quote == 'BTC':
        return amount_min_btc / price
    if quote == 'ETH':
        return amount_min_eth / price


print('start pusher')
# client = Client(('127.0.0.1', 11211))
# client.set('UpdatesHistory', '[]')
# client.set('TEST', '{}')
# AllExchanges = json.loads(get_settings('activeExchanges')[0]['value'])
AllExchanges = {'Binance': True, 'Bittrex': False, 'Hitbtc': False, 'Okex': True}
AllCoins = json.loads(get_settings('coins')[0]['value'])
while True:
    time.sleep(.5)
    try:
        client = Client(('127.0.0.1', 11211))
        count = 0
        table.init()
        t = time.time()
        for exchange in AllExchanges:
            if AllExchanges[exchange]:
                rr = client.get(exchange + 'HW')
                fltr = client.get('filter_' + exchange)
                if rr is not None and fltr is not None:
                    whitelist = json.loads(rr)
                    blacklist = list(x['coin'] for x in json.loads(get_settings('blacklist')[0]['value']))
                    fltr = json.loads(fltr)
                    for _symbol in fltr:
                        data = client.get(_symbol.replace('_', '') + '_' + exchange)
                        if data is not None:
                            data = json.loads(data.decode())
                            _symbol = utils.split_symbol(_symbol.replace('_', ''))
                            if (_symbol['s'] in whitelist and
                                    _symbol['b'] in whitelist and
                                    _symbol['s'] not in blacklist and
                                    _symbol['b'] not in blacklist):
                                if _symbol['s'] not in list(x['coin'] for x in AllCoins):
                                    add_coin(_symbol['s'])
                                    AllCoins = json.loads(get_settings('coins')[0]['value'])
                                    print('All coins updated ' + _symbol['s'])
                                base = _symbol['s'] + '_' + exchange
                                quote = _symbol['b'] + '_' + exchange
                                if base not in table.get_left_column():
                                    table.add_row(base)
                                if base not in table.get_top_row():
                                    table.add_column(base)
                                if quote not in table.get_left_column():
                                    table.add_row(quote)
                                if quote not in table.get_top_row():
                                    table.add_column(quote)

                                bid = '{0:.10f}'.format(float(data['bids'][0][0]))
                                ask = '{0:.10f}'.format(1 / float(data['asks'][-1][0]))
                                # print(exchange, ask, bid)

                                table.set(base, quote, bid)
                                table.set(quote, base, ask)

        # print('speed', time.time() - t)
        table.save()

        shcommand = get_settings('script')[0]['value']
        # shcommand = shcommand.replace('input.csv', '/root/crystal/calculator/rel/input.csv')
        # shcommand = shcommand.replace('output.csv', '/root/crystal/calculator/rel/output.csv')
        status = subprocess.call([shcommand], shell=True)

        ttest = {}
        opportunities = table.get_output()
        if opportunities is not None:
            opps = {}
            type_single = 0
            type_double = 0
            type_triple = 0
            for opp in opportunities:
                exx = parse_exchanges(opp)
                if exx is not None:
                    # active = json.loads(get_settings('activeExchanges')[0]['value'])
                    active = AllExchanges
                    nodes = opp.split('( ')[1].split(' )')[0].split(' ')
                    flag = True
                    for ex in exx:
                        if active[ex] is False:
                            flag = False
                    if flag is True:
                        chains = {}
                        for ex in exx:
                            ns = [n.split('_')[0] for n in nodes if n.split('_')[1] == ex]
                            chains.update({ex: ns})

                        if len(chains) == 1:
                            type_single += 1
                        if len(chains) == 2:
                            type_double += 1
                        if len(chains) == 3:
                            type_triple += 1

                        chains.update({'profit': opp.split(' ')[0]})

                        Id = get_id(chains, client)
                        if len(chains) == 3:
                            symbol = utils.normalize_pair(chains[list(chains.keys())[0]][0],
                                                          chains[list(chains.keys())[0]][1])
                            amount = utils.get_amount_dom(symbol[1] + symbol[2], list(chains.keys())[0],
                                                          list(chains.keys())[1])

                            # chains.update(
                            #     {'info': 'amount ' + '{:.10f}'.format(amount[0]) + ' profit ' + amount[1] + ' ' + symbol[1]})
                            opps.update({Id: chains})

            print(len(opps), 'opps')
            for op in opps:
                print(opps[op])
                # to_log_file(str(opps[op]))
            client.set('Arbitrage', json.dumps(opps))

            # iop = {'Okex': ['LTC', 'USDT', 'NEO'], 'Bittrex': ['NEO', 'USDT', 'LTC'], 'profit': '1.00691'}
            # iop = {'Okex': ['LSK', 'USDT', 'NEO', 'ETH', 'LSK'], 'profit': '1.00691'}
            # iop = {'Bittrex': ['ETH', 'NEO', 'USDT'], 'profit': '1.00691'}

            # Id = get_id(iop, client)
            # client.set('Arbitrage', json.dumps({Id: iop}))
            inf = "Updated {} Output: {}, Actual output: {}  Stats: single-{}, double-{}, triple-{} |{}".format(
                utils.get_time().split(' ')[0], len(opportunities),
                len(opps), type_single, type_double, type_triple, time.time())

            client.set('OppLogInfo', inf)
            print(inf)
            if opps:
                update_history(client, opps)

            # client.set('TEST', json.dumps(ttest))
            # print('ttest', json.dumps(ttest))

        time.sleep(1)
    except KeyboardInterrupt:
        pass
    except:
        print('pusher Error: memcache connection failed')
        print(traceback.format_exc())
        time.sleep(100)
