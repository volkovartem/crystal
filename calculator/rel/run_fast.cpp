#include <stdio.h>
#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include <sstream>
#include <omp.h>
// special name for start/end/bridge nodes
const char *ANY_NODE = "*";

// network separator within node name
const char NETWORK_SEPARATOR = '_';

// using IndexVec = std::vector<size_t>;
typedef std::vector<size_t> IndexVec;

////////////////////////////////////////////////////////////////////////////////////////////////////

enum class CSVState {
    UnquotedField,
    QuotedField,
    QuotedQuote
};


std::vector<std::string> readCSVRow(const std::string &row) {
    CSVState state = CSVState::UnquotedField;
    std::vector<std::string> fields;
	fields.push_back("");
    size_t i = 0; // index of the current field

    for (char c : row) {
        switch (state) {
            case CSVState::UnquotedField:
                switch (c) {
                    case ',': fields.push_back(""); i++;        break;  // end of field
                    case '"': state = CSVState::QuotedField;    break;
                     default: fields[i].push_back(c);           break;
                }
                break;
            case CSVState::QuotedField:
                switch (c) {
                    case '"': state = CSVState::QuotedQuote;    break;
                    default:  fields[i].push_back(c);           break;
                }
                break;
            case CSVState::QuotedQuote:
                switch (c) {
                    case ',': // , after closing quote
                              fields.push_back(""); i++;
                              state = CSVState::UnquotedField;
                              break;
                    case '"': // "" -> "
                              fields[i].push_back('"');
                              state = CSVState::QuotedField;
                              break;
                     default: // end of quote
                              state = CSVState::UnquotedField;
                              break;
                }
                break;
        }
    }
    return fields;
}

// read csv file, excel dialect
std::vector<std::vector<std::string>> readCSV(std::istream &in) {
    std::vector<std::vector<std::string>> table;
    std::string row;
    while (!in.eof()) {
        std::getline(in, row);
        if (in.bad() || in.fail()) break;
        auto fields = readCSVRow(row);
        table.push_back(fields);
    }
    return table;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<std::vector<std::string>>  readTable(const std::string &filename) {
    std::ifstream finput(filename);
    if (!finput.is_open()) throw   std::runtime_error ( "Failed to open file " + filename);

    std::vector<std::vector<std::string>>  table = readCSV(finput);
    if (table.empty()) throw std::runtime_error ("No data found within " + filename );

    for (auto &row : table) {

        // validate table dimensions
        if (row.size() != table.size()) throw std::runtime_error ( "Data table is not square" );

        // remove control characters
        for (auto &field : row) {
            field.erase(std::remove_if(field.begin(), field.end(), [](unsigned char c) {
                return std::iscntrl(c) || std::isspace(c);
            }), field.end());
        }
    }

    // validate row/column labels
    for (size_t i = 1; i < table.size(); ++i) {
        if (table[0][i] != table[i][0]) {
            std::string message = "Invalid table format: row <" + table[i][0]
                                + "> doesn't correspond to column <" + table[0][i] + ">";
            throw std::runtime_error ( message );
        }
    }

    return table;
}

bool isNodePrefix(const std::string &prefix, const std::string &node) {
    return node.size() > prefix.size()
        && node.substr(0, prefix.size()) == prefix
        && node[prefix.size()] == NETWORK_SEPARATOR;
}

// search node indices by name/prefix
IndexVec findNodeIndices(const std::string &name, const std::vector<std::string> &header,
                         const std::string &label) {

    if (name.empty()) {
		IndexVec res;
		res.push_back(0);
		return res;
	};

    IndexVec result;

    for (size_t i = 0; i < header.size(); ++i) {
        if (name == header[i] || isNodePrefix(name, header[i])) {
            result.push_back(i - 1);
        }
    }

    if (result.empty()) throw std::out_of_range ( "Non-existent " + label + " node " + name );
    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

struct SearchResult {
    IndexVec path;
    double cost ;
	SearchResult(const IndexVec &c, double _cc):path(c),cost(_cc){}
	SearchResult():cost(1){}

    bool operator<(const SearchResult &other) const { return cost < other.cost; }
};


typedef std::set <SearchResult> s_type1;
typedef std::vector<SearchResult> v_type1;

bool larger(const SearchResult& lhs, const SearchResult& rhs) {
    return lhs.cost > rhs.cost;
}

void scan_level(std::vector<double> &links, size_t nodesNum, v_type1 &v1,
        SearchResult& r, const std::vector<bool> &isEndNode) {
    const double *row = &links[r.path.back() * nodesNum];
    for (size_t j = 0; j < nodesNum; ++j) {
        if (row[j] != 0 && !isEndNode[j]) {
            auto r2 = v1.end();
            for (v_type1::iterator t = v1.begin(); t != v1.end(); ++t) {
                if (t->path.back() == j) {
                    r2 = t;
                    break;
                }
            }
            IndexVec s2;
            s2 = r.path;
            double d1 = row[j] * r.cost;
            s2.push_back(j);
            if (r2 == v1.end()) {
                SearchResult ts2(std::move(s2), d1);
                v1.push_back(ts2);
            } else {
                if (row[j] != 0 && d1 > r2->cost) {
                    r2->cost = d1;
                    r2->path = s2;
                }
            }
        }
    }
}

s_type1 &fast_search(std::vector<double> &links, size_t nodesNum,
            const IndexVec &startNode, const IndexVec &endNode,
            s_type1 &outputs, size_t outputsNum,
            size_t maxPathLen, const double maxOutputValue) {
    // add search result output if it's good enough
    auto addOutput = [&](s_type1 &outputs, IndexVec &path, double srcCost) {
        const double *row = &links[path.back() * nodesNum];

        for (size_t end : endNode) {
            double cost = srcCost * row[end];
            if (cost >= maxOutputValue && (outputs.size() < outputsNum || cost > outputs.cbegin()->cost)) {
                auto newPath = path;
                newPath.push_back(end);
                SearchResult ts(std::move(newPath), cost);
                outputs.insert(ts);

                if (outputs.size() > outputsNum) {
                    outputs.erase(outputs.begin());
                }
            }
        }
    };

    std::vector<bool> isEndNode(nodesNum, false);
    for (size_t index : endNode) {
        isEndNode[index] = true;
    }

    v_type1 v0;
    v0.reserve(nodesNum - 1);
    for (size_t start : startNode) {
        IndexVec sPath;
        sPath.push_back(start);
        SearchResult ts2(std::move(sPath), 1.0);
        v0.push_back(ts2);
    }

    for (size_t level = 1; level <= maxPathLen; ++level) {
        v_type1 v1;
        v1.reserve(nodesNum - 1);
        for (auto r : v0) {
            addOutput(outputs, r.path, r.cost);
            if (level < maxPathLen) {
                scan_level(links, nodesNum, v1, r, isEndNode);
            };
        }
        if (level < maxPathLen) {
            v0 = v1;
        }
    }

    return outputs;
}

void search(const std::vector <std::string> &header,
        const std::set <std::string> &commonNodesSet,
        std::vector <double> &links, size_t nodesNum,
        const std::string &startNode, const std::string &endNode,
        s_type1 &outputs, size_t outputsNum,
        size_t maxPathLen, const double maxOutputValue) {
    std::cout << " Start Nodes size=" << commonNodesSet.size();
	int  list_mode = 1;
	if (startNode.find(' ') == std::string::npos)
		list_mode = 0;

    if (startNode == ANY_NODE) {
        if (endNode != ANY_NODE) {
            throw std::invalid_argument("If start node is '*' then end node must be '*' too");
        }
        //int cnt = 1;
        const std::vector <std::string> commonNodesVec(commonNodesSet.begin(), commonNodesSet.end());
        int N_nodes = commonNodesVec.size();
        #pragma omp parallel
        {
            // thread local outputs buffer
            s_type1 threadOutputs;
            printf(" Num threads=%d", omp_get_num_threads());
            #pragma omp for
            for (int st = 0; st < N_nodes; ++st) {
                auto node = *(commonNodesVec.begin() + st);
                auto same = findNodeIndices(node, header, "start");
                fast_search(links, nodesNum, same, same, threadOutputs, outputsNum, maxPathLen, maxOutputValue);
                //std::cout << " " << cnt++ << ")" << node;
            }
            // merge buffers
            #pragma omp critical
            {
                if (outputs.empty()) {
                    outputs = std::move(threadOutputs);
                } else {
                    outputs.insert(threadOutputs.cbegin(), threadOutputs.cend());
                    if (outputs.size() > outputsNum) {
                        auto end = outputs.begin();
                        std::advance(end, outputs.size() - outputsNum);
                        outputs.erase(outputs.begin(), end);
                    }
                }
            }
        }
    }
	else if(list_mode==1) {
		std::istringstream ss(startNode);
		std::istream_iterator<std::string> begin(ss);
		std::istream_iterator<std::string> end;
		std::vector <std::string> startNodesVec(begin, end);
		std::vector <std::string> endNodesVec;
		if (endNode == ANY_NODE) {
			endNodesVec = startNodesVec;
			//throw std::invalid_argument("If start node is '*' then end node must be '*' too");
		}
		else {
			std::istringstream s2(endNode);
			std::istream_iterator<std::string> begin(s2);
			std::istream_iterator<std::string> end;
			std::vector <std::string> e2(begin, end);
			endNodesVec = e2;

		}

		int N_nodes = startNodesVec.size();
		#pragma omp parallel
		{
			// thread local outputs buffer
			s_type1 threadOutputs;
			printf("List mode start. Num threads=%d", omp_get_num_threads());
			#pragma omp for
			for (int st = 0; st < N_nodes; ++st) {
				auto node = *(startNodesVec.begin() + st);
				auto start = findNodeIndices(node, header, "start");
				if (st >= int(endNodesVec.size()))
				{
					fast_search(links, nodesNum, start, start, threadOutputs, outputsNum, maxPathLen, maxOutputValue);
				}
				else {
					auto end_node = *(endNodesVec.begin() + st);
					auto end = findNodeIndices(end_node, header, "end");
					fast_search(links, nodesNum, start, end, threadOutputs, outputsNum, maxPathLen, maxOutputValue);
				}
			}
			// merge buffers
			#pragma omp critical
			{
				if (outputs.empty()) {
					outputs = std::move(threadOutputs);
				}
				else {
					outputs.insert(threadOutputs.cbegin(), threadOutputs.cend());
					if (outputs.size() > outputsNum) {
						auto end = outputs.begin();
						std::advance(end, outputs.size() - outputsNum);
						outputs.erase(outputs.begin(), end);
					}
				}
			}
		}


	}
	else {
        auto start = findNodeIndices(startNode, header, "start");
        auto end = findNodeIndices(endNode, header, "end");

        fast_search(links, nodesNum, start, end, outputs, outputsNum, maxPathLen, maxOutputValue);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void makeBridgeLinks(std::vector <double> &links, size_t nodesNum,
                     const std::vector <std::string> &header, const std::string &commonNode) {
    // find common nodes
    auto common = findNodeIndices(commonNode, header, "common");

    // add links
    for (size_t from : common) {
        for (size_t to : common) {
            if (from != to) {
                links[from * nodesNum + to] = links[to * nodesNum + from] = 1;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// searches for the newtwork link (i.e: BTC_I <-> BTC_W )
// and according to number of nodes before/after it and the newtwork balance argument,
// filters the outputs of interest

void filterbyNetworkBalance(s_type1 &outputs , std::pair <int,int> networkBalance,
                            std::vector<std::vector<std::string>> &table) {

    bool DEBUG = false;
    s_type1 outputs2;

    if (DEBUG) std::cout << std::endl;

    for (auto it = outputs.rbegin(); it != outputs.rend(); ++it) {

        if (DEBUG) std::cout  <<  " ( ";

        bool foundBridge = false;
        int pre = 0;
        int post = 0;

        for (int i = 0; i < it->path.size(); i++) {
            if (!foundBridge && i+1 < it->path.size()) {
                pre++;
                size_t separator_0 = table.front()[it->path[i] + 1].find_first_of(NETWORK_SEPARATOR);
                size_t separator_1 = table.front()[it->path[i+1] + 1].find_first_of(NETWORK_SEPARATOR);

                if ( table.front()[it->path[i] + 1].substr(0, separator_0) ==
                     table.front()[it->path[i+1] + 1].substr(0, separator_1)  ) {

                    foundBridge = true;
                    if (DEBUG) std::cout << " << ";
                }
                if (DEBUG) std::cout << table.front()[it->path[i] + 1] << " ";

            } else {
                if (DEBUG) std::cout << table.front()[it->path[i] + 1] << " ";
                if (foundBridge ) {
                    if (post == 0) if (DEBUG) std::cout << ">> " ;
                    post++;
                } else { pre++; }
            }
        }
        if (DEBUG)  std::cout << ")" << " -- << pre : " << pre << "| post: " << post << " >>" << std::endl;

        if ( (pre == networkBalance.first && post == networkBalance.second) ||
             (post == networkBalance.first && pre == networkBalance.second)) {
                outputs2.insert(*it);
             }
    }

    outputs = outputs2;

}

////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {

    int maxPathLen, outputsNum;
    std::string startNode, endNode, commonNode;
	double maxOutputValue=1.01;

    // default value for end/start/bridge node
    startNode = endNode = commonNode = ANY_NODE;

    //Network Balance Filtering vars
    std::string networkBalanceArg = "";
    std::pair <int,int> networkBalance (0,0);

    { // process command line arguments
        if (argc < 5 || argc > 10) {
            std::cout << "Usage: " << argv[0]
                      << " <input> <output> <max-length> <outputs-num> <max-output-value> [start] [end] [common] [network-balance]"
                      << std::endl;
            return 1;
        }

        if (argc >= 6) maxOutputValue  =  atof(argv[5]);
        if (argc >= 7) startNode  = argv[6];
        if (argc >= 8) endNode    = argv[7];
        if (argc >= 9) commonNode = argv[8];
        if (argc >= 10) networkBalanceArg = argv[9];

        try {
            maxPathLen = std::stoi(argv[3]);
            outputsNum = std::stoi(argv[4]);
            if (maxPathLen <= 0 || outputsNum <= 0) {
                std::cerr << "Command line arguments are out of range" << std::endl;
                return 1;
            }
        }
        catch(std::invalid_argument &e) {
            std::cerr << "Invalid numbers encountered within command line arguments" << std::endl;
            return 1;
        }
    }

    std::string input = argv[1], output = argv[2];

    try {
        // read data table
        auto table = readTable(input);
        assert(table.size());

        // nodes num is table size minus one for header
        size_t nodesNum = table.size() - 1;

        // find all node prefixes
        std::set <std::string> commonNodesSet;
        for (auto &node : table.front()) {
            if (!node.empty()) {
                size_t separator = node.find_first_of(NETWORK_SEPARATOR);
                commonNodesSet.insert(node.substr(0, separator));
            }
        }

        // create links matrix
        std::vector<double> links(nodesNum * nodesNum);

        try { // fill links matrix
            auto icost = links.begin();
            for (size_t i = 0; i < nodesNum; ++i) {
                for (size_t j = 0; j < nodesNum; ++j, ++icost) {
                    *icost = std::stod(table[i + 1][j + 1]);
                    if (*icost == 1) *icost = 0;
                }
            }
        }
        catch (std::invalid_argument &e) {
            throw std::invalid_argument ( "Non-float value encountered in the table" );
        }

        { // add common node links
            if (commonNode == ANY_NODE) {
                // link all common nodes
                for (auto &prefix : commonNodesSet) {
                    makeBridgeLinks(links, nodesNum, table.front(), prefix);
                }
            }
			else if (commonNode.find(' ') != std::string::npos)
			{  // link nodes from list
				std::istringstream ss(commonNode);
				std::istream_iterator<std::string> begin(ss);
				std::istream_iterator<std::string> end;
				std::vector <std::string> bridgeNodesVec(begin, end);
				for (auto &prefix : bridgeNodesVec) {
					makeBridgeLinks(links, nodesNum, table.front(), prefix);
				}
			}
			else {
                makeBridgeLinks(links, nodesNum, table.front(), commonNode);
            }
        }

        if (endNode == ANY_NODE) { endNode = startNode; }

        //Network Balance argument handler
        try {
            if (networkBalanceArg != "") {
                size_t separator = networkBalanceArg.find_first_of(":");
                networkBalance.first = std::stoi( networkBalanceArg.substr(0, separator));
                networkBalance.second = std::stoi( networkBalanceArg.substr(separator+1,networkBalanceArg.size()-1));
            }
        }
        catch (std::invalid_argument &e) {
            throw std::invalid_argument ( "Wrong Network Balance argument value [use int:int instead]" );
        }


	    const clock_t begin_time = clock();
        // perform search
        s_type1 outputs;
        search(table.front(), commonNodesSet, links, nodesNum,
               startNode, endNode, outputs, outputsNum, maxPathLen, maxOutputValue);

        // ---------------------------------------------------------------------
        //XXX: network balance filter
        if ( networkBalance.first != 0 || networkBalance.second != 0 )
            filterbyNetworkBalance(outputs, networkBalance, table);
        // ---------------------------------------------------------------------

        std::cout << " Time=" << float( clock () - begin_time ) /  CLOCKS_PER_SEC;
        std::cout << " Output size=" << outputs.size()  << std::endl;

	    { // write results
            std::ofstream foutput(output);
            if (!foutput.is_open()) throw std::runtime_error ( "Failed to open file " + output );

            // iterate results in reverse order (from higher value to lower value)
            for (auto it = outputs.rbegin(); it != outputs.rend(); ++it) {
                foutput << std::fixed << std::setprecision(5) << it->cost << " ( ";
                for (size_t index : it->path) {
                    foutput << table.front()[index + 1] << " ";
                }
                foutput << ")" << std::endl;
            }
        }
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
