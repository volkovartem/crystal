function init() {
    var exchs = ['Binance', 'Okex', 'Bittrex', 'Hitbtc'];
    var exchBody = document.getElementById('exchBody');
    var content = '';
    exchs.forEach(function(exch) {
        content = content + '<div class="row" style="margin-bottom: 15px">'
                              + '<div class="col-sm-6"><h4 style="margin: 0">' + exch + '</h4></div>'
                              + '<div class="col-sm-6" style="text-align: right">'
                                  + '<input id="toggle-' + exch.toLowerCase() + '" type="checkbox" checked data-toggle="toggle" data-size="mini" onchange="update()">'
                              + '</div>'
                          + '</div>'
    });
    exchBody.innerHTML = content;
    initToggles();
    initTable2();
}

function initToggles() {
    $('#toggle-binance').prop('checked', JSON.parse(data['Binance']));
    $('#toggle-okex').prop('checked', JSON.parse(data['Okex']));
    $('#toggle-bittrex').prop('checked', JSON.parse(data['Bittrex']));
    $('#toggle-hitbtc').prop('checked', JSON.parse(data['Hitbtc']));
}


$(document).ready(function(){
//    init()
    document.getElementById('toggle-binance').onchange = function(eventdata) {
        data['Binance'] = eventdata['target']['checked']
        set_setting('activeExchanges', JSON.stringify(data))
    }

    document.getElementById('toggle-okex').onchange = function(eventdata) {
        data['Okex'] = eventdata['target']['checked']
        set_setting('activeExchanges', JSON.stringify(data))
    }

    document.getElementById('toggle-bittrex').onchange = function(eventdata) {
        data['Bittrex'] = eventdata['target']['checked']
        set_setting('activeExchanges', JSON.stringify(data))
    }

    document.getElementById('toggle-hitbtc').onchange = function(eventdata) {
        data['Hitbtc'] = eventdata['target']['checked']
        set_setting('activeExchanges', JSON.stringify(data))
    }
})

function set_setting(param, value) {
    if (param != '' && value != '') {
        $.post("/setSettings", {
            param: param,
            value: value
        });
    }
}

function apply(id) {
    item = document.getElementById('i' + id)
    var value = document.getElementById('t' + id).value
    set_setting(id, value)
    item.classList.remove('fa-square-o')
    item.classList.add('fa-check-square-o')
}

function apply2() {
    item = document.getElementById('iapply2')
    var volume_value = document.getElementById('volume').value
    var timeout_value = document.getElementById('timeout').value
    set_setting('volume', volume_value)
    set_setting('timeout', timeout_value)
    item.classList.remove('fa-square-o')
    item.classList.add('fa-check-square-o')
}

function apply4() {
    item = document.getElementById('iapply4')
    var density_value = document.getElementById('density').value
    var frequency_value = document.getElementById('frequency').value
    var time_value = document.getElementById('time').value
    params = {}
    params['density'] = density_value
    params['frequency'] = frequency_value
    params['time'] = time_value
    var premoving = JSON.stringify(params)
    set_setting('premoving', premoving)
    item.classList.remove('fa-square-o')
    item.classList.add('fa-check-square-o')
}

function apply3() {
    item = document.getElementById('iapply3')
    var walletPercent = document.getElementById('walletPercent').value
    var amount_min_btc = document.getElementById('amount_min_btc').value
    var amount_min_eth = document.getElementById('amount_min_eth').value
    var amount_min_usdt = document.getElementById('amount_min_usdt').value
    var amount_max_btc = document.getElementById('amount_max_btc').value
    var amount_max_eth = document.getElementById('amount_max_eth').value
    var amount_max_usdt = document.getElementById('amount_max_usdt').value

    strs = {}
    strs['amount_min_btc'] = amount_min_btc
    strs['amount_min_eth'] = amount_min_eth
    strs['amount_min_usdt'] = amount_min_usdt
    strs['amount_max_btc'] = amount_max_btc
    strs['amount_max_eth'] = amount_max_eth
    strs['amount_max_usdt'] = amount_max_usdt

    set_setting('trade_limits', JSON.stringify(strs))
    set_setting('walletPercent', walletPercent)
    item.classList.remove('fa-square-o')
    item.classList.add('fa-check-square-o')
}

function range() {
    var value = document.getElementById('formControlRange').value
    document.getElementById('walletPercent').value = value
}