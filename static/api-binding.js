function init(data) {
    var tabs = ['Binance', 'Okex', 'Bittrex', 'Hitbtc'];
    var body = document.getElementById('v-pills-tabContent1');
    var header = document.getElementById('v-pills-tab');
    var header_content = '';
    var content = '';
    tabs.forEach(function(tab) {
        var block = ''
        var status = '';
        var h_status = '';
        var displayPassPhrase = 'none'
        var displayFundPwd = 'none'
        if (tab == 'Binance') {
            status = 'show active';
            h_status = 'active';
        }


        if (tab == 'Okex') {
           displayPassPhrase = 'block'
        }
        if (tab == 'Okex') {
           displayFundPwd = 'block'
        }

        block = '<div class="form-group" style="display: ' + displayFundPwd + '"><label for="' + tab + '-fundPwd">Fund password:</label><input type="password" class="form-control" id="' + tab + '-fundPwd" value="' + data[tab]['fundPwd'] + '"></div> <div class="form-group" style="display: ' + displayPassPhrase + '"><label for="' + tab + '-passPhrase">Okexphrase:</label><input type="password" class="form-control" id="' + tab + '-passPhrase" value="' + data[tab]['passPhrase'] + '"></div>'
        header_content = header_content + '<a class="nav-link ' + h_status + '" id="v-pills-' + tab.toLowerCase() + '-tab" data-toggle="pill" href="#v-pills-' + tab.toLowerCase() + '" role="tab" aria-controls="v-pills-' + tab.toLowerCase() + '" aria-selected="true"><i id="' + tab + '-label" class="fa ' + data[tab]['label_class'] + '"></i>  ' + tab.toUpperCase() + '</a>';
        content = content + '<div class="tab-pane fade' + status + '" id="v-pills-' + tab.toLowerCase() + '" role="tabpanel" aria-labelledby="v-pills-' + tab.toLowerCase() + '-tab"><h2>' + tab + '</h2><div class="form-group"><label for="' + tab + '-apiKey">ApiKey:</label><input type="password" class="form-control" id="' + tab + '-apiKey" value="' + data[tab]['apiKey'] + '"></div><div class="form-group"><label for="' + tab + '-secretKey">SecretKey:</label><input type="password" class="form-control" id="' + tab + '-secretKey" value="' + data[tab]['secretKey'] + '"></div>' + block + '<div class="row"><div class="col-sm-2"><div class="form-group"><label for="' + tab + '-buyFees">buy fees (%):</label><input type="text" class="form-control" id="' + tab + '-buyFees" value="' + data[tab]['buyFees'] + '"></div><div class="form-group"><label for="' + tab + '-sellFees">sell fees (%):</label><input type="text" class="form-control" id="' + tab + '-sellFees" value="' + data[tab]['sellFees'] + '"></div></div><div class="col-sm-10"></div></div><div class="row"><div class="col-md-6"></div><div class="col-md-6 text-right"><button class="btn btn-primary btn-sm" type="submit" style="margin: 5px" onclick="clearButton(\'' + tab + '\')"><i class="fa fa-trash-o"></i> clean</button><button class="btn btn-primary btn-sm" style="margin: 5px" type="submit" onclick="testButton(\'' + tab + '\')"><i class="fa fa-plug"></i> test API</button><button class="btn btn-primary btn-sm" style="margin: 5px" onclick="btnSave(\'' + tab + '\')" type="submit"><i class="fa fa-cloud"></i> save</button><p id="' + tab + '-status"></p></div></div></div>'
    });
    body.innerHTML = content;
    header.innerHTML = header_content;
}

function clearButton(exch) {
    document.getElementById(exch + '-apiKey').value = '';
    document.getElementById(exch + '-secretKey').value = '';
    document.getElementById(exch + '-buyFees').value = '';
    document.getElementById(exch + '-sellFees').value = '';
    document.getElementById(exch + '-fundPwd').value = '';
    document.getElementById(exch + '-passPhrase').value = '';
    $.post("/buttonDelete", {
        exchange: exch
    })
}

function testButton(exch) {
    var apiKey = document.getElementById(exch + '-apiKey').value;
    var secretKey = document.getElementById(exch + '-secretKey').value;
    var buyFees = document.getElementById(exch + '-buyFees').value;
    var sellFees = document.getElementById(exch + '-sellFees').value;
//    var buyFees = document.getElementById(exch + '-fundPwd').value;
//    var sellFees = document.getElementById(exch + '-passPhrase').value;
    if (apiKey !== '' && secretKey !== '' && buyFees !== '' && sellFees !== '') {
        $.post("/testApi", {
            exchange: exch
        }, testResponce)
    }
}

function testResponce(data) {
    if (data.includes('failed')) {
        document.getElementById(data.split('-')[0] + '-label').classList.remove('fa-check-square-o')
        document.getElementById(data.split('-')[0] + '-label').classList.add('fa-square-o')
        document.getElementById(data.split('-')[0] + '-status').innerHTML = data.split('-')[0] + ' test: Failed'
    } else {
        document.getElementById(data + '-label').classList.remove('fa-square-o')
        document.getElementById(data + '-label').classList.add('fa-check-square-o')
        document.getElementById(data + '-status').innerHTML = data + ' test: OK'
    }
}

function btnSave(exch) {
    var apiKey = document.getElementById(exch + '-apiKey').value;
    var secretKey = document.getElementById(exch + '-secretKey').value;
    var buyFees = document.getElementById(exch + '-buyFees').value;
    var sellFees = document.getElementById(exch + '-sellFees').value;
    var fundPwd = document.getElementById(exch + '-fundPwd').value;
    var passPhrase = document.getElementById(exch + '-passPhrase').value;
    if (apiKey !== '' && secretKey !== '' && buyFees !== '' && sellFees !== '') {
        $.post("/buttonSave", {
            exchange: exch,
            apiKey: apiKey,
            secretKey: secretKey,
            buyFees: buyFees,
            sellFees: sellFees,
            fundPwd: fundPwd,
            passPhrase: passPhrase
        }, onAjaxSuccess)
    }
}

function onAjaxSuccess(data) {
    document.getElementById(data + '-status').innerHTML = data + ' saved';
}