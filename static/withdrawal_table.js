function initTable() {
    $table.bootstrapTable({
        height: getHeight(),
        data: data,
        columns: [
                [
                    {
                        title: 'id',
                        field: 'id',
                        rowspan: 2,
                        align: 'center',
                        valign: 'middle',
                        sortable: true,
                        footerFormatter: totalTextFormatter
                    },
                    {
                        field: 'description',
                        title: 'exchange',
                        sortable: true,
                        footerFormatter: totalNameFormatter,
                        align: 'center'
                    },
                    {
                        field: 'token',
                        title: 'currency',
                        sortable: true,
                        align: 'center',
                        footerFormatter: totalPriceFormatter
                    },
                    {
                        field: 'amount',
                        title: 'amount',
                        sortable: true,
                        align: 'center',
                        footerFormatter: totalPriceFormatter
                    },
                    {
                        field: 'txid',
                        title: 'withdrawal ID',
                        sortable: true,
                        align: 'center',
                        footerFormatter: totalPriceFormatter
                    },
                    {
                        field: 'time',
                        title: 'time',
                        sortable: true,
                        align: 'center',
                        footerFormatter: totalPriceFormatter
                    },
                    {
                        field: 'status',
                        title: 'status',
                        sortable: true,
                        align: 'center',
                        footerFormatter: totalPriceFormatter
                    },
                ],
                [

                ]
            ]
    });
    // sometimes footer render error.
    setTimeout(function() {
        $table.bootstrapTable('resetView');
    }, 200);
    $table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function() {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        // save your data, here just save the current page
        selections = getIdSelections();
        // push or splice the selections if you want to save all data selections
    });
    $table.on('expand-row.bs.table', function(e, index, row, $detail) {
        if (index % 2 == 1) {
            $detail.html('Loading from ajax request...');
            $.get('LICENSE', function(res) {
                $detail.html(res.replace(/\n/g, '<br>'));
            });
        }
    });
    $table.on('all.bs.table', function(e, name, args) {
        console.log(name, args);
    });
    $remove.click(function() {
        var ids = getIdSelections();
        removeSetting(ids)
        $table.bootstrapTable('remove', {
            field: 'id',
            values: ids
        });
        $remove.prop('disabled', true);
    });
    $(window).resize(function() {
        $table.bootstrapTable('resetView', {
            height: getHeight()
        });
    });
}

function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function(row) {
        return row.id
    });
}

function responseHandler(res) {
    $.each(res.rows, function(i, row) {
        row.state = $.inArray(row.id, selections) !== -1;
    });
    return res;
}

function detailFormatter(index, row) {
    var html = [];
    $.each(row, function(key, value) {
        html.push('<p><b>' + key + ':</b> ' + value + '</p>');
    });
    return html.join('');
}

function operateFormatter(value, row, index) {
    return ['<a class="remove" href="javascript:void(0)" title="Remove">', '<i class="fa fa-trash"></i>', '</a>'].join('');
}
window.operateEvents = {
    'click .remove': function(e, value, row, index) {
        removeSetting([row.id])
        $table.bootstrapTable('remove', {
            field: 'id',
            values: [row.id]
        });
    }
};

function totalTextFormatter(data) {
    return 'Total';
}

function totalNameFormatter(data) {
    return data.length;
}

function totalPriceFormatter(data) {
    var total = 0;
    $.each(data, function(i, row) {
        total += +(row.price.substring(1));
    });
    return '$' + total;
}

function getHeight() {
    return $(window).height() - $('h1').outerHeight(true);
}

function apply() {
    item = document.getElementById('ide')
    var value = document.getElementById('tde').value
    $.post("/setSettings", {
            param: 'deposit_expiration',
            value: value
        });
    item.classList.remove('fa-square-o')
    item.classList.add('fa-check-square-o')
}