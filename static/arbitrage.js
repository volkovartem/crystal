function load_history(data) {
    his = document.getElementById('his')
    strr = ''
    for (var dt in data) {
    var opid = "'" + data[dt]['opid'] + "'"
    var description = data[dt]['description']
    var status = data[dt]['status']
    var clr = 'red'
    if (status === 'completed') {
        clr = 'green'
    }
    strr = strr + '<div class="history" style="padding: 5px;'
     + 'font-size: 12px" onclick="update(' + String(opid) + ')"><label style="font-weight: normal">' + description + '</label><label style="font-weight: normal; float: right; color: ' + clr + '">' + status + '</label></div>'
}
    his.innerHTML = strr
}

function initTable(data) {
    $table.bootstrapTable({
        height: getHeight(),
        data: data,
        columns: [
            [{
                field: 'exchange',
                title: 'exchange',
                sortable: true,
                footerFormatter: totalNameFormatter,
                align: 'center'
            }, {
                field: 'symbol',
                title: 'symbol',
                sortable: true,
                align: 'center',
                footerFormatter: totalPriceFormatter
            }, {
                field: 'orderId',
                title: 'orderId',
                sortable: true,
                align: 'center',
                footerFormatter: totalPriceFormatter
            }, {
                field: 'amount',
                title: 'amount',
                sortable: true,
                align: 'center',
                footerFormatter: totalPriceFormatter
            }, {
                field: 'side',
                title: 'side',
                sortable: true,
                align: 'center',
                footerFormatter: totalPriceFormatter
            }, {
                field: 'status',
                title: 'status',
                sortable: true,
                align: 'center',
                footerFormatter: totalPriceFormatter
            }, {
                field: 'time',
                title: 'time',
                sortable: true,
                align: 'center',
                footerFormatter: totalPriceFormatter
            }, ],
            []
        ]
    });
    // sometimes footer render error.
    setTimeout(function() {
        $table.bootstrapTable('resetView');
    }, 200);
    $table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function() {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        // save your data, here just save the current page
        selections = getIdSelections();
        // push or splice the selections if you want to save all data selections
    });
    $table.on('expand-row.bs.table', function(e, index, row, $detail) {
        if (index % 2 == 1) {
            $detail.html('Loading from ajax request...');
            $.get('LICENSE', function(res) {
                $detail.html(res.replace(/\n/g, '<br>'));
            });
        }
    });
    $table.on('all.bs.table', function(e, name, args) {
        console.log(name, args);
    });
    $(window).resize(function() {
        $table.bootstrapTable('resetView', {
            height: getHeight()
        });
    });
}

function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function(row) {
        return row.id
    });
}

function responseHandler(res) {
    $.each(res.rows, function(i, row) {
        row.state = $.inArray(row.id, selections) !== -1;
    });
    return res;
}

function detailFormatter(index, row) {
    var html = [];
    $.each(row, function(key, value) {
        html.push('<p><b>' + key + ':</b> ' + value + '</p>');
    });
    return html.join('');
}

function operateFormatter(value, row, index) {
    return ['<a class="remove" href="javascript:void(0)" title="Remove">', '<i class="fa fa-trash"></i>', '</a>'].join('');
}
window.operateEvents = {
    'click .remove': function(e, value, row, index) {
        removeSetting([row.id])
        $table.bootstrapTable('remove', {
            field: 'id',
            values: [row.id]
        });
    }
};

function totalTextFormatter(data) {
    return 'Total';
}

function totalNameFormatter(data) {
    return data.length;
}

function totalPriceFormatter(data) {
    var total = 0;
    $.each(data, function(i, row) {
        total += +(row.price.substring(1));
    });
    return '$' + total;
}

function getHeight() {
    return $(window).height() - $('h1').outerHeight(true);
}

function update(opid) {
    for (var dt in data) {
        if (data[dt]['opid'] === String(opid)) {
            var trades1 = JSON.parse(data[dt]["data"])['trades1']
            var trades2 = JSON.parse(data[dt]["data"])['trades2']
            $table.bootstrapTable("load", trades1.concat(trades2));

//            console.log(trades1.concat(trades2))
            update_info(data[dt]['description'], JSON.parse(data[dt]["data"])['gross'], JSON.parse(data[dt]["data"])['stake'], JSON.parse(data[dt]["data"])['fees'], JSON.parse(data[dt]["data"])['wfees'], JSON.parse(data[dt]["data"])['profit'], data[dt]['time'])
        }
    }
};

function update_info(d, gross, stake, fees, wfee2, profit, time) {
    var h = document.getElementById('trades_h');
    h.innerText = d
    var targ = document.getElementById('info');
    str = '<div class="col-sm-2"><span>Gross return = </span><span id="gross">' + gross + '</span></div><div class="col-sm-2"><span>Stake = </span> <span id="stake">' + stake + '</span></div><div class="col-sm-2"><span>Fees = </span> <span id="fees">' + fees +'</span></div><div class="col-sm-2"><span>WFees = </span> <span id="wfee2">' + wfee2 + '</span></div><div class="col-sm-2"><span>Actual profit = </span> <span id="profit">' + profit + '</span></div><div class="col-sm-2"><span>Time </span><span id="time">' + time + '</span></div>'
    targ.innerHTML = str
}