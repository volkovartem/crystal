function initTable2() {
    $table2.bootstrapTable({
        height: getHeight(),
        data: data2,
        columns: [
            [{
                field: 'token',
                title: 'Coin',
                sortable: true,
                footerFormatter: totalNameFormatter,
                align: 'center'
            },{
                field: 'Binance',
                title: 'Binance',
                sortable: true,
                footerFormatter: totalNameFormatter,
                align: 'center'
            },{
                field: 'Bittrex',
                title: 'Bittrex',
                sortable: true,
                footerFormatter: totalNameFormatter,
                align: 'center'
            },{
                field: 'Okex',
                title: 'Okex',
                sortable: true,
                footerFormatter: totalNameFormatter,
                align: 'center'
            },{
                field: 'Hitbtc',
                title: 'Hitbtc',
                sortable: true,
                footerFormatter: totalNameFormatter,
                align: 'center'
            },],
            []
        ]
    });
    // sometimes footer render error.
    setTimeout(function() {
        $table2.bootstrapTable('resetView');
    }, 200);
    $table2.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function() {
        $remove2.prop('disabled', !$table2.bootstrapTable('getSelections').length);
        // save your data, here just save the current page
        selections = getIdSelections();
        // push or splice the selections if you want to save all data selections
    });
    $table2.on('expand-row.bs.table', function(e, index, row, $detail) {
        if (index % 2 == 1) {
            $detail.html('Loading from ajax request...');
            $.get('LICENSE', function(res) {
                $detail.html(res.replace(/\n/g, '<br>'));
            });
        }
    });
    $table2.on('all.bs.table', function(e, name, args) {
        console.log(name, args);
    });
    $remove2.click(function() {
//        alert('here')
//        var  ids = getIdSelections();
//        removeSetting(ids)
//        $table2.bootstrapTable('remove', {
//            field: 'id',
//            values: ids
//        });
//        $remove2.prop('disabled', true);
    });
    $(window).resize(function() {
        $table2.bootstrapTable('resetView', {
            height: getHeight()
        });
    });
}

function getIdSelections() {
    return $.map($table2.bootstrapTable('getSelections'), function(row) {
        return row.id
    });
}

function responseHandler(res) {
    $.each(res.rows, function(i, row) {
        row.state = $.inArray(row.id, selections) !== -1;
    });
    return res;
}

function detailFormatter(index, row) {
    var html = [];
    $.each(row, function(key, value) {
        html.push('<p><b>' + key + ':</b> ' + value + '</p>');
    });
    return html.join('');
}

function operateFormatter(value, row, index) {
    return ['<a class="remove" href="javascript:void(0)" title="Remove">', '<i class="fa fa-trash"></i>', '</a>'].join('');
}
window.operateEvents = {
    'click .remove': function(e, value, row, index) {
        var sata = [];
        for (var x = 0; x < data2.length; x++) {
            if (x !== index) {
                sata.push(data2[x])
            }
        }

        $.post("/setSettings", {
                param: 'blacklist',
                value: JSON.stringify(sata)
            }, respFunc);

        data2 = sata

        $table2.bootstrapTable('remove', {
            field: 'id',
            values: [row.id]
        });
    }
};

function totalTextFormatter(data) {
    return 'Total';
}

function totalNameFormatter(data) {
    return data2.length;
}

function totalPriceFormatter(data) {
    var total = 0;
    $.each(data2, function(i, row) {
        total += +(row.price.substring(1));
    });
    return '$' + total;
}

function getHeight() {
    return $(window).height() - $('h1').outerHeight(true);
}




