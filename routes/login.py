from routes import routes
from flask import render_template, request, redirect, session, flash


@routes.route('/login', methods=['GET'])
def login():
    return render_template('login.html')


@routes.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == 'password' and request.form['username'] == 'admin':
        session['logged_in'] = True
        return redirect('/')
    else:
        flash('wrong password!')


@routes.route("/logout")
def logout():
    session['logged_in'] = False
    return redirect('/login')

