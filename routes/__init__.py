from flask import Blueprint
routes = Blueprint('routes', __name__)


from routes.pages.home import *
from routes.pages.api_binding import *
from routes.pages.arbitrage import *
from routes.pages.settings import *
from routes.pages.withdrawal import *
from routes.pages.balance import *
from routes.pages.stats import *
from routes.control.binding import *
from routes.control.logging import *
from routes.control.start_button import *
from routes.control.updates import *
from routes.downloads import *
from routes.login import *



