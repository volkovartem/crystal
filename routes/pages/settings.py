import json
from routes import routes
from flask import render_template
from database.services.settings import get_settings

AllExchanges = ['Binance', 'Okex', 'Bittrex', 'Hitbtc']


@routes.route("/settings")
# @authenticate
def settings():
    shcommand = get_settings('script')[0]['value']
    email = get_settings('email')[0]['value']
    volume = get_settings('volume')[0]['value']
    timeout = get_settings('timeout')[0]['value']
    walletPercent = get_settings('walletPercent')[0]['value']
    trade_limits = json.loads(get_settings('trade_limits')[0]['value'])
    data2 = json.loads(get_settings('blacklist')[0]['value'])
    data = json.loads(get_settings('activeExchanges')[0]['value'])
    premoving = json.loads(get_settings('premoving')[0]['value'])

    return render_template("settings.html",
                           data=data,
                           data2=data2,
                           email=email,
                           command=shcommand,
                           volume=volume,
                           timeout=timeout,
                           density=premoving['density'],
                           frequency=premoving['frequency'],
                           time=premoving['time'],
                           amount_min_btc=trade_limits['amount_min_btc'],
                           amount_min_eth=trade_limits['amount_min_eth'],
                           amount_min_usdt=trade_limits['amount_min_usdt'],
                           amount_max_btc=trade_limits['amount_max_btc'],
                           amount_max_eth=trade_limits['amount_max_eth'],
                           amount_max_usdt=trade_limits['amount_max_usdt'],
                           walletPercent=walletPercent)