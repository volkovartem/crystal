from routes import routes
from flask import render_template
from routes.authenticate import authenticate


@routes.route('/')
# @authenticate
def home():
    return render_template('home.html')
