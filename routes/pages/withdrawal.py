import json
from routes import routes
from flask import render_template
from database.services.transfers import *
from database.services.settings import get_settings


@routes.route("/withdrawal")
# @authenticate
def withdrawal():
    data = get_transfers()
    data2 = json.loads(get_settings('coins')[0]['value'])
    deposit_expiration = json.loads(get_settings('deposit_expiration')[0]['value'])
    return render_template("withdrawal.html", data=data, data2=data2, deposit_expiration=deposit_expiration)
