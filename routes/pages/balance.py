import trade_utils
from routes import routes
from flask import render_template


@routes.route("/balance")
# @authenticate
def balance():
    data = trade_utils.get_available_balance()
    return render_template("balance.html", data2=data)
