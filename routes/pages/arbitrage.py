from routes import routes
from flask import render_template
from database.services.arbitrage import *


@routes.route("/arbitrage")
# @authenticate
def arbitrage():
    data = get_arbitrages()
    return render_template("arbitrage.html", data=data)
