from routes import routes
from flask import render_template
from database.services.api import get_api
from routes.authenticate import authenticate

AllExchanges = ['Binance', 'Okex', 'Bittrex', 'Hitbtc']


@routes.route("/api-binding")
# @authenticate
def api_binding():
    data = {}
    for exchange in AllExchanges:
        api = get_api(exchange)
        status = 'fa-square-o'
        if api[6] == 'True':
            status = 'fa-check-square-o'
        exch = {exchange: {'apiKey': api[0],
                           'secretKey': api[1],
                           'buyFees': api[2],
                           'sellFees': api[3],
                           'fundPwd': api[4],
                           'passPhrase': api[5],
                           'label_class': status}}
        data.update(exch)

    return render_template('api-binding_v2.html', data=data)