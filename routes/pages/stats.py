from routes import routes
from flask import render_template
from database.services.settings import get_settings


@routes.route("/stats")
# @authenticate
def stats():
    return render_template("stats.html")
