from routes import routes
from flask import send_file


@routes.route("/input.csv")
def DownloadInput():
    try:
        return send_file('calculator/rel/input.csv', as_attachment=True)
    except Exception as e:
        print(e.args)
        return ''


@routes.route("/output.csv")
def DownloadOutput():
    try:
        return send_file('calculator/rel/output.csv', as_attachment=True)
    except Exception as e:
        print(e.args)
        return ''
