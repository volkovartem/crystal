import restWrapper as wp
from routes import routes
from flask import request
from database.services.api import *


@routes.route('/buttonSave', methods=["POST"])
def button():
    exchange = request.form['exchange']
    apiKey = request.form['apiKey']
    secretKey = request.form['secretKey']
    buyFees = request.form['buyFees']
    sellFees = request.form['sellFees']
    fundPwd = request.form['fundPwd']
    passPhrase = request.form['passPhrase']
    add_api(exchange, apiKey, secretKey, buyFees, sellFees, fundPwd, passPhrase)
    wp.connect_api(exchange)
    return exchange


@routes.route('/buttonDelete', methods=["POST"])
def buttonDelete():
    exchange = request.form['exchange']
    remove_api(exchange)
    # wp.connect_api(exchange)


@routes.route('/testApi', methods=["POST"])
def buttonTestApi():
    exchange = request.form['exchange']
    if wp.get_balance(exchange, 'BTC') is not None:
        set_status_api(exchange, 'True')
        return exchange
    set_status_api(exchange, 'False')
    return exchange + '-failed'
