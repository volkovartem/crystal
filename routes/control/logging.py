import json
from routes import routes
from database.services.logging import *
from pymemcache.client.base import Client


@routes.route("/getLog", methods=["GET"])
def getLog():
    logString = ''
    lines = get_log()
    for l in lines:
        logString += l['time'] + ':   ' + l['line'] + '\n'

    return logString


@routes.route("/getThreadsLog", methods=["GET"])
def getThreadsLog():
    logString = ''
    lines = get_threads_log()
    for l in lines:
        logString += l['time'] + ':   ' + l['line'] + '\n'

    return logString


@routes.route("/getOpps", methods=["GET"])
def getOpps():
    client = Client(('127.0.0.1', 11211))
    req = client.get('Arbitrage')
    if req is not None:
        # print(req)
        opps = json.loads(req.decode())
        lenth = len(opps)
        lines = ''
        for op in opps:
            chains = ''
            for exch in list(opps[op].keys())[:-1]:
                cline = ''
                for node in opps[op][exch]:
                    cline = cline + node + ' > '
                chains = chains + ' | {} - {}'.format(exch, cline.strip(' >'))
            lines += '\n{} {}'.format(opps[op]['profit'], chains)

        inf = client.get('OppLogInfo')
        if inf is not None:
            inf = inf.decode().split('|')[0]
        lines = '\n' + '<p style="font-size: 14px"><b>Opportunities ({})</b></p><p style="font-size: 12px">{}</p>'.format(
            lenth, inf) + lines

        return lines
    return str(req)


@routes.route("/getAnyLog", methods=["GET"])
def getAnyLog():
    logString = ''
    lines = get_log()
    for l in lines:
        logString += l['time'] + ':   ' + l['line'] + '\n'
    return logString


@routes.route("/clearErrLog", methods=["GET"])
def clearErrLog():
    clearLog()
    return 'done'
