import utils
from routes import routes
from flask import request
from database.services.control import *
from database.services.logging import log


@routes.route('/state', methods=["POST"])
def state():
    text = request.form['text']
    set_state(text)
    log(text)
    return text


@routes.route('/getState', methods=["GET"])
def getState():
    return get_state()