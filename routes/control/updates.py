import json
import utils
from routes import routes
from flask import request
from database.services.settings import *


@routes.route('/setSettings', methods=["POST"])
def setSettings():
    param = request.form['param']
    value = request.form['value']
    add_setting(param, value)
    return value


@routes.route('/getSettings', methods=["POST"])
def getSettings():
    param = request.form['param']
    data = get_settings(param)[0]['value']  # string
    return data

@routes.route('/updateCoins', methods=["POST"])
def updateCoins():
    param = request.form['param']
    value = False if request.form['value'] == 'false' else True
    data = request.form['data']
    for dt in json.loads(data):
        update_coin(dt['coin'], param, value)
    return get_settings('coins')[0]['value']
