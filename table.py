import csv

# initial
table = [[' ']]


def init():
    global table
    table = [[' ']]


def show():
    for row in table:
        for column in row:
            print(column, end="")
        print(end="\n")


def get_top_row():
    return table[0][1:]


def get_left_column():
    result = []
    for row in table:
        result.append(row[0])
    return result[1:]


def add_row(name):
    l = len(get_left_column())
    a = [name]
    for x in range(l):
        a.append(1)
    table.append(a)


def add_column(name):
    table[0].append(name)
    for row in table[1:]:
        row.append(1)


def set(row_name, column_name, value):
    row_index = 0
    column_index = 0
    for row in table:
        if row[0] == row_name:
            row_index = table.index(row)
    for x in table[0]:
        if x == column_name:
            column_index = table[0].index(x)
    table[row_index][column_index] = value


def save():
    # /root/crystal/calculator/rel/input.csv
    with open('input.csv', 'w', newline='') as csvfile:
        fieldnames = table[0]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in table[1:]:
            data = {' ': row[0]}
            names = get_top_row()
            for x in range(len(names)):
                data.update({names[x]: row[1:][x]})
            # print(data)
            writer.writerow(data)


def get_table():
    return table


def get_output():
    # /root/crystal/calculator/rel/output.csv
    with open('output.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        result = []
        if reader.fieldnames is not None:
            result.append(reader.fieldnames[0])
            for row in reader:
                result.append(dict(row)[reader.fieldnames[0]])
        return result

# add_row('A')
# add_column('A')
# add_row('B')
# add_column('B')
# add_row('C')
# add_column('C')
# add_row('D')
# add_column('D')
# set('B', 'C', 2)
# show()
# save()
