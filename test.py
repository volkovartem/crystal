# import sys
# sys.path.append('/root/crystal')
import gzip
import json
import threading
import time
import traceback
import zlib

import websocket
from pymemcache.client.base import Client

from database.services.logging import log
from database.services.settings import get_settings

# settings
filter_check_timeout = 10
client = Client(('127.0.0.1', 11211))

orderbook = {}
name = 'Okex'


def inflate(data):
    decompress = zlib.decompressobj(
        -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


def send_message(ws, message_dict):
    data = json.dumps(message_dict).encode()
    ws.send(data)


f = 0
s = 0
th = 0
tstart = time.time()
restart = 0
count = 0


def on_message(ws, message):
    try:
        now = time.time()
        global f
        global s
        global th
        global restart
        message = inflate(message)
        channel = json.loads(message)[0]['channel']
        if channel != 'addChannel':
            symbol = (channel.split('_')[3] + '_' + channel.split('_')[4]).upper()
            # print(symbol)
            data = json.loads(message)[0]['data']
            client.set(symbol.replace('_', '') + '_' + name, json.dumps(data))
            diff = round(now - (float(data['timestamp']) / 1000), 2)
            # print(time.time(), (float(data['timestamp']) / 1000))
            if diff < 1:
                f += 1
            if 2 > diff > 1:
                s += 1
            if diff > 2:
                th += 1
            summ = f + s + th
            _f = int(f / summ * 100)
            _s = int(s / summ * 100)
            _th = int(th / summ * 100)

            print('diff', diff, 'stats', f, s, th, 'percent', _f, _s, _th, 'restart', restart)

            # if diff > 2 and time.time() > tstart + 30: info.append('{} {} {} {} {} {} {} {} {} {} {} {} {}'.format(
            # time.time(), 'diff', diff, 'stats', f, s, th, 'percent', _f, _s, _th, 'restart', restart))
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(error):
    error = gzip.decompress(error).decode()
    print(error)


def on_close(ws):
    print("### closed ###")
    global restart
    restart += 1


def on_open(ws):
    def run(*args):
        symbs = fltr
        data = []
        send_message(ws, data)
        # symbs = symbs[:120]
        print(len(symbs), symbs)

        for sr in symbs:
            data.append({'event': 'addChannel', 'channel': 'ok_sub_spot_{}_depth_10'.format(sr)})
        send_message(ws, data)

    t = threading.Thread(target=run, args=())
    t.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    fltr = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(fltr))
    print(json.dumps(fltr))
    client.set('filter_' + name, json.dumps(fltr))
    try:
        ws = websocket.WebSocketApp(
            "wss://real.okex.com:10441/websocket",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close
        )

        tstart = time.time()

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
