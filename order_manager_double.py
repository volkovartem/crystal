import time
import traceback

import order_manager
import premoving
import trade_utils
import utils
from database.services.arbitrage import *
from database.services.logging import *

# settings
return_threshold = 'No threshold'
expiration_time = 20  # seconds


def calc_stake(trades):
    client = Client(('127.0.0.1', 11211))
    stake = 0
    for trade in trades:
        rm = client.get(trade['exchange'] + 'Markets')
        if rm is not None:
            rm = json.loads(rm.decode())
            smb = utils.split_symbol(trade['symbol'])
            stake += trade['amount'] * trade_utils.convert(trade['exchange'],
                                                           smb['s'] if trade['side'] == 'SELL' else smb['b'],
                                                           'USDT', rm)
    return round(stake, 5)


def statement(trades, amount, mark):
    opid = str(time.time())
    description = ('Pre transfer. Chain = ' + str(mark)).replace(
        "'", "").replace('[', '').replace(']', '').replace(', ', ' ')
    fees = 0
    for x in trades:
        fees = fees + float(trade_utils.get_trading_fees(x['exchange'])['buyFees'])

    stake = calc_stake(trades)

    _data = {'gross': '', 'stake': str(stake) + ' USDT', 'fees': str(fees) + '%',
             'wfees': '-', 'profit': '-', 'trades1': trades, 'trades2': []}

    status = 'completed' if amount is not None else 'failed'
    add_arbitrage(opid, description, json.dumps(_data), utils.get_time(), status)


# data = {'Binance': ['LTC', 'USDT', 'BTC'], 'Okex': ['BTC', 'USDT', 'ETH', 'LTC'], 'profit': '1.01691'}


# print(allow_premoving(data))
# t = time.time()
# print(order_manager.get_amount(data, percent=True))
# print(order_manager.get_amount(data, mode=True))
# print(time.time() -t )

def pre_calculating(data, amount, _amount, mark):
    print(data, amount, _amount)
    exchange1 = list(data.keys())[0]
    exchange2 = list(data.keys())[1]
    trade_fee1 = trade_utils.get_trading_fees(exchange1)['buyFees']
    trade_fee2 = trade_utils.get_trading_fees(exchange2)['buyFees']
    wfee1 = utils.get_fee_fast(exchange1, data[exchange1][0])
    wfee2 = utils.get_fee_fast(exchange2, data[exchange2][0])
    profit = float(data['profit'])

    if trade_fee1 is None or trade_fee2 is None or wfee1 is None or wfee2 is None:
        return False

    wfee = {exchange1: float(wfee1),
            exchange2: float(wfee2)}
    profit -= round(((float(trade_fee1) * len(data[exchange1][-1])) / 100) \
                    + ((float(trade_fee2) * len(data[exchange2][-1])) / 100) + 1, 5)

    xamount = amount
    if amount is None:
        if _amount is not None:
            xamount = _amount
        else:
            return False

    res = {}
    for x in xamount:
        res.update({x: (float(xamount[x]) * profit) - wfee[x]})

    coeff = trade_utils.calc_coeff_chain(exchange1, data[exchange1])
    if coeff is None:
        return False
    if (res[exchange1] * coeff) + res[exchange2] > 0:
        return True
    else:
        print_arb_log(['Arbitrage failed', 'Reason: {}'.format('Profit is not enough to pay a withdrawal fee'),
                       'Withdrawal fee: {} {}, {} {}'.format(wfee[exchange1],
                                                             data[exchange1][0],
                                                             wfee[exchange2],
                                                             data[exchange2][0]),
                       'Expected profit {} {}, {} {}'.format(round(float(xamount[exchange1]) * profit, 6),
                                                             data[exchange1][0],
                                                             round(float(xamount[exchange2]) * profit, 6),
                                                             data[exchange2][0])], str(mark))
        return False


# print(pre_calculating(data, None, {'Binance': 3.15, 'Okex': 0.042}))


def check(data, mark, op):
    try:
        print(data, mark)
        exchange1 = list(data.keys())[0]
        exchange2 = list(data.keys())[1]
        amount = order_manager.get_amount(data, percent=True)
        _amount = order_manager.get_amount(data, mode=True)

        precalc = pre_calculating(data, amount, _amount, mark)

        if precalc is None or precalc is False:
            return

        if order_manager.allow_premoving(data):
            if amount is None and _amount is not None:
                trades = premoving.pre_moving(data, _amount)
                print('trades', trades)
                time.sleep(2)
                if trades is not None:
                    amount = order_manager.get_amount(data)
                    if trades:
                        for trade in trades:
                            symbol = trade['symbol']
                            base = utils.split_symbol(symbol)['s']
                            quote = utils.split_symbol(symbol)['b']
                            utils.add_history(base)
                            utils.add_history(quote)
                        statement(trades, amount, mark)
                    else:
                        print('Pre moving is NOT required')

        print('Amount ', amount)
        print('----------------------------')
        # input('CONTINUE')
        # return
        # time.sleep(11111)
        if amount is not None:
            result1 = order_manager.simulation(exchange1, data[exchange1], amount, mark)
            if result1 is not None:
                print(result1)
                print('Amount ', result1[1])
                print('----------------------------')
                result2 = order_manager.simulation(exchange2, data[exchange2], result1[1], mark)
                print(result2)
                profit = utils.round_dwn(result2[1] - amount, 5)
                print('profit', profit)
                trade_fees = result1[2] + result2[2]
                print('trade_fees', trade_fees)
                result1[0].update(result2[0])
                print('real_prices', result1[0])
                amounts = {exchange1: utils.round_dwn(amount, 5), exchange2: utils.round_dwn(result1[1], 5)}
                print('amounts', amounts)
                wfees = order_manager.get_wfees(exchange1, data[exchange1][-1], exchange2, data[exchange2][-1])
                if profit > 0:
                    if trade_utils.is_relevant(op):
                        return [result1[0], utils.round_dwn(profit, 5), trade_fees, amounts, wfees]
                    else:
                        print_arb_log(['Arbitrage failed', 'Reason: Rejected. Opportunity expired'],
                                      mark)
                else:
                    print_arb_log(['Arbitrage failed', 'Reason: Rejected. Expected profit is {}'.format(profit),
                                   'Withdrawal fees: {}'.format(wfees)], mark)
            else:
                print_arb_log(
                    ['Arbitrage failed', 'Reason: {} {} {}'.format(exchange1, data[exchange1][0], 'Simulation failed')],
                    mark)
        else:
            print_arb_log(
                ['Arbitrage failed', 'Reason: {} {} {}'.format(exchange1, data[exchange1][0], 'Not enough balance'),
                 'Balance: {} {}'.format(utils.get_balance_fast(exchange1, data[exchange1][0]), data[exchange1][0])], mark)
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

# def execute(exchange, nodes, real_prices, amount, mark, response):
#     try:
#         print(exchange, nodes, real_prices, amount, mark)
#
#         trades = []
#
#
#         for i in range(len(nodes) - 1):
#             pair = utils.normalize_pair(nodes[i], nodes[i + 1])
#             symbol = pair[1] + pair[2]
#             side = pair[0]
#             amount = trade_utils.normalize_amount(exchange, amount, nodes[i], nodes[i + 1], real_prices[i])
#             if amount is None:
#                 print_arb_log(['Arbitrage failed', 'Reason: normalize amount failed ('
#                                + str(amount) + ' ' + nodes[i] + ' ' + nodes[i + 1] + ')'], mark)
#                 response.update({'trades': trades, 'success': False, 'amount': 0})
#                 return
#             print(exchange + ': ' + 'normalize_amount', amount, symbol)
#             balance = wp.get_balance(exchange, nodes[i + 1])
#             if balance is None:
#                 print_arb_log(['Arbitrage failed', 'Reason: getting balance failed (' + nodes[i + 1] + ')'], mark)
#                 response.update({'trades': trades, 'success': False, 'amount': 0})
#                 return
#             print(exchange, symbol, side, amount, real_prices[i])
#             orderId = wp.place_order(exchange, symbol, side, amount, real_prices[i])
#             orderTime = utils.get_time()
#             if orderId is not None and 'Error' not in orderId and 'code' not in orderId:
#                 print_arb_log(['Order {} is opened'.format(side.upper()), 'ID {}'.format(orderId)], mark)
#                 time.sleep(.5)
#                 status = order_manager.check_status(exchange, orderId, symbol)
#                 if status:
#                     trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
#                                    'side': side.upper(), 'status': 'closed', 'time': orderTime})
#                     print(exchange + ': ' + 'old balance', balance, nodes[i + 1])
#                     new_balance = wp.get_balance(exchange, nodes[i + 1])
#                     print(exchange + ': ' + 'new balance', new_balance, nodes[i + 1])
#                     balance = new_balance - balance
#                     amount = balance
#                     print(exchange + ': ' + 'next amount', amount, nodes[i + 1])
#                 else:
#                     corderId = wp.cancel_order(exchange, orderId, symbol)
#                     wp.cancel_order(exchange, orderId, symbol)
#                     trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
#                                    'side': side.upper(), 'status': 'expired', 'time': orderTime})
#                     print_arb_log(['Arbitrage failed',
#                                    'Reason: order is not closed within ' + str(expiration_time) + ' sec'], mark)
#                     response.update({'trades': trades, 'success': False, 'amount': 0})
#                     return
#             else:
#                 # if orderId is not None and 'Error' in orderId and 'code' not in orderId:
#                 trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': str(orderId), 'amount': amount,
#                                'side': side.upper(), 'status': 'failed', 'time': orderTime})
#                 print_arb_log(['Arbitrage failed', 'Reason: order is not opened'], mark)
#                 response.update({'trades': trades, 'success': False, 'amount': 0})
#                 return
#         response.update({'trades': trades, 'success': True, 'amount': amount})
#
#     except KeyboardInterrupt:
#         pass
#     except:
#         log(traceback.format_exc())
#         print(traceback.format_exc())

# print(wp.place_order('Okex', 'ETHUSDT', 'buy', 0.05947, 200.1859)) # Okex ETHUSDT buy 0.05947 200.1859
