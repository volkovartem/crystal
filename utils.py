import datetime as dt
import math
from decimal import Decimal

from pymemcache.client.base import Client

from database.services.settings import *

# settings
logLenth = 300
threadLogLenth = 50
TimeRangeStatsSec = 86400


def get_balance_fast(exchange, token=''):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(exchange + 'Balance')
        if r is not None:
            r = json.loads(r.decode())
            if token == '':
                return r
            else:
                dct = dict((x['token'], x['quantity']) for x in r)
                return 0 if token not in dct else dct[token]
    except:
        pass
        # print(traceback.format_exc())


def get_fee_fast(exchange, token=''):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(exchange + 'Fees')
        if r is not None:
            r = json.loads(r.decode())
            return r if token == '' else r[token]
    except:
        pass
        # print(traceback.format_exc())


def get_trade_limits_fast(exchange, symbol=''):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(exchange + 'Limits')
        if r is not None:
            r = json.loads(r.decode())
            if symbol == '':
                return r
            for t in r:
                if t['symbol'].replace('_', '') == symbol:
                    return t
    except:
        pass
        # print(traceback.format_exc())


# print(get_trade_limits_fast('Binance', 'ETHUSDT'))

def get_order_book_fast(exchange, symbol):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(symbol + '_' + exchange)
        if r is not None:
            orderbook = json.loads(r.decode())
            if exchange == 'Okex':
                orderbook['asks'].reverse()
            return orderbook
    except:
        pass


def split_symbol(symbol):
    if 'USDT' in symbol:
        sellSymb = symbol.rsplit("USDT", 1)[0]
        return {'s': sellSymb, 'b': 'USDT'}
    if 'BTC' in symbol:
        sellSymb = symbol.rsplit("BTC", 1)[0]
        return {'s': sellSymb, 'b': 'BTC'}
    if 'ETH' in symbol:
        sellSymb = symbol.rsplit("ETH", 1)[0]
        return {'s': sellSymb, 'b': 'ETH'}


def normalize_pair(s1, s2, mode=''):
    if s1 == 'USDT':
        return s2 + s1 if mode == 'symbol' else s1 + '_' + s2 if mode == '_symbol' else ['buy', s2, s1]

    if s1 == 'ETH':
        if s2 == 'USDT' or s2 == 'BTC':
            return s1 + s2 if mode == 'symbol' else s1 + '_' + s2 if mode == '_symbol' else ['sell', s1, s2]
        else:
            return s2 + s1 if mode == 'symbol' else s1 + '_' + s2 if mode == '_symbol' else ['buy', s2, s1]

    if s1 == 'BTC':
        if s2 == 'USDT':
            return s1 + s2 if mode == 'symbol' else s1 + '_' + s2 if mode == '_symbol' else ['sell', s1, s2]
        else:
            return s2 + s1 if mode == 'symbol' else s1 + '_' + s2 if mode == '_symbol' else ['buy', s2, s1]

    return s1 + s2 if mode == 'symbol' else s1 + '_' + s2 if mode == '_symbol' else ['sell', s1, s2]


def sqr(text):
    count = 0
    res = ''
    for x in text:
        res = res + x
        count = count + 1
        if count >= 40:
            count = 0
            res = res + '\r\n'
    return res


def get_time():
    ss = dt.datetime.utcnow().isoformat()
    ss = ss.split('.')[0].replace('T', ' ')
    ss = ss.split(' ')[1] + ' ' + ss.split(' ')[0]
    return ss


def is_updated(updid):
    client = Client(('127.0.0.1', 11211))
    r = client.get('OppLogInfo')
    if r is not None:
        updId = float(r.decode().split('|')[1])
        return True if float(updid) != updId else False
    return False


def norm(s):
    try:
        return '{0:.10f}'.format(s)
    except:
        return s


def round_dwn(value, param=1):
    return math.floor(value * math.pow(10, param)) / math.pow(10, param)


def get_preffered():
    coins = json.loads(get_settings('coins')[0]['value'])
    pref_coins = []
    for c in coins:
        if bool(c['preferred']):
            pref_coins.append(c['coin'])
    return pref_coins


def get_history():
    coins = json.loads(get_settings('history_transfers_coins')[0]['value'])
    return coins


def add_history(token):
    history = get_history()
    if token not in history:
        history.append(token)
        add_setting('history_transfers_coins', json.dumps(history))


def calc_amount_dom(orderbook1, orderbook2):
    asks = []
    bids = []
    if Decimal(orderbook1['bids'][0][0]) > Decimal(orderbook2['asks'][0][0]):
        asks = orderbook2['asks']
        bids = orderbook1['bids']
    if Decimal(orderbook1['asks'][0][0]) < Decimal(orderbook2['bids'][0][0]):
        asks = orderbook1['asks']
        bids = orderbook2['bids']

    a = 0
    b = 0
    ask_amount = 0
    bid_amount = 0
    ask_price = 0
    bid_price = 0
    amount = 0

    while True:
        try:
            ask = Decimal(asks[a][0])
            bid = Decimal(bids[b][0])
            if 1 / ask * bid > 1:
                bid_price = Decimal(bids[b][0])
                ask_price = Decimal(asks[a][0])
                amount = min(ask_amount + Decimal(asks[a][1]), bid_amount + Decimal(bids[b][1]))

                if ask_amount + Decimal(asks[a][1]) > bid_amount + Decimal(bids[b][1]):
                    bid_amount += Decimal(bids[b][1])
                    b += 1
                else:
                    ask_amount += Decimal(asks[a][1])
                    a += 1
            else:
                break
        except IndexError:
            break

    profit = '{:.10f}'.format(Decimal(round(((1 / ask_price * bid_price) - 1) * amount, 12)))
    return [float(amount), profit]


def get_amount_dom(symbol, exchange1, exchange2):
    orderbook1 = get_order_book_fast(exchange1, symbol)
    orderbook2 = get_order_book_fast(exchange2, symbol)
    # print(exchange1, 'ask', orderbook1['asks'][0])
    # print(exchange1, 'bid', orderbook1['bids'][0])
    # print(exchange2, 'ask', orderbook2['asks'][0])
    # print(exchange2, 'bid', orderbook2['bids'][0])
    return calc_amount_dom(orderbook1, orderbook2)

# add_history('NEO')
# add_history('LTC')


# print(get_order_book_fast('Binance', 'PPTBTC'))
# print(get_order_book_fast('Okex', 'PPTBTC'))
# print(get_amount_dom('PPTBTC', 'Binance', 'Okex'))