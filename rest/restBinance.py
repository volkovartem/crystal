import ccxt
import json
import requests
import traceback
from decimal import *
from ccxt.base.errors import *
from database.services.api import get_api
from database.services.logging import log
from pymemcache.client.base import Client
from utils import sqr, split_symbol, normalize_pair, round_dwn

name = 'Binance'
binance = ccxt.binance({
    'apiKey': get_api(name)[0],
    'secret': get_api(name)[1],
})


def connect_api():
    global binance
    binance = ccxt.binance({
        'apiKey': get_api(name)[0],
        'secret': get_api(name)[1],
    })


def get_the_price(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return binance.fetch_tickers()[symbol]['last']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_price(symbol):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(name)
        if r is not None:
            orderbook = json.loads(r.decode())
            for data in orderbook['data']:
                if data['symbol'] == symbol:
                    ask = '{0:.10f}'.format(round(Decimal(data['ask']).normalize(), 10))
                    bid = '{0:.10f}'.format(round(Decimal(data['bid']).normalize(), 10))
                    return {'bid': bid, 'ask': ask}
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_24hVolume():
    try:
        tickers = binance.fetch_tickers()
        # print(tickers)
        resp = {}
        for x in tickers:
            if tickers[x]['baseVolume'] is not None and tickers[x]['last'] is not None:
                vol = float(tickers[x]['baseVolume']) * float(tickers[x]['last'])
                resp.update({tickers[x]['symbol'].upper().replace('/', '_'): int(vol)})
        return resp
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_balance(symbol=''):
    try:
        balances = binance.fetch_balance()
        if symbol == '':
            result = []
            for balance in balances['info']['balances']:
                result.append({'token': balance['asset'], 'quantity': balance['free']})
            return result
        else:
            dct = dict((x['asset'], x['free']) for x in balances['info']['balances'])
            return 0 if symbol not in dct else dct[symbol]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_book(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return binance.fetch_order_book(symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_real_price(symbol, amount):
    try:
        orderbook = get_order_book(symbol)
        if orderbook is not None:
            result = {}
            bid_stack = 0
            for bid in orderbook['bids']:
                if float(bid[1]) + bid_stack > amount:
                    result.update({'realBid': float(bid[0])})
                    break
                else:
                    bid_stack += float(bid[1])
            ask_stack = 0
            for ask in orderbook['asks']:
                if float(ask[1]) + ask_stack > amount:
                    result.update({'realAsk': float(ask[0])})
                    break
                else:
                    ask_stack += float(ask[1])
            if result != {}:
                return result
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_fee(token=''):
    try:
        fees = binance.load_fees()['funding']['withdraw']
        return fees if token == '' else fees[token]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def place_order(symbol, side, amount, price):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        _symbol = '{}/{}'.format(base, quote)
        # print((_symbol, amount, price))
        if side == 'buy':
            order = binance.create_limit_buy_order(_symbol, amount, price)
            # utils.add_trade(name, symbol, order['id'], amount, side.upper(), status, orderTime)
            return order['id']
        if side == 'sell':
            order = binance.create_limit_sell_order(_symbol, amount, price)
            return order['id']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_status(id, symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = '{}/{}'.format(base, quote)
        status = binance.fetch_order(id, symbol)
        if status is not None:
            return status['status']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_health_wallets():
    try:
        r = requests.get('https://www.binance.com/assetWithdraw/getAllAsset.html')
        if r is not None:
            responce = []
            wallets = r.json()
            for wallet in wallets:
                if wallet['enableCharge'] and wallet['enableWithdraw']:
                    responce.append(wallet['assetCode'])
            return responce
    except requests.exceptions.ConnectionError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def transfer(amount, fromToken, toToken):
    try:
        pair = normalize_pair(fromToken, toToken)
        symbol = pair[1] + pair[2]
        price = get_real_price(symbol, amount)
        if price is not None:
            if pair[0] == 'buy':
                amount = round_dwn(amount / float(price['realAsk']), 5)
                return binance.create_market_buy_order(pair[1] + '/' + pair[2], amount)
            else:
                return binance.create_market_sell_order(pair[1] + '/' + pair[2], amount)
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def withdraw(token, amount, address):
    try:
        wid = binance.withdraw(token, amount, address)['id']
        return wid
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_wallet_address(token):
    try:
        return binance.fetch_deposit_address(token)['address']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_markets():
    try:
        ms = binance.load_markets()
        if ms is not None:
            return [k.replace('/', '_') for k in ms.keys()]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def cancel_order(id, symbol):
    try:
        return binance.cancel_order(id, symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_trade_limits(symbol=''):
    try:
        slippage = 2  # %
        ms = binance.load_markets()
        ts = binance.fetch_tickers()
        if symbol != '':
            base = split_symbol(symbol)['s']
            quote = split_symbol(symbol)['b']
            symbol = base + '/' + quote
        result = []
        if ms is not None and ts is not None:
            for m in ms:
                min = float(ms[m]['limits']['amount']['min'])
                # print(m, ms[m]['limits'])
                market = float(ms[m]['limits']['cost']['min'])
                price = ts[m]['ask']
                # print(m, market, price, ms[m]['limits'])
                if price > 0:
                    market = round(market / price * ((slippage + 100) / 100), 8)
                if symbol != '' and symbol == m:
                    # print(symbol, m, price)
                    return {'symbol': symbol.replace('/', '_'), 'min': min, 'market': market}
                result.append({'symbol': m.replace('/', '_'), 'min': min, 'market': market})
        return result
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

# binance.load_markets()
# print(binance.markets['ETH/BTC'])
#
# print(get_trade_limits())
# print(get_trade_limits('BTCUSDT'))

# lim = get_trade_limits('NEOETH')['market']
# print(lim)
# print(transfer(lim, 'ETH', 'USDT'))


# print(get_markets())
# print(type(get_balance('ETH')), get_balance('ETH'))
# print(get_balance('ETH'))
# print(get_order_book('ETH/USDT'))
# print(get_fee('ETH'))
# print(get_bad_wallets())

# import time
# transfer(0.49, 'NEO', 'ETH')
# # transfer(0.03947, 'ETH', 'NEO')
# time.sleep(1)
# print(get_balance('ETH'))
# print(get_balance('NEO'))

# print(get_health_wallets())
# print(get_fee('ETH'))
# print(get_24hVolume())
# print(get_the_price('ETHBTC'))

# print(get_wallet_address('USDT'))
# print(binance.load_markets())