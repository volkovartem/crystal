import ccxt
import json
import traceback
import requests
import okex_api_spot
from wsTrading import open_market_order
from decimal import *
from ccxt.base.errors import *
from okex_api_spot import ExchangeType
from database.services.api import get_api
from database.services.logging import log
from pymemcache.client.base import Client
from utils import sqr, split_symbol, normalize_pair

name = 'Okex'

# ApiKey = '5aab4742-bb38-4d55-82a1-0dcd6f9cff00'
# ApiSecret = '4DE09137BE0200156191BCBC34A0464B'
# Passphrase = "'9U=NDUz2Z\\t:$d!"
# FundPassword = 'ee>W>>x[`(72W42m'

api = get_api(name)
# print(api)
okex = okex_api_spot.Okex(api[0], api[1], api[5].replace('»', '>>'), api[4].replace('»', '>>'))
# print(okex)

def connect_api():
    api = get_api(name)
    global okex
    okex = okex_api_spot.Okex(api[0], api[1], api[5].replace('»', '>>'), api[4].replace('»', '>>'))


def get_the_price(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return okex.get_ticker(symbol)['last']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_price(symbol):
    try:
        if '_' not in symbol:
            base = split_symbol(symbol)['s']
            quote = split_symbol(symbol)['b']
            symbol = '{}_{}'.format(base, quote)
        client = Client(('127.0.0.1', 11211))
        r = client.get(symbol)
        if r is not None:
            data = json.loads(r.decode())
            ask = '{0:.10f}'.format(round(Decimal(float(data['asks'][0][0])).normalize(), 10))
            bid = '{0:.10f}'.format(round(Decimal(float(data['bids'][0][0])).normalize(), 10))
            return {'bid': bid, 'ask': ask}
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())
# print(get_price('ETH_USDT'))

def get_24hVolume():
    try:
        tickers = okex.get_ticker()
        resp = {}
        for x in tickers:
            if x['base_volume_24h'] is not None and x['last'] is not None:
                vol = float(x['base_volume_24h']) * float(x['last'])
                resp.update({x['instrument_id'].upper().replace('-', '_'): int(vol)})
        return resp
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_balance(symbol=''):
    try:
        # t = 5/0
        balances = okex.spot_account()
        if symbol == '':
            if 'code' not in balances:
                result = []
                for balance in balances:
                    result.append({'token': balance['currency'], 'quantity': balance['available']})
                return result
        else:
            bal = okex.spot_account(symbol)
            if 'code' not in bal:
                return float(bal['available'])
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_book(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return okex.get_orderbook(symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_fee(token=''):
    try:
        if token != '':
            fee = okex.withdrawal_fee(token)[0]
            if 'min_fee' in fee:
                return fee['min_fee']
        else:
            dc = {}
            fees = okex.withdrawal_fee()
            for x in fees:
                if 'min_fee' in x and 'currency' in x:
                    dc.update({x['currency']: x['min_fee']})
            if dc != {}:
                return dc
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


# print(get_fee('ETH'))
# print(get_fee())


def place_order(symbol, side, amount, price):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = '{}/{}'.format(base, quote)
        if side == 'buy':
            order = okex.place_limit_buy_order(symbol, price, amount)
            if 'order_id' in order:
                return order['order_id']
            else:
                return order
        if side == 'sell':
            order = okex.place_limit_sell_order(symbol, price, amount)
            if 'order_id' in order:
                return order['order_id']
            else:
                return order
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_status(id, symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = '{}/{}'.format(base, quote)
        return okex.get_order_details(id, symbol)['status']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_health_wallets():
    try:
        responce = []
        wallets = okex.currencies()
        for wallet in wallets:
            if 'can_withdraw' not in wallet and 'can_deposit' not in wallet:
                log('wallets' + str(wallets))
                return
            if wallet['can_withdraw'] and wallet['can_deposit']:
                responce.append(wallet['currency'])
        return responce
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def transfer(amount, fromToken, toToken):
    try:
        pair = normalize_pair(fromToken, toToken)
        symbol = pair[1] + pair[2]
        return open_market_order(symbol, pair[0], amount)
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def withdraw(token, amount, address):
    try:
        fee = float(get_fee(token))
        wid = okex.withdrawal(token, amount, ExchangeType.EXTERN, address, fee)
        return wid
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_wallet_address(token):
    try:
        return okex.deposit_address(token)[0]['address']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_markets():
    try:
        ms = okex.get_markets_details()
        if ms is not None:
            return [k['instrument_id'].replace('-', '_') for k in ms]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def cancel_order(id, symbol):
    try:
        return okex.cancel_order(id, symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_trade_limits(symbol=''):
    try:
        r = requests.get('https://www.okex.com/v2/spot/markets/products')
        if r is not None:
            if symbol != '':
                base = split_symbol(symbol)['s']
                quote = split_symbol(symbol)['b']
                symbol = base + '_' + quote
            r = json.loads(r.text)
            result = []
            for pair in r['data']:
                _symbol = pair['symbol'].upper()
                if _symbol.split('_')[1] == 'BTC' or _symbol.split('_')[1] == 'ETH' or _symbol.split('_')[1] == 'USDT':
                    result.append({'symbol': _symbol,
                                   'min': float(pair['minTradeSize']),
                                   'market': float(pair['minTradeSize'])})
                    if symbol == _symbol:
                        return {'symbol': _symbol,
                                'min': float(pair['minTradeSize']),
                                'market': float(pair['minTradeSize'])}
            return result
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

# print(get_trade_limits('VEEETH'))

# print(get_price('SNGLSBTC'))

# print(okex.cancel_order(1792096939282432, 'INSUR-BTC'))

# print(get_order_status(1899966764291072, 'ETHUSDT'))

# print(okex.get_all_open_orders())
# print(get_markets())

# print(get_the_price('ETHBTC'))
# print(get_price('ETHBTC'))
# print(get_24hVolume())
# print(get_balance())
# print(get_balance('LTC'))
# print(get_order_book('ETHBTC'))
# print(get_fee('ETH'))
# print(place_order('LSKUSDT', 'buy', 1, 4))
# print(get_order_status(1651847144082432, 'LSKUSDT'))
# print(get_health_wallets())
# print(transfer(0.01, 'LSK', 'USDT'))
# print(get_wallet_address('ETH'))
# print(withdraw('USDT', 4, '1GkyZKhmCoCRscxYkTvpcSDLthhnjQF5KB'))


# print(transfer(0.01524, 'ETH', 'LTC'))