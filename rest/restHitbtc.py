import ccxt
import json
import traceback
from decimal import *
from ccxt.base.errors import *
from database.services.api import get_api
from database.services.logging import log
from pymemcache.client.base import Client
from utils import sqr, split_symbol, normalize_pair, round_dwn

name = 'Hitbtc'
hitbtc = ccxt.hitbtc2({
    'apiKey': get_api(name)[0],
    'secret': get_api(name)[1],
})


def connect_api():
    global hitbtc
    hitbtc = ccxt.hitbtc2({
        'apiKey': get_api(name)[0],
        'secret': get_api(name)[1],
    })


def get_the_price(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        try:
            r = hitbtc.fetch_tickers()
            # print(r)
            if symbol in r:
                # print(r[symbol])
                return r[symbol]['last']
        except ExchangeError:
            print(traceback.format_exc())
        except NetworkError:
            print(traceback.format_exc())
        except:
            print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_price(symbol):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(name)
        if r is not None:
            orderbook = json.loads(r.decode())
            for data in orderbook['data']:
                if data['symbol'] == symbol:
                    ask = '{0:.10f}'.format(round(Decimal(data['ask']).normalize(), 10))
                    bid = '{0:.10f}'.format(round(Decimal(data['bid']).normalize(), 10))
                    return {'bid': bid, 'ask': ask}
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_24hVolume():
    try:
        tickers = hitbtc.fetch_tickers()
        # print(tickers)
        resp = {}
        for x in tickers:
            if tickers[x]['baseVolume'] is not None and tickers[x]['last'] is not None:
                vol = float(tickers[x]['baseVolume']) * float(tickers[x]['last'])
                resp.update({tickers[x]['symbol'].upper().replace('/', '_'): int(vol)})
        return resp
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_balance(symbol=''):
    try:
        # t = 5/0
        balances = hitbtc.fetch_balance()
        if symbol == '':
            result = []
            for balance in balances['info']:
                result.append({'token': balance['currency'], 'quantity': balance['available']})
            return result
        else:
            dct = dict((x['currency'], x['available']) for x in balances['info'])
            return 0 if symbol not in dct else dct[symbol]

    except ExchangeError:
        print(traceback.format_exc())
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_book(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return hitbtc.fetch_order_book(symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_real_price(symbol, amount):
    try:
        orderbook = get_order_book(symbol)
        if orderbook is not None:
            result = {}
            bid_stack = 0
            for bid in orderbook['bids']:
                if float(bid[1]) + bid_stack > amount:
                    result.update({'realBid': float(bid[0])})
                    break
                else:
                    bid_stack += float(bid[1])
            ask_stack = 0
            for ask in orderbook['asks']:
                if float(ask[1]) + ask_stack > amount:
                    result.update({'realAsk': float(ask[0])})
                    break
                else:
                    ask_stack += float(ask[1])
            if result != {}:
                return result
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_fee(token=''):
    try:
        fees = hitbtc.load_fees()['funding']['withdraw']
        return fees if token == '' else fees[token]
    except ExchangeError:
        print(traceback.format_exc())
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def place_order(symbol, side, amount, price):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        _symbol = '{}/{}'.format(base, quote)
        if side == 'buy':
            order = hitbtc.create_limit_buy_order(_symbol, amount, price)
            return order['id']
        if side == 'sell':
            order = hitbtc.create_limit_sell_order(_symbol, amount, price)
            return order['id']
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_status(id, symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = '{}/{}'.format(base, quote)
        return hitbtc.fetch_order_status(id, symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_health_wallets():
    try:
        responce = []
        currencies = hitbtc.fetch_currencies()
        for currency in currencies:
            if currencies[currency]['payin'] and currencies[currency]['payout'] and currencies[currency]['transfer']:
                responce.append(currency)
        return responce
    except ExchangeError:
        print(traceback.format_exc())
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def transfer(amount, fromToken, toToken):
    try:
        pair = normalize_pair(fromToken, toToken)
        symbol = pair[1] + pair[2]
        price = get_real_price(symbol, amount)
        if price is not None:
            if pair[0] == 'buy':
                amount = round_dwn(amount / float(price['realAsk']), 5)
                return hitbtc.create_market_buy_order(pair[1] + '/' + pair[2], amount)
            else:
                return hitbtc.create_market_sell_order(pair[1] + '/' + pair[2], amount)
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def withdraw(token, amount, address):
    try:
        wid = hitbtc.withdraw(token, amount, address)['id']
        return wid
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_wallet_address(token):
    try:
        return hitbtc.fetch_deposit_address(token)['address']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_cIds():
    try:
        lst = {}
        markets = hitbtc.load_markets()
        for m in markets:
            lst.update({m.replace('/', ''): markets[m]['id']})
        return lst
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_markets():
    try:
        ms = hitbtc.load_markets()
        if ms is not None:
            return [k.replace('/', '_') for k in ms.keys()]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_trade_limits(symbol=''):
    try:
        ms = hitbtc.load_markets()
        if ms is not None:
            if symbol != '':
                base = split_symbol(symbol)['s']
                quote = split_symbol(symbol)['b']
                symbol = base + '/' + quote
                return {'symbol': symbol.replace('/', '_'),
                        'min': float(ms[symbol]['limits']['amount']['min']),
                        'market': float(ms[symbol]['limits']['amount']['min'])}
            return [{'symbol': market.replace('/', '_'),
                     'min': float(ms[market]['limits']['amount']['min']),
                     'market': float(ms[market]['limits']['amount']['min'])} for market in ms]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def cancel_order(id, symbol):
    try:
        return hitbtc.cancel_order(id, symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


# print(get_trade_limits('ETHBTC'))
# print(hitbtc.load_markets()['ETH/BTC'])
# print(get_balance())
# print(get_balance('ETH'))
# print(get_order_book('ETHUSDT'))
# print(get_fee('ETH'))

# print(hitbtc.load_markets()['BTC/USDT'])
# import time
# transfer(0.49, 'NEO', 'ETH')
# # transfer(0.03947, 'ETH', 'NEO')
# time.sleep(1)
# print(get_balance('ETH'))
# print(get_balance('NEO'))
# print(get_24hVolume())

# print(get_wallet_address('ETH'))
# print(get_health_wallets())
# print(get_the_price('PBTTBTC'))
# print(get_cIds())
# print(get_wallet_address('NEO'))
