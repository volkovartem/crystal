import ccxt
import json
import traceback
from decimal import *
from utils import round_dwn
from ccxt.base.errors import *
from database.services.api import get_api
from database.services.logging import log
from pymemcache.client.base import Client
from utils import sqr, split_symbol, normalize_pair, round_dwn

name = 'Bittrex'
bittrex = ccxt.bittrex({
    'apiKey': get_api(name)[0],
    'secret': get_api(name)[1],
})


def connect_api():
    global bittrex
    bittrex = ccxt.bittrex({
        'apiKey': get_api(name)[0],
        'secret': get_api(name)[1],
    })


def get_the_price(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return bittrex.fetch_tickers()[symbol]['last']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_price(symbol):
    try:
        client = Client(('127.0.0.1', 11211))
        r = client.get(name)
        if r is not None:
            orderbook = json.loads(r.decode())
            for data in orderbook['data']:
                if data['symbol'] == symbol:
                    ask = '{0:.10f}'.format(round(Decimal(data['ask']).normalize(), 10))
                    bid = '{0:.10f}'.format(round(Decimal(data['bid']).normalize(), 10))
                    return {'bid': bid, 'ask': ask}
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_24hVolume():
    try:
        tickers = bittrex.fetch_tickers()
        resp = {}
        for x in tickers:
            if tickers[x]['baseVolume'] is not None and tickers[x]['last'] is not None:
                vol = float(tickers[x]['baseVolume']) * float(tickers[x]['last'])
                if tickers[x]['symbol'] is not None:
                    resp.update({tickers[x]['symbol'].upper().replace('/', '_'): int(vol)})
        return resp
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_balance(symbol=''):
    try:
        balances = bittrex.fetch_balance()
        if symbol == '':
            result = []
            for balance in balances['info']:
                result.append({'token': balance['Currency'], 'quantity': balance['Available']})
            return result
        else:
            dct = dict((x['Currency'], x['Available']) for x in balances['info'])
            return 0 if symbol not in dct else dct[symbol]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_book(symbol):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        symbol = base + '/' + quote
        return bittrex.fetch_order_book(symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_real_price(symbol, amount):
    try:
        orderbook = get_order_book(symbol)
        if orderbook is not None:
            result = {}
            bid_stack = 0
            for bid in orderbook['bids']:
                if float(bid[1]) + bid_stack > amount:
                    result.update({'realBid': float(bid[0])})
                    break
                else:
                    bid_stack += float(bid[1])
            ask_stack = 0
            for ask in orderbook['asks']:
                if float(ask[1]) + ask_stack > amount:
                    result.update({'realAsk': float(ask[0])})
                    break
                else:
                    ask_stack += float(ask[1])
            if result != {}:
                return result
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_fee(token=''):
    try:
        fees = bittrex.load_fees()['funding']['withdraw']
        return fees if token == '' else fees[token]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

# print(get_fee('ETH'))
# print(get_fee())


def place_order(symbol, side, amount, price):
    try:
        base = split_symbol(symbol)['s']
        quote = split_symbol(symbol)['b']
        _symbol = '{}/{}'.format(base, quote)
        if side == 'buy':
            order = bittrex.create_limit_buy_order(_symbol, amount, price)
            return order['id']
        if side == 'sell':
            order = bittrex.create_limit_sell_order(_symbol, amount, price)
            return order['id']
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_order_status(id, symbol):
    try:
        return bittrex.fetch_order_status(id)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_health_wallets():
    try:
        wallets = bittrex.fetch_currencies()
        responce = []
        for wallet in wallets:
            if wallets[wallet]['info']['IsActive'] is True:
                responce.append(wallets[wallet]['id'])
        return responce
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def transfer(amount, fromToken, toToken):
    try:
        pair = normalize_pair(fromToken, toToken)
        symbol = pair[1] + pair[2]
        price = get_real_price(symbol, amount)
        if price is not None:
            if pair[0] == 'buy':
                amount = round_dwn(amount / float(price['realAsk']), 5)
                return bittrex.create_limit_buy_order(pair[1] + '/' + pair[2], amount, price['realAsk'])
            else:
                return bittrex.create_limit_sell_order(pair[1] + '/' + pair[2], amount, price['realBid'])
    except KeyboardInterrupt:
        pass
    except ccxt.InvalidOrder as e:
        print(sqr('Error ' + str(e.args)))
        return sqr('Error ' + str(e.args))
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def withdraw(token, amount, address):
    try:
        wid = bittrex.withdraw(token, amount, address)['id']
        return wid
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_wallet_address(token):
    try:
        return bittrex.fetch_deposit_address(token)['address']
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_markets():
    try:
        ms = bittrex.load_markets()
        if ms is not None:
            return [k.replace('/', '_') for k in ms.keys()]
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def get_trade_limits(symbol=''):
    try:
        if symbol != '':
            base = split_symbol(symbol)['s']
            quote = split_symbol(symbol)['b']
            symbol = base + '/' + quote
        result = []
        ts = bittrex.fetch_tickers()
        for t in ts:
            if t.split('/')[1] != 'USD':
                min = 0.0008
                if t.split('/')[0] != 'BTC':
                    price = ts[t.split('/')[0] + '/BTC']['ask']
                    if price > 0:
                        min = round(0.0008 / price, 8)
                if symbol != '' and symbol == t:
                    return {'symbol': symbol.replace('/', '_'), 'min': min, 'market': min}
                result.append({'symbol': t.replace('/', '_'), 'min': min, 'market': min})
        return result
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


# print(get_trade_limits('VEEBTC'))
# print(get_trade_limits())


def cancel_order(id, symbol):
    try:
        return bittrex.cancel_order(id, symbol)
    except NetworkError:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


# print(get_order_book('ETH/USDT'))
# print(get_balance())
# print(get_balance('ETH'))
# print((get_fee('ETH')))
# print(get_balance('USDT'))
# print(get_balance('NEO'))
# print(get_bad_wallets())


# import time
# # transfer(0.49, 'NEO', 'ETH')
# transfer(0.0278, 'ETH', 'NEO')
# time.sleep(1)
# print(get_balance('ETH'))
# print(get_balance('NEO'))

# print(get_health_wallets())
# print(get_fee('ETH'))
# print(get_24hVolume())
# print(get_the_price('ETHBTC'))
