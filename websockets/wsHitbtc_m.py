# import sys
# sys.path.append('/root/crystal')
import time
import json
import threading
import websocket
import traceback
from rest import restHitbtc as hitbtc
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings


#settings
filter_update_timeout = 10

cIds = {}
name = 'Hitbtc'
client = Client(('127.0.0.1', 11211))
client2 = Client(('127.0.0.1', 11211))


def filter_update(ws, old_filter):
    try:
        while True:
            time.sleep(filter_update_timeout)
            fltr = json.loads(get_settings('filter')[0]['value'])[name]
            if fltr != old_filter:
                ws.close()
                global orderbook
                orderbook = {}
                break
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_message(ws, message):
    try:
        # print(message)
        time.sleep(0.01)
        global cIds
        data = json.loads(message)
        if 'params' in data:
            data = data['params']
            symbol = list(cIds.keys())[list(cIds.values()).index(data['symbol'])]
            bid = data['bid']
            ask = data['ask']
            if bid is not None and ask is not None:
                xd = {
                    'symbol': symbol,
                    'bid': float(bid),
                    'ask': float(ask)
                }
                r = client.get(name)
                if r is None or r.decode() == '':
                    data = {'timestamp': int(time.time()), 'data': [xd]}
                    client2.set(name, json.dumps(data))
                else:
                    data = json.loads(r.decode())['data']
                    if symbol in [k['symbol'] for k in data]:
                        for d in data:
                            if d['symbol'] == symbol:
                                d.update({
                                    'bid': float(bid),
                                    'ask': float(ask)
                                })
                                break
                    else:
                        data.append(xd)

                    data = {'timestamp': int(time.time()), 'data': data}
                    print(len(data['data']), data)
                    client2.set(name, json.dumps(data))
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

def on_error(ws, error):
    print('error', error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    try:
        filter = json.loads(get_settings('filter')[0]['value'])[name]
        global cIds
        cIds = hitbtc.get_cIds()

        print(filter)
        for pair in filter:
            if '.' not in pair and '_' in pair:
                pair = pair.replace('_', '')
                if pair in cIds:
                    ws.send('{"method": "subscribeTicker", "params": {"symbol": "' + cIds[pair] + '"}, "id": 123}')
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)
    client2.set(name, '')

    filter = json.loads(get_settings('filter')[0]['value'])[name]

    try:
        ws = websocket.WebSocketApp(
            "wss://api.hitbtc.com/api/2/ws",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close)

        t3 = threading.Thread(target=filter_update, args=(ws, filter))
        t3.start()

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
