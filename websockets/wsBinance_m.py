# import sys
# sys.path.append('/root/crystal')
import time
import json
import threading
import websocket
import traceback
from decimal import Decimal
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings

# settings
filter_update_timeout = 10

name = 'Binance'
client = Client(('127.0.0.1', 11211))
client2 = Client(('127.0.0.1', 11211))


def filter_update(ws, old_filter):
    try:
        while True:
            time.sleep(filter_update_timeout)
            fltr = json.loads(get_settings('filter')[0]['value'])[name]
            if fltr != old_filter:
                ws.close()
                global orderbook
                orderbook = {}
                break
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_message(ws, message):
    try:
        # print(message)
        # time.sleep(1)
        data = json.loads(message)['data']
        symbol = json.loads(message)['stream'].split('@')[0].upper()
        bids = data['bids']
        asks = data['asks']
        # asks.reverse()

        if asks != [] and bids != []:
            xd = {
                'symbol': symbol,
                'bid': float(Decimal(bids[0][0]).normalize()),
                'ask': float(Decimal(asks[0][0]).normalize())
            }
            r = client.get(name)
            if r is None or r.decode() == '':
                data = {'timestamp': int(time.time()), 'data': [xd]}
                client2.set(name, json.dumps(data))
            else:
                data = json.loads(r.decode())['data']
                if symbol in [k['symbol'] for k in data]:
                    for d in data:
                        if d['symbol'] == symbol:
                            d.update({
                                'bid': float(Decimal(bids[0][0]).normalize()),
                                'ask': float(Decimal(asks[0][0]).normalize())
                            })
                            break
                else:
                    data.append(xd)

                data = {'timestamp': int(time.time()), 'data': data}
                # print(len(data['data']), data)
                client2.set(name, json.dumps(data))
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print("### opened ###")


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)
    client2.set(name, '')

    filter = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(filter))
    try:
        subs = ''
        for pair in filter:
            subs += pair.replace('_', '').lower() + '@depth5/'
        # subs += 'LTC_BTC'.replace('_', '').lower() + '@depth5/'
        if subs != '':
            ws = websocket.WebSocketApp(
                "wss://stream.binance.com:9443/stream?streams={}".format(subs.strip('/')),
                # ws = websocket.WebSocketApp("wss://stream.binance.com:9443/ws/ltcbtc@aggTrade/ethbtc@aggTrade",
                on_open=on_open,
                on_message=on_message,
                on_error=on_error,
                on_close=on_close)

            t3 = threading.Thread(target=filter_update, args=(ws, filter))
            t3.start()

            ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
