# import sys
# sys.path.append('/root/crystal')
import gzip
import hashlib
import json
import threading
import time
import traceback
import zlib

import websocket
from pymemcache.client.base import Client

from database.services.logging import log
from utils import split_symbol

# settings
filter_check_timeout = 10

order_container = {}
login_status = [False]
name = 'Okex'

api_key = '92952754-87e2-41c7-b5d6-a27605b7e2fa'
secret_key = '1F87CF665C57E96FB2E05AA08D623D4F'


def inflate(data):
    decompress = zlib.decompressobj(
        -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


def send_message(ws, message_dict):
    data = json.dumps(message_dict).encode()
    ws.send(data)


def on_message(ws, message):
    try:
        message = inflate(message)
        message = json.loads(message)
        if 'event' in message:
            if message['event'] == 'pong':
                return
        message = message[0]
        channel = message['channel']
        print(time.time(), message)
        if channel == 'login':
            if bool(message['data']['result']):
                login_status[0] = True

        if 'ok_sub_spot_' in channel and '_order' in channel:
            symbol = message['data']['symbol']
            order_id = message['data']['orderId']
            status_code = int(message['data']['status'])
            tradeAmount = message['data']['completedTradeAmount']
            averagePrice = message['data']['averagePrice']

            status = 'wait'
            if status_code == -1:
                status = 'cancelled'
            if status_code == 0:
                status = 'unfilled'
            if status_code == 2:
                status = 'filled'
            if status_code == 4:
                status = 'canceling'
            if status_code == 5:
                status = 'canceling'

            trade_type = message['data']['tradeType']
            side = 'buy' if 'buy' in trade_type else 'sell'
            if symbol in order_container:
                order_container[symbol].update({'order_id': order_id,
                                                'side': side,
                                                'status': status,
                                                'trade_amount': tradeAmount,
                                                'price': averagePrice})
        if 'ok_sub_spot_' in channel and '_balance' in channel:
            free = message['data']['info']['free']
            token = list(free.keys())[0]
            balance = free[token]
            for x in order_container:
                if token in x:
                    order_container[x]['balances'].update({token.upper(): balance})
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(error):
    error = gzip.decompress(error).decode()
    print(error)


def on_close(ws):
    print("### closed ###")


def buildMySign(params, secret_key):
    sign = ''
    for key in sorted(params.keys()):
        sign += key + '=' + str(params[key]) + '&'
    return hashlib.md5((sign + 'secret_key=' + secret_key).encode("utf-8")).hexdigest().upper()


def place_market_order(symbol, side, amount):
    bq = split_symbol(symbol)
    symbol = bq['s'] + '_' + bq['b']
    params = {'api_key': api_key}
    params.update({'price': amount} if side == 'buy' else {'amount': amount})
    params.update({'symbol': symbol.lower()})
    params.update({'type': 'buy_market'} if side == 'buy' else {'type': 'sell_market'})
    # print(params)
    sign = buildMySign(params, secret_key)
    params.update({'sign': sign})
    order_container.update({symbol.lower(): {'symbol': symbol,
                                             'balances': {},
                                             'status': 'wait'}})
    send_message(ws, {'event': 'addChannel', 'channel': 'ok_spot_order', "parameters": params})
    order = order_container[symbol.lower()]
    timeout = time.time()
    while time.time() < timeout + 15:
        if 'status' in order and order['status'] == 'filled':
            if len(order['balances']) == 2:
                break
    if order['status'] == 'wait':
        order['status'] = 'failed'
    return order


def login():
    params = {'api_key': api_key}
    sign = buildMySign(params, secret_key)

    send_message(ws, {"event": "login",
                      "parameters": {"api_key": api_key,
                                     "sign": sign}})

    timeout = time.time()
    while time.time() < timeout + 5:
        if login_status[0]:
            return True
    return False


def run(*args):
    auth = login()
    if auth:
        client = Client(('127.0.0.1', 11211))
        client.set('OkexTrading', '{}')
        while True:
            r = client.get('OkexTrading')
            if r is not None:
                r = json.loads(r.decode())
                if r and not r['result']:
                    if r['action'] == 'open_market_order':
                        order = place_market_order(r['data']['symbol'], r['data']['side'], float(r['data']['amount']))
                        r['result'].update(order)
                        client.set('OkexTrading', json.dumps(r))
            time.sleep(.1)


def ping(*args):
    while True:
        send_message(args[0], {'event': 'ping'})
        time.sleep(29)


def on_open(ws):
    t = threading.Thread(target=ping, args=([ws]))
    t.start()

    t2 = threading.Thread(target=run, args=())
    t2.start()

    # order = place_market_order('LTCETH', 'sell', 0.05)
    # order = place_market_order('LTCETH', 'buy', 0.014)
    # print(order)


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    try:
        host = "wss://real.okex.com:10440/ws/v1"
        ws = websocket.WebSocketApp(host,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close)
        ws.on_open = on_open
        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
