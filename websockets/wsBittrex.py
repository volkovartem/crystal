# import sys
# sys.path.append('/root/crystal')
import time
import json
import requests
import threading
import websocket
import traceback
from base64 import b64decode
from urllib.parse import quote_plus
from zlib import decompress, MAX_WBITS
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings


# settings
filter_check_timeout = 4

orderbook = {}
name = 'Bittrex'

filter = json.loads(get_settings('filter')[0]['value'])[name]
print(len(filter))

def filter_update(*args):
    try:
        while True:
            global filter
            global orderbook
            time.sleep(filter_check_timeout)
            new_filter = json.loads(get_settings('filter')[0]['value'])[name]
            if new_filter != filter:
                orderbook = {}
                filter = new_filter
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

def push_to_memcache(*args):
    while True:
        try:
            client = Client(('127.0.0.1', 11211))
            while True:
                data = []
                # print(orderbook)
                for c in orderbook:
                    data.append({
                        'symbol': c,
                        'bid': orderbook[c]['bid'],
                        'ask': orderbook[c]['ask']
                    })

                data = {'timestamp': int(time.time()), 'data': data}
                client.set(name, json.dumps(data))
                print(orderbook.__len__(), orderbook)
                # print(data)
                time.sleep(1)
        except RuntimeError:
            print(traceback.format_exc())
        except KeyboardInterrupt:
            pass
        except:
            log(traceback.format_exc())
            print(traceback.format_exc())
            time.sleep(1)


def process_message(message):
    try:
        deflated_msg = decompress(b64decode(message, validate=True), -MAX_WBITS)
    except SyntaxError:
        deflated_msg = decompress(b64decode(message, validate=True))
    return json.loads(deflated_msg.decode())

def on_message(ws, message):
    try:
        message = json.loads(message)
        # print(message)
        if "M" in message and len(message['M']) > 0:
            message = message["M"][0]
            if 'A' in message and len(message['A']) > 0:
                message = message['A'][0]
                msg = process_message(message)
                if 'D' in msg:
                    # print(len(msg['D']), msg['D'])
                    for x in msg['D']:
                        symbol = x['M'].split('-')[1] + '_' + x['M'].split('-')[0]
                        if symbol in filter:
                            xd = {
                                'bid': float(x['B']),
                                'ask': float(x['A'])
                            }
                            orderbook[symbol.replace('_', '')] = xd
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("### opened ###")
    ws.send('{' + '"H":"c2","M":"SubscribeToSummaryDeltas","A":[],"I":0}')

    t3 = threading.Thread(target=filter_update, args=())
    t3.start()

    t2 = threading.Thread(target=push_to_memcache, args=())
    t2.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    try:
        r = requests.get(
            'https://socket.bittrex.com/signalr/negotiate?clientProtocol=1.5&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D&_={}'.format(
                time.time()))
        if r is not None:
            token = json.loads(r.text)['ConnectionToken']
            token = quote_plus(token)
            # print(token)

            r = requests.get(
                'https://socket.bittrex.com/signalr/start?transport=webSockets&clientProtocol=1.5&connectionToken={}&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D&_={}'.format(
                    token, time.time()))
            if r is not None:
                print(r.text)

                ws = websocket.WebSocketApp(
                    "wss://socket.bittrex.com/signalr/connect?transport=webSockets&clientProtocol=1.5&connectionToken={}&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D&tid=3".format(
                        token),
                    on_open=on_open,
                    on_message=on_message,
                    on_error=on_error,
                    on_close=on_close)

                ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())