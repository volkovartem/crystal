import websocket


def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("### opened ###")

if __name__ == "__main__":
    print('start websocket')

    ws = websocket.WebSocketApp(
        "wss://stream.binance.com:9443/stream?streams=",
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close)

    ws.run_forever()