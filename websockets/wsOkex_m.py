# import sys
# sys.path.append('/root/crystal')
import gzip
import json
import time
import threading
import zlib
import websocket
import traceback
from decimal import Decimal
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings


#settings
filter_check_timeout = 10

name = 'Okex'
client = Client(('127.0.0.1', 11211))
client2 = Client(('127.0.0.1', 11211))


def inflate(data):
    decompress = zlib.decompressobj(
            -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


def filter_update(ws, old_filter):
    try:
        while True:
            time.sleep(filter_check_timeout)
            fltr = json.loads(get_settings('filter')[0]['value'])[name]
            if fltr != old_filter:
                ws.close()
                global orderbook
                orderbook = {}
                break
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def send_message(ws, message_dict):
    data = json.dumps(message_dict).encode()
    ws.send(data)


def on_message(ws, message):
    try:
        # print(message)
        message = inflate(message)
        channel = json.loads(message)[0]['channel']
        if channel != 'addChannel':
            symbol = (channel.split('_')[3] + channel.split('_')[4]).upper()
            data = json.loads(message)[0]['data']

            if 'asks' in data:
                if data['asks'] != [] and data['bids'] != []:
                    bids = data['bids']
                    asks = data['asks']
                    asks.reverse()

                    if (bids[0][1] != '0' and asks[0][1] != '0'):
                        xd = {
                            'symbol': symbol,
                            'bid': float(Decimal(bids[0][0]).normalize()),
                            'ask': float(Decimal(asks[0][0]).normalize())
                        }
                        r = client.get(name)
                        if r is None or r.decode() == '':
                            data = {'timestamp': int(time.time()), 'data': [xd]}
                            client2.set(name, json.dumps(data))
                        else:
                            data = json.loads(r.decode())['data']
                            if symbol in [k['symbol'] for k in data]:
                                for d in data:
                                    if d['symbol'] == symbol:
                                        d.update({
                                            'bid': float(Decimal(bids[0][0]).normalize()),
                                            'ask': float(Decimal(asks[0][0]).normalize())
                                        })
                                        break
                            else:
                                data.append(xd)

                            data = {'timestamp': int(time.time()), 'data': data}
                            print(len(data['data']), data)
                            client2.set(name, json.dumps(data))
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

def on_error(error):
    error = gzip.decompress(error).decode()
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    def run(*args):
        symbs = filter
        data = []
        send_message(ws, data)

        for s in symbs:
            data.append({'event': 'addChannel', 'channel': 'ok_sub_spot_{}_depth_5'.format(s)})
            send_message(ws, data)

    t = threading.Thread(target=run, args=())
    t.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)
    client2.set(name, '')

    filter = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(filter))
    try:
        ws = websocket.WebSocketApp(
            "wss://real.okex.com:10441/websocket",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close
        )

        t3 = threading.Thread(target=filter_update, args=(ws, filter))
        t3.start()

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
