# import sys
# sys.path.append('/root/crystal')
import gzip
import json
import time
import threading
import zlib
import websocket
import traceback
from decimal import Decimal
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings

# settings
filter_check_timeout = 10

orderbook = {}
name = 'Okex'


def inflate(data):
    decompress = zlib.decompressobj(
        -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


def filter_update(ws, old_filter):
    try:
        while True:
            time.sleep(filter_check_timeout)
            fltr = json.loads(get_settings('filter')[0]['value'])[name]
            if fltr != old_filter:
                ws.close()
                global orderbook
                orderbook = {}
                break
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def push_to_memcache(*args):
    while True:
        try:
            client = Client(('127.0.0.1', 11211))
            while True:
                data = []
                # print(orderbook)
                _orderbook = orderbook.copy()
                for c in _orderbook:
                    data.append({
                        'symbol': c,
                        'bid': _orderbook[c]['bid'],
                        'ask': _orderbook[c]['ask']
                    })

                data = {'timestamp': int(time.time()), 'data': data}
                client.set(name, json.dumps(data))
                print(_orderbook.__len__(), _orderbook)
                # print(data)
                time.sleep(1)
        except RuntimeError:
            print(traceback.format_exc())
        except KeyboardInterrupt:
            pass
        except:
            log(traceback.format_exc())
            print(traceback.format_exc())
            time.sleep(1)


def send_message(ws, message_dict):
    data = json.dumps(message_dict).encode()
    ws.send(data)


def on_message(ws, message):
    try:
        message = inflate(message)
        channel = json.loads(message)[0]['channel']
        if channel != 'addChannel':
            symbol = (channel.split('_')[3] + channel.split('_')[4]).upper()
            data = json.loads(message)[0]['data']

            if 'asks' in data:
                if data['asks'] != [] and data['bids'] != []:
                    bids = data['bids']
                    asks = data['asks']
                    asks.reverse()

                    if (bids[0][1] != '0' and asks[0][1] != '0'):
                        xd = {
                            'bid': float(Decimal(bids[0][0]).normalize()),
                            'ask': float(Decimal(asks[0][0]).normalize())
                        }
                        orderbook[symbol] = xd
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(error):
    error = gzip.decompress(error).decode()
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    def run(*args):
        symbs = filter
        data = []
        send_message(ws, data)

        for s in symbs:
            data.append({'event': 'addChannel', 'channel': 'ok_sub_spot_{}_depth_5'.format(s)})
            send_message(ws, data)

    t = threading.Thread(target=run, args=())
    t.start()

    t2 = threading.Thread(target=push_to_memcache, args=())
    t2.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    filter = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(filter))
    try:
        ws = websocket.WebSocketApp(
            "wss://real.okex.com:10441/websocket",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close
        )

        t3 = threading.Thread(target=filter_update, args=(ws, filter))
        t3.start()

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
