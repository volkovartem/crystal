# import sys
# sys.path.append('/root/crystal')
import time
import json
import requests
import threading
import websocket
import traceback
from base64 import b64decode
from urllib.parse import quote_plus
from zlib import decompress, MAX_WBITS
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings


# settings
filter_check_timeout = 4

name = 'Bittrex'
client = Client(('127.0.0.1', 11211))
client2 = Client(('127.0.0.1', 11211))

filter = json.loads(get_settings('filter')[0]['value'])[name]
print(len(filter))

def filter_update(*args):
    try:
        while True:
            global filter
            global orderbook
            time.sleep(filter_check_timeout)
            new_filter = json.loads(get_settings('filter')[0]['value'])[name]
            if new_filter != filter:
                orderbook = {}
                filter = new_filter
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def process_message(message):
    try:
        deflated_msg = decompress(b64decode(message, validate=True), -MAX_WBITS)
    except SyntaxError:
        deflated_msg = decompress(b64decode(message, validate=True))
    return json.loads(deflated_msg.decode())

def on_message(ws, message):
    try:
        # print(message)
        message = json.loads(message)
        # print(message)
        if "M" in message and len(message['M']) > 0:
            message = message["M"][0]
            if 'A' in message and len(message['A']) > 0:
                message = message['A'][0]
                msg = process_message(message)
                if 'D' in msg:
                    # print(len(msg['D']), msg['D'])
                    for x in msg['D']:
                        symbol = x['M'].split('-')[1] + '_' + x['M'].split('-')[0]
                        # print(symbol)
                        if symbol in filter:
                            symbol = symbol.replace('_', '')
                            xd = {
                                'symbol': symbol,
                                'bid': float(x['B']),
                                'ask': float(x['A'])
                            }
                            r = client.get(name)
                            if r is None or r.decode() == '':
                                data = {'timestamp': int(time.time()), 'data': [xd]}
                                client2.set(name, json.dumps(data))
                            else:
                                data = json.loads(r.decode())['data']
                                if symbol in [k['symbol'] for k in data]:
                                    for d in data:
                                        if d['symbol'] == symbol:
                                            d.update({
                                                'bid': float(x['B']),
                                                'ask': float(x['A'])
                                            })
                                            break
                                else:
                                    data.append(xd)

                                data = {'timestamp': int(time.time()), 'data': data}
                                print(len(data['data']), data)
                                client2.set(name, json.dumps(data))
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("### opened ###")
    ws.send('{' + '"H":"c2","M":"SubscribeToSummaryDeltas","A":[],"I":0}')

    t3 = threading.Thread(target=filter_update, args=())
    t3.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)
    client2.set(name, '')

    try:
        r = requests.get(
            'https://socket.bittrex.com/signalr/negotiate?clientProtocol=1.5&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D&_={}'.format(
                time.time()))
        if r is not None:
            token = json.loads(r.text)['ConnectionToken']
            # print(token)
            token = quote_plus(token)
            # print(token)

            r = requests.get(
                'https://socket.bittrex.com/signalr/start?transport=webSockets&clientProtocol=1.5&connectionToken={}&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D&_={}'.format(
                    token, time.time()))
            if r is not None:
                print(r.text)

                ws = websocket.WebSocketApp(
                    "wss://socket.bittrex.com/signalr/connect?transport=webSockets&clientProtocol=1.5&connectionToken={}&connectionData=%5B%7B%22name%22%3A%22c2%22%7D%5D&tid=3".format(
                        token),
                    on_open=on_open,
                    on_message=on_message,
                    on_error=on_error,
                    on_close=on_close)

                ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
