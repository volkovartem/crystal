# import sys
# sys.path.append('/root/crystal')
import gzip
import json
import threading
import time
import traceback
import zlib

import websocket
from pymemcache.client.base import Client

from database.services.logging import log
from database.services.settings import get_settings

# settings
filter_check_timeout = 10

orderbook = {}
name = 'Okex'


def inflate(data):
    decompress = zlib.decompressobj(
        -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


def filter_update(ws, old_filter):
    try:
        while True:
            time.sleep(filter_check_timeout)
            fltr = json.loads(get_settings('filter')[0]['value'])[name]
            if fltr != old_filter:
                ws.close()
                global orderbook
                orderbook = {}
                break
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def push_to_memcache(*args):
    while True:
        try:
            client = Client(('127.0.0.1', 11211))
            while True:
                data = []
                # print(orderbook)
                _orderbook = orderbook.copy()
                for c in _orderbook:
                    # print(int(float(_orderbook[c]['timestamp']) / 1000), _orderbook[c])
                    data.append({
                        'symbol': c,
                        'asks': _orderbook[c]['asks'],
                        'bids': _orderbook[c]['bids'],
                        'timestamp': int(float(_orderbook[c]['timestamp']) / 1000)
                    })

                data = {'timestamp': int(time.time()), 'data': data}
                client.set(name, json.dumps(data))
                # print(_orderbook.__len__(), _orderbook)
                # print(len(data['data']), data)
                time.sleep(.2)
        except RuntimeError:
            print(traceback.format_exc())
        except KeyboardInterrupt:
            pass
        except:
            log(traceback.format_exc())
            print(traceback.format_exc())
            time.sleep(1)


def send_message(ws, message_dict):
    data = json.dumps(message_dict).encode()
    ws.send(data)


f = 0
s = 0
th = 0
tstart = time.time()
restart = 0


def on_message(ws, message):
    try:
        global f
        global s
        global th
        message = inflate(message)
        channel = json.loads(message)[0]['channel']
        if channel != 'addChannel':
            symbol = (channel.split('_')[3] + channel.split('_')[4]).upper()
            data = json.loads(message)[0]['data']

            orderbook[symbol] = data
            diff = round(time.time() - (float(data['timestamp']) / 1000), 2)
            if diff < 1:
                f += 1
            if 2 > diff > 1:
                s += 1
            if diff > 2:
                th += 1
            summ = f + s + th
            _f = int(f / summ * 100)
            _s = int(s / summ * 100)
            _th = int(th / summ * 100)

            print('diff', diff, 'stats', f, s, th, 'percent', _f, _s, _th)

            if time.time() > tstart + 30 and diff > 2:
                ws.close()

            # print('diff', diff, 'stats', f, s, th, '   ', float(data['timestamp']) / 1000, time.time())
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(error):
    error = gzip.decompress(error).decode()
    print(error)


def on_close(ws):
    print("### closed ###")
    global restart
    restart += 1


def on_open(ws):
    def run(*args):
        symbs = fltr
        data = []
        send_message(ws, data)
        # symbs = symbs[:120]
        print(len(symbs), symbs)

        for sr in symbs:
            data.append({'event': 'addChannel', 'channel': 'ok_sub_spot_{}_depth_10'.format(sr)})
        send_message(ws, data)

    t = threading.Thread(target=run, args=())
    t.start()

    t2 = threading.Thread(target=push_to_memcache, args=())
    t2.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    fltr = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(fltr))
    try:
        ws = websocket.WebSocketApp(
            "wss://real.okex.com:10441/websocket",
            on_open=on_open,
            on_message=on_message,
            on_error=on_error,
            on_close=on_close
        )

        tstart = time.time()

        t3 = threading.Thread(target=filter_update, args=(ws, fltr))
        t3.start()

        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
