# import sys
# sys.path.append('/root/crystal')
import time
import json
import threading
import websocket
import traceback
from decimal import Decimal
from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings

# settings
filter_update_timeout = 10

name = 'Binance'
client = Client(('127.0.0.1', 11211))
client2 = Client(('127.0.0.1', 11211))

#
# def filter_update(ws, old_filter):
#     try:
#         while True:
#             time.sleep(filter_update_timeout)
#             fltr = json.loads(get_settings('filter')[0]['value'])[name]
#             if fltr != old_filter:
#                 ws.close()
#                 global orderbook
#                 orderbook = {}
#                 break
#     except KeyboardInterrupt:
#         pass
#     except:
#         log(traceback.format_exc())
#         print(traceback.format_exc())


# def push_to_memcache(*args):
#     while True:
#         try:
#             while True:
#                 data = []
#                 # print(orderbook)pip in
#                 for c in orderbook:
#                     data.append({
#                         'symbol': c,
#                         'bid': orderbook[c]['bid'],
#                         'ask': orderbook[c]['ask']
#                     })
#
#                 data = {'timestamp': int(time.time()), 'data': data}
#                 client.set(name, json.dumps(data))
#                 # print(name, json.dumps(data))
#                 print(orderbook.__len__(), orderbook)
#                 time.sleep(1)
#         except RuntimeError:
#             print(traceback.format_exc())
#         except KeyboardInterrupt:
#             pass
#         except:
#             log(traceback.format_exc())
#             print(traceback.format_exc())
#             time.sleep(1)
tt = 0


def on_message(ws, message):
    try:
        global tt
        # print(message)
        data = json.loads(message)['data']
        symbol = json.loads(message)['stream'].split('@')[0].upper()
        client.set(symbol + '_' + name, json.dumps(data))
        # bids = data['bids']
        # asks = data['asks']
        # # print('asks', asks)
        # # print(bids)
        now = time.time()
        print(now - tt)
        tt = time.time()

        # print(bids, asks)
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print('open')
    # t2 = threading.Thread(target=push_to_memcache, args=())
    # # t2.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    fltr = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(fltr))
    client.set('filter_' + name, json.dumps(fltr))

    try:
        subs = ''
        for pair in fltr:
            subs += pair.replace('_', '').lower() + '@depth10/'
        # subs += 'LTC_BTC'.replace('_', '').lower() + '@depth5/'
        if subs != '':
            ws = websocket.WebSocketApp(
                "wss://stream.binance.com:9443/stream?streams={}".format(subs.strip('/')),
                # ws = websocket.WebSocketApp("wss://stream.binance.com:9443/ws/ltcbtc@aggTrade/ethbtc@aggTrade",
                on_open=on_open,
                on_message=on_message,
                on_error=on_error,
                on_close=on_close)

            # t3 = threading.Thread(target=filter_update, args=(ws, filter))
            # t3.start()
            #
            # t2 = threading.Thread(target=push_to_memcache, args=())
            # t2.start()

            ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
