# import sys
# sys.path.append('/root/crystal')
import gzip
import hashlib
import json
import threading
import time
import traceback
import zlib

import websocket

from database.services.api import get_api

from pymemcache.client.base import Client
from database.services.logging import log
from database.services.settings import get_settings

# settings
filter_check_timeout = 10

name = 'Okex'

api = get_api(name)


def inflate(data):
    decompress = zlib.decompressobj(
        -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


def send_message(ws, message_dict):
    data = json.dumps(message_dict).encode()
    ws.send(data)


client = Client(('127.0.0.1', 11211))


def on_message(ws, message):
    try:
        message = inflate(message)
        channel = json.loads(message)[0]['channel']
        if channel == 'ok_spot_userinfo':
            if bool(json.loads(message)[0]['data']['result']):
                balances = json.loads(message)[0]['data']['info']['funds']['free']
                xd = []
                for balance in balances:
                    xd.append({'token': balance.upper(), 'quantity': balances[balance]})
                print(len(xd), xd)
                client.set('OkexBalance', json.dumps(xd))
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


def on_error(error):
    error = gzip.decompress(error).decode()
    print(error)


def on_close(ws):
    print("### closed ###")


def buildMySign(params, secret_key):
    sign = ''
    for key in sorted(params.keys()):
        sign += key + '=' + str(params[key]) + '&'
    return hashlib.md5((sign + 'secret_key=' + secret_key).encode("utf-8")).hexdigest().upper()


def on_open(ws):
    def run(*args):
        data = []
        send_message(ws, data)

        _api_key = '92952754-87e2-41c7-b5d6-a27605b7e2fa'
        _secret_key = '1F87CF665C57E96FB2E05AA08D623D4F'

        params = {'api_key': _api_key}
        sign = buildMySign(params, _secret_key)
        # timestamp = '{0:.3f}'.format(time.time())

        while True:
            send_message(ws, {"event": "addChannel",
                              "channel": "ok_spot_userinfo",
                              "parameters": {"api_key": _api_key,
                                             "sign": sign}})
            time.sleep(.5)

    t = threading.Thread(target=run, args=())
    t.start()


while True:
    print('start websocket {}'.format(name))
    time.sleep(1)

    fltr = json.loads(get_settings('filter')[0]['value'])[name]
    print(len(fltr))

    try:
        host = "wss://real.okex.com:10440/ws/v1"
        ws = websocket.WebSocketApp(host,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close)
        ws.on_open = on_open
        ws.run_forever()
    except:
        print('ws{} Error: websocket failed'.format(name))
        print(traceback.format_exc())
