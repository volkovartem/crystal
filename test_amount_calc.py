from decimal import Decimal

_orderbook1 = {'lastUpdateId': 363226108,
               'bids': [['0.02608600', '1.02600000', []], ['0.02608200', '4.84000000', []],
                        ['0.02607600', '1.27900000', []], ['0.02606800', '10.00000000', []],
                        ['0.02606500', '122.72400000', []]],
               'asks': [['0.02608800', '0.41200000', []], ['0.02608900', '1.04300000', []],
                        ['0.02609900', '20.00000000', []], ['0.02610000', '198.04500000', []],
                        ['0.02610100', '0.19200000', []]]}

_orderbook2 = {'asks': [['0.02610759', '2'], ['0.02610999', '0.033627'], ['0.02611', '1.038'], ['0.02611094', '2'],
                        ['0.02611099', '2']],
               'bids': [['0.02609024', '4.202129'], ['0.02609023', '3'], ['0.02509013', '1.130874'],
                        ['0.02608893', '0.465036'], ['0.02608673', '11.232417']],
               'timestamp': 1544596562112}


def get_amount_dom(orderbook1, orderbook2):
    asks = []
    bids = []
    if Decimal(orderbook1['bids'][0][0]) > Decimal(orderbook2['asks'][0][0]):
        asks = orderbook2['asks']
        bids = orderbook1['bids']
    if Decimal(orderbook1['asks'][0][0]) < Decimal(orderbook2['bids'][0][0]):
        asks = orderbook1['asks']
        bids = orderbook2['bids']

    a = 0
    b = 0
    ask_price = 0
    bid_price = 0
    ask_amount = 0
    bid_amount = 0
    amount = 0

    while True:
        try:
            ask = Decimal(asks[a][0])
            bid = Decimal(bids[b][0])
            if 1 / ask * bid > 1:
                bid_price = Decimal(bids[b][0])
                ask_price = Decimal(asks[a][0])
                amount = min(ask_amount + Decimal(asks[a][1]), bid_amount + Decimal(bids[b][1]))

                if ask_amount + Decimal(asks[a][1]) > bid_amount + Decimal(bids[b][1]):
                    bid_amount += Decimal(bids[b][1])
                    b += 1
                else:
                    ask_amount += Decimal(asks[a][1])
                    a += 1
            else:
                break
        except IndexError as e:
            break
    return amount


print(get_amount_dom(_orderbook1, _orderbook2))
