import os
from routes import *
from flask import Flask
from database.database import db_session, init_db


app = Flask(__name__)
app.register_blueprint(routes)
app.secret_key = os.urandom(12)
init_db()

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if True:
    app.secret_key = os.urandom(12)
    app.run(debug=False, host='0.0.0.0', port=8080)
