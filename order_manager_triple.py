import time

import order_manager
import utils
import traceback
import trade_utils
import restWrapper as wp
from database.services.logging import *
from database.services.settings import get_settings


# settings
return_threshold = 'No threshold'
expiration_time = 20  # seconds


def check(data, mark, op):
    try:
        print(data, mark)
        exchange1 = list(data.keys())[0]
        exchange2 = list(data.keys())[1]
        exchange3 = list(data.keys())[2]
        amount = order_manager.get_amount(data, percent=False)

        # premovtrades = []
        # if amount is None:
        #     _amount = order_manager.get_amount(data, mode=True)
        #     trades = premoving.pre_moving(data, _amount)
        #     if trades is not None:
        #         premovtrades.append(trades)
        #         amount = order_manager.get_amount(data)



        print('Amount ', amount)
        print('----------------------------')

        if amount is not None:
            result1 = order_manager.simulation(exchange1, data[exchange1], amount, mark)
            if result1 is not None:
                print(result1)
                print('Amount ', result1[1])
                print('----------------------------')
                result2 = order_manager.simulation(exchange2, data[exchange2], result1[1], mark)
                if result2 is not None:
                    print(result2)
                    print('Amount ', result2[1])
                    print('----------------------------')
                    result3 = order_manager.simulation(exchange3, data[exchange3], result2[1], mark)

                    profit = utils.round_dwn(result3[1] - amount, 5)
                    print('profit', profit)
                    trade_fees = result1[2] + result2[2] + result3[2]
                    print('trade_fees', trade_fees)
                    result1[0].update(result2[0])
                    result1[0].update(result3[0])
                    print('real_prices', result1[0])
                    amounts = {exchange1: utils.round_dwn(amount, 5), exchange2: utils.round_dwn(result1[1], 5),
                               exchange3: utils.round_dwn(result2[1], 5)}
                    print('amounts', amounts)
                    wfees = order_manager.get_wfees(exchange1, data[exchange1][-1],
                                      exchange2, data[exchange2][-1],
                                      exchange3, data[exchange3][-1])
                    if profit > 0:
                        if trade_utils.is_relevant(op):
                            return [result1[0], utils.round_dwn(profit, 5), trade_fees, amounts, wfees]
                        else:
                            print_arb_log(['Arbitrage failed', 'Reason: Rejected. Opportunity expired'],
                                          mark)
                    else:
                        print_arb_log(['Arbitrage failed', 'Reason: Rejected. Expected profit is {}'.format(profit),
                                       'Withdrawal fees: {}'.format(wfees)], mark)
                else:
                    print_arb_log(
                        ['Arbitrage failed',
                         'Reason: {} {} {}'.format(exchange2, data[exchange2][0], 'Simulation failed')],
                        mark)
            else:
                print_arb_log(
                    ['Arbitrage failed', 'Reason: {} {} {}'.format(exchange1, data[exchange1][0], 'Simulation failed')],
                    mark)
        else:
            print_arb_log(
                ['Arbitrage failed', 'Reason: {} {} {}'.format(exchange1, data[exchange1][0], 'No balance'),
                 'Balance: {} {}'.format(utils.get_balance_fast(exchange1, data[exchange1][0]), data[exchange1][0])], mark)
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())


# print(wp.place_order('Okex', 'ETHUSDT', 'buy', 0.05947, 200.1859)) # Okex ETHUSDT buy 0.05947 200.1859
