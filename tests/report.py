import json


def makelines(data):
    module = data['module']
    simulation = data['checking']['simulation']
    lines = ['{};;;;;{};;;;;;'.format('INPUT CSV', 'SIMULATION'),
             '{};;;;;{};;;;;;'.format(str(module['chain']).replace(', ', ' > '),
                                      str(simulation['chain']).replace(', ', ' > ')),
             '{};{};{};{};;{};{};{};{};{};{};{}'.format('symbol', 'side', 'ask', 'bid', 'symbol', 'side',
                                                        'best', 'real', 'normalized', 'shift', 'time')]
    for x in range(len(data['module']['input'])):
        input = module['input']
        orderbook = simulation['orderbook']
        lines.append(
            '{};{};{};{};;{};{};{};{};{};{};{}'.format(input[x]['symbol'], input[x]['side'], input[x]['ask'],
                                                       input[x]['bid'], orderbook[x]['symbol'], orderbook[x]['side'],
                                                       orderbook[x]['best'], orderbook[x]['real'],
                                                       orderbook[x]['normalized'], orderbook[x]['shift'],
                                                       orderbook[x]['time']))
    lines.append(';;;;;Premoving start time {}'.format(data['checking']['premoving_time_start']))
    lines.append(';;;;;Premoving end time {}'.format(data['checking']['premoving_time_end']))
    lines.append(';;;;;Premoving time {} sec'.format(data['checking']['premoving_time']))
    lines.append(';;;;;Simulation time start {}'.format(simulation['time_start']))
    lines.append(';;;;;Simulation time end {}'.format(simulation['time_end']))
    lines.append(';;;;;Simulation time {} sec'.format(simulation['time']))
    lines.append('Return (without fees) {};;;;;Return (without fees) {}'.format(module['profit'], simulation['profit0']))
    lines.append(';;;;;Amount {}'.format(simulation['amount']))
    lines.append(';;;;;Trade fees {}%'.format(round(float(simulation['tradefees']) * 100, 2)))
    lines.append(';;;;;Actual profit {} (fees included)'.format(simulation['profit1']))
    lines.append(';;;;;;;;;;;')
    for f in lines:
        print(f)


# makelines(xdata)
with open('test_report.txt') as ff:
    content = ff.readlines()
    for x in content:
        # print(x)
        makelines(json.loads(x))

