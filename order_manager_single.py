import time
import traceback
import json
import order_manager
import restWrapper as wp
import premoving
import trade_utils
import utils
from database.services.arbitrage import *
from database.services.logging import *

# settings
return_threshold = 'No threshold'
timeout_status = 20  # seconds


def to_log_file(text):
    f = open("log.txt", "a+")
    f.write(text + '\n')


def calc_stake(trades):
    client = Client(('127.0.0.1', 11211))
    stake = 0
    for trade in trades:
        rm = client.get(trade['exchange'] + 'Markets')
        if rm is not None:
            rm = json.loads(rm.decode())
            smb = utils.split_symbol(trade['symbol'])
            stake += trade['amount'] * trade_utils.convert(trade['exchange'],
                                                           smb['s'] if trade['side'] == 'SELL' else smb['b'],
                                                           'USDT', rm)
    return round(stake, 5)


def statement(trades, amount, mark):
    opid = str(time.time())
    description = ('Pre transfer. Chain = ' + str(mark)).replace(
        "'", "").replace('[', '').replace(']', '').replace(', ', ' ')

    fees = 0
    for x in trades:
        fees = fees + float(trade_utils.get_trading_fees(x['exchange'])['buyFees'])

    stake = calc_stake(trades)

    _data = {'gross': '', 'stake': str(stake) + ' USDT', 'fees': str(round(fees, 2)) + '%',
             'wfees': '-', 'profit': '-', 'trades1': trades, 'trades2': []}

    status = 'completed' if amount is not None else 'failed'
    add_arbitrage(opid, description, json.dumps(_data), utils.get_time(), status)


def check_status(exchange, orderId, symbol):
    for x in range(timeout_status):
        status = wp.get_order_status(exchange, orderId, symbol)
        if status == 'closed' or status == 'filled':
            return True
        time.sleep(1)
    return False


def check(data, mark):
    try:
        # print(mark)
        exchange = list(data.keys())[0]
        amount = trade_utils.get_amount_wallet(exchange, data[exchange][0], limits=True, percent=True)
        minimum = trade_utils.get_trade_limits_fast_chain(exchange, data[exchange])
        if amount is not None and minimum > amount:
            amount = None

        if order_manager.allow_premoving(data) and amount is None:
            _amount = trade_utils.get_amount_limits(exchange, data[exchange][0])
            if _amount is not None:
                amount = max(float(_amount['amount_min']), float(minimum))

        # print('Amount ', amount)
        # print('----------------------------')

        if amount is not None:
            t = time.time()
            result = order_manager.simulation_v2(exchange, data[exchange], amount, mark, single=True)
            # print(time.time() - t, 'sec')
            # print('Amount ', amount)
            if result is not None:
                # print_arb_log(
                #     ['{} {} {} {} {} {}'.format('PROFIT', '{0:.10f}'.format(result[1]),
                #                                 str(data[exchange][0]),
                #                                 'Amount',
                #                                 str(utils.round_dwn(amount, 6)),
                #                                 str(data[exchange][0]))],
                #     str(data[exchange]))
                if result[1] > 0:
                    print(str(utils.get_time()), 'PROFIT', '{0:.10f}'.format(result[1]), data[exchange][0],
                          data[exchange], 'Amount {} (min {})'.format(str(utils.round_dwn(amount, 6)), minimum),
                          data[exchange][0], 'Simulation speed', '{0:.3f}'.format(time.time() - t), 'sec')
                else:
                    print(str(utils.get_time()), 'PROFIT', '{0:.10f}'.format(result[1]), data[exchange][0],
                          data[exchange], 'Amount {} (min {})'.format(str(utils.round_dwn(amount, 6)), minimum),
                          data[exchange][0], 'Simulation speed', '{0:.3f}'.format(time.time() - t), 'sec')

                if result[1] > 0:
                    to_log_file(
                        '{} {} {} {} {} {} {} {} {} {} {}'.format(str(utils.get_time()), 'PROFIT',
                                                                  '{0:.10f}'.format(result[1]),
                                                                  data[exchange][0],
                                                                  data[exchange], 'Amount (min {})'.format(minimum),
                                                                  str(utils.round_dwn(amount, 6)),
                                                                  data[exchange][0], 'Simulation speed',
                                                                  '{0:.3f}'.format(time.time() - t), 'sec'))
                    trades = premoving.pre_moving(data, {exchange: amount})
                    print('trades', trades)
                    if trades is not None:
                        amount = trade_utils.get_amount_wallet(exchange, data[exchange][0])
                        if amount is not None:
                            result.append(amount)
                            return result
                        if trades:
                            for trade in trades:
                                symbol = trade['symbol']
                                base = utils.split_symbol(symbol)['s']
                                quote = utils.split_symbol(symbol)['b']
                                utils.add_history(base)
                                utils.add_history(quote)
                            statement(trades, amount, mark)
                        else:
                            print('Pre moving is NOT required')
    except KeyboardInterrupt:
        pass
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())

# print(check({'Okex': ['ETH', 'USDT', 'NEO', 'BTC', 'ETH'], 'profit': '1.00691'},
#       str({'Okex': ['ETH', 'USDT', 'NEO', 'BTC', 'ETH'], 'profit': '1.00691'})))
