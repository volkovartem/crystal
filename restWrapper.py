import traceback
from rest import restBinance as binance, restBittrex as bittrex, restHitbtc as hitbtc, restOkex_v3 as okex


def get_exchange(exchange):
    if exchange == 'Binance':
        return binance
    if exchange == 'Okex':
        return okex
    if exchange == 'Bittrex':
        return bittrex
    if exchange == 'Hitbtc':
        return hitbtc


def connect_api(exchange):
    try:
        return get_exchange(exchange).connect_api()
    except:
        print(traceback.format_exc())


def get_the_price(exchange, symbol):
    try:
        return get_exchange(exchange).get_the_price(symbol)
    except:
        print(traceback.format_exc())


def get_price(exchange, symbol):
    try:
        return get_exchange(exchange).get_price(symbol)
    except:
        print(traceback.format_exc())


def get_24hVolume(exchange):
    try:
        return get_exchange(exchange).get_24hVolume()
    except:
        print(traceback.format_exc())


def get_balance(exchange, token=''):
    try:
        return get_exchange(exchange).get_balance(token)
    except:
        print(traceback.format_exc())


def get_order_book(exchange, symbol):
    try:
        return get_exchange(exchange).get_order_book(symbol)
    except:
        print(traceback.format_exc())


def get_fee(exchange, token):
    try:
        return get_exchange(exchange).get_fee(token)
    except:
        print(traceback.format_exc())


def place_order(exchange, symbol, side, amount, price):
    try:
        return get_exchange(exchange).place_order(symbol, side, amount, price)
    except:
        print(traceback.format_exc())


def get_order_status(exchange, id, symbol):
    try:
        return get_exchange(exchange).get_order_status(id, symbol)
    except:
        print(traceback.format_exc())


def get_health_wallets(exchange):
    try:
        return get_exchange(exchange).get_health_wallets()
    except:
        print(traceback.format_exc())


def transfer(exchange, amount, fromToken, toToken):
    try:
        return get_exchange(exchange).transfer(amount, fromToken, toToken)
    except:
        print(traceback.format_exc())


def withdraw(amount, token, fromExch, toExch):
    try:
        import time
        address = get_exchange(toExch).get_wallet_address(token)
        if address is not None:
            return get_exchange(fromExch).withdraw(token, amount, address)
    except:
        print(traceback.format_exc())


def get_markets(exchange):
    try:
        return get_exchange(exchange).get_markets()
    except:
        print(traceback.format_exc())


def cancel_order(exchange, id, symbol):
    try:
        return get_exchange(exchange).cancel_order(id, symbol)
    except:
        print(traceback.format_exc())
