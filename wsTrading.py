import json
import time
from pymemcache.client.base import Client

client = Client(('127.0.0.1', 11211))


def open_market_order(symbol, side, amount):
    request_id = int(time.time())
    request = {'id': request_id,
               'action': 'open_market_order',
               'data': {'symbol': symbol,
                        'side': side,
                        'amount': amount},
               'result': {}}
    client.set('OkexTrading', json.dumps(request))
    timeout = time.time()
    while time.time() < timeout + 20:
        r = client.get('OkexTrading')
        if r is not None:
            r = json.loads(r.decode())
            if r['result']:
                return r['result']
        time.sleep(.1)


# t = time.time()
# print(open_market_order('YOUUSDT', 'buy', 0.00027))
# print(time.time() - t)
# t = time.time()
# print(open_market_order('PRAUSDT', 'sell', 40))
# print(time.time() - t)

