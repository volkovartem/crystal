import utils
import traceback
import trade_utils
import restWrapper as wp
from operator import itemgetter
from database.services.logging import *
from database.services.settings import *
from database.services.transfers import *
from database.services.arbitrage import *
from database.services.withdrawal import *
from database.services.settings import get_settings


def execute_chain(w, chain):
    exchange = w['exchange']
    opid = str(time.time())
    description = ('Move to preferred. "' + exchange + '" Chain = ' + str(chain)).replace("'", "").replace('[', '').replace(']', '').replace(', ', ' ')
    amount = float(utils.get_balance_fast(exchange, w['token']))
    stake = '{} {}'.format(utils.norm(amount), chain[0])
    fee = trade_utils.get_trading_fees(w['exchange'])['buyFees']
    fees = float(fee) * len(chain) - 1 if fee is not None else 0
    trades = []
    result = [True]

    for i in range(len(chain) - 1):
        pair = utils.normalize_pair(chain[i], chain[i + 1])
        symbol = pair[1] + pair[2]
        side = pair[0]
        balance = float(utils.get_balance_fast(exchange, chain[i + 1]))
        print('wp.Transfer', exchange, amount, chain[i], chain[i + 1])

        info = wp.transfer(exchange, amount, chain[i], chain[i + 1])

        orderTime = utils.get_time()
        if info is not None and 'Error' not in info and 'code' not in info:
            orderId = info['order_id'] if exchange == 'Okex' else info['id']
            print_arb_log(['Order {} is opened'.format(side.upper()), 'ID {}'.format(orderId)], description)

            res = False
            status = info['status'].lower() if 'status' in info else 'okex'
            trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': amount,
                           'side': side.upper(), 'status': status, 'time': orderTime})

            if exchange == 'Okex':
                status = wp.get_order_status(exchange, orderId, symbol)

            if status == 'closed' or status == 'filled':
                res = True
                new_balance = wp.get_balance(exchange, chain[i + 1])
                balance = new_balance - balance
                amount = balance
            if res is False:
                result[0] = False
                print_arb_log(['Moving failed', 'Reason: order is not closed'], description)
        else:
            mess = ''
            # if orderId is not None and 'Error' in orderId and 'code' not in orderId:
            trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': info, 'amount': amount,
                           'side': side.upper(), 'status': 'failed', 'time': orderTime})
            result[0] = False
            print_arb_log(['Moving failed', 'Reason: order is not opened', mess], description)

    status = 'completed' if result[0] else 'failed'
    _data = {'gross': '', 'stake': stake, 'fees': str(fees) + '%',
             'wfees': '-', 'profit': '-', 'trades1': trades, 'trades2': []}

    add_arbitrage(opid, description, json.dumps(_data), utils.get_time(), status)


def get_best_chain(exchange, chains):
    maxquantity = 0
    topchain = []
    if len(chains) == 1:
        return chains[0]
    for chain in chains:
        pair = utils.normalize_pair(chain[0], chain[1])
        orderbook = wp.get_order_book(exchange, pair[1] + pair[2])
        if orderbook is not None:
            quantity = float(orderbook['bids'][0][1] if pair[0] == 'sell' else orderbook['asks'][0][1])
            if float(quantity) > maxquantity:
                maxquantity = quantity
                topchain = chain
    return topchain


def withdraw_all(w):
    coins = json.loads(get_settings('coins')[0]['value'])
    pref_coins = []
    for c in coins:
        if bool(c['preferred']):
            pref_coins.append(c['coin'])
    if pref_coins != [] and w['token'] not in pref_coins:
        chains = []
        for pc in pref_coins:
            chain = trade_utils.make_achain(w['exchange'], w['token'], pc)
            if chain is not None:
                chains.append(chain)
        best_chain = get_best_chain(w['exchange'], chains)
        if best_chain is not None and best_chain != []:
            execute_chain(w, best_chain)


def compare(withdrawals, a):
    withdrawals = sorted(withdrawals, key=itemgetter('timestamp'), reverse=True)
    startexch = list(a.keys())[0]
    endexch = list(a.keys())[1]
    startnode = a[startexch][0]
    endnode = a[endexch][0]

    result = {startexch: {'flag': False, 'id': 0},
              endexch: {'flag': False, 'id': 0}}

    # check for start exchange
    for w in withdrawals:
        if startnode == w['token'] and startexch != w['exchange']:
            result.update({startexch: {'flag': True, 'id': w['id']}})

    if result[startexch]['flag']:
        # check for end exchange
        for w in withdrawals:
            if endnode == w['token'] and endexch != w['exchange']:
                result.update({endexch: {'flag': True, 'id': w['id']}})

    return result

# wp.transfer('Okex', 0.15, 'ETH', 'LTC')
# withdraw_all(get_withdrawals()[-1])

while True:
    print('Wallet manager started')
    try:
        client = Client(('127.0.0.1', 11211))
        while True:
            withdrawals = get_withdrawals()
            arbitrage = client.get('Arbitrage')
            if arbitrage is not None:
                arbitrage = json.loads(arbitrage)
                arbitrage = [arbitrage[a] for a in arbitrage]
                arbitrage = sorted(arbitrage, key=itemgetter('profit'), reverse=True)
                for arb in arbitrage:
                    res = compare(withdrawals, arb)
                    dest1 = list(arb.keys())[0]
                    dest2 = list(arb.keys())[1]
                    if res[dest1]['flag'] and res[dest2]['flag']:
                        id1 = res[dest1]['id']
                        w1 = dict((item['id'], item) for item in withdrawals)[id1]
                        amount1 = float(w1['amount'])
                        prov1 = w1['exchange']
                        token1 = w1['token']

                        id2 = res[dest2]['id']
                        w2 = dict((item['id']   , item) for item in withdrawals)[id2]
                        amount2 = float(w2['amount'])
                        prov2 = w2['exchange']
                        token2 = w2['token']

                        print('Withdraw {} {} from {} to {}'.format(amount1, token1, prov1, dest1))
                        print('Withdraw {} {} from {} to {}'.format(amount2, token2, prov2, dest2))
                        s1 = wp.withdraw(amount1, token1, prov1, dest1)
                        s2 = wp.withdraw(amount2, token2, prov2, dest2)

                        status1 = 'done' if s1 is not None else 'failed'
                        status2 = 'done' if s2 is not None else 'failed'

                        add_transfer(prov1 + ' to ' + dest1, token1, amount1, s1['id'], utils.get_time(), status1)
                        add_transfer(prov2 + ' to ' + dest2, token2, amount2, s2['id'], utils.get_time(), status2)

                        remove_withdrawal(id1)
                        remove_withdrawal(id2)
                        withdrawals = get_withdrawals()

            limit = get_settings('deposit_expiration')

            for withdrawal in withdrawals:
                ts = float(withdrawal['timestamp'])
                if time.time() - ts > float(limit) * 3600:
                    # withdrawal all to preferred coin inside this exchange
                    withdraw_all(withdrawal)
            time.sleep(1)
    except KeyboardInterrupt:
        break
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())
        time.sleep(1)
