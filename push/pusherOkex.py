# import sys
# sys.path.append('/root/crystal')
import time
import json
import traceback
from rest import restOkex_v3 as okex
from pymemcache.client.base import Client
from database.services.logging import log


#settings
to_balance = 2.2
t_balance = time.time() - to_balance
to_fees = 150
t_fees = time.time() - to_fees
to_hw = 60
t_hw = time.time() - to_hw
to_load_markets = 320
t_load_markets = time.time() - to_load_markets
to_limits = 700
t_limits = time.time() - to_limits

name = 'Okex'


def push_balance(client):
    global t_balance
    if time.time() - t_balance > to_balance:
        balances = okex.get_balance()
        if balances is not None:
            client.set('OkexBalance', json.dumps(balances))
            print('balances', len(balances), balances)
        t_balance = time.time()


def push_fees(client):
    global t_fees
    if time.time() - t_fees > to_fees:
        fees = okex.get_fee()
        if fees is not None:
            client.set('OkexFees', json.dumps(fees))
            print('fees', len(fees), fees)
        t_fees = time.time()


def push_health_wallets(client):
    global t_hw
    if time.time() - t_hw > to_hw:
        hw = okex.get_health_wallets()
        if hw is not None:
            client.set('OkexHW', json.dumps(hw))
            print('hw', len(hw), hw)
        t_hw = time.time()


def push_markets(client):
    global t_load_markets
    if time.time() - t_load_markets > to_load_markets:
        markets = okex.get_markets()
        if markets is not None:
            client.set('OkexMarkets', json.dumps(markets))
            print('markets', len(markets), markets)
        t_load_markets = time.time()


def push_limits(client):
    global t_limits
    if time.time() - t_limits > to_limits:
        limits = okex.get_trade_limits()
        if limits is not None:
            client.set(name + 'Limits', json.dumps(limits))
            print('limits', len(limits), limits)
        t_limits = time.time()


while True:
    try:
        client = Client(('127.0.0.1', 11211))
        # push_balance(client)
        push_fees(client)
        push_health_wallets(client)
        push_markets(client)
        push_limits(client)
        time.sleep(0.01)
    except KeyboardInterrupt:
        break
    except:
        log(traceback.format_exc())
        print(traceback.format_exc())
        time.sleep(5)
