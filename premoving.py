import math
import time
import restWrapper as wp
import trade_utils
import utils
from database.services.logging import *
from database.services.settings import get_settings

# settings

# n_density = 0  # %
# n_frequency = 0
# n_time = 10  # minutes

premoving = json.loads(get_settings('premoving')[0]['value'])
n_density = float(premoving['density'])
n_frequency = float(premoving['frequency'])
n_time = float(premoving['time'])


# print(n_density, n_frequency, n_time)


# def calc_frequency(history, token):


def get_trade_lim(lims, token, exchange):
    for x in lims:
        if token == 'USDT' and x['symbol'] == 'BTC_USDT':
            price = wp.get_price(exchange, 'BTCUSDT')['bid']
            if price is not None:
                return round(x['market'] * float(price), 6)
        # symbol = utils.normalize_pair()
        symbol = x['symbol'].split('_')[0]
        if symbol == token:
            # print(x)
            return x['market']


# print(get_trade_lim(utils.get_trade_limits_fast('Binance'), 'USDT', 'Binance'))


def find_coin(client, exchange, token, amount):
    wallets = trade_utils.get_available_balance2(exchange)
    slippage = 3
    # print(wallets)
    # amount, chain[]
    print('amount', amount)
    rm = client.get(exchange + 'Markets')
    if rm is not None:
        rm = json.loads(rm.decode())
        res_chains = []
        balances = []
        for w in wallets:
            # print(exchange)
            if w['token'] != token:
                coeff = trade_utils.convert(exchange, w['token'], token, rm, False)
                if coeff is not None:
                    balances.append({'token': w['token'], 'balance': w['quantity'],
                                     'balance{}'.format(token): utils.round_dwn(float(w['quantity']) * coeff, 6)})
        # print('balances', balances)
        balances = sorted(balances, key=lambda k: k['balance{}'.format(token)])
        balances.reverse()
        # print('balances', balances)

        # print('amount', amount)
        amount = amount * ((slippage + 100) / 100)
        # print('amount * slippage ({}%)'.format(slippage), amount)
        minLims = utils.get_trade_limits_fast(exchange)
        xamount = amount
        sum = 0
        flag = False
        for balance in balances:
            # print(exchange, balance['token'])
            limin = get_trade_lim(minLims, balance['token'], exchange)
            # print('limin', limin, balance['token'])
            # print(exchange, float(balance['balance']), limin, balance['token'])
            if float(balance['balance']) > limin:
                chain = trade_utils.make_achain(exchange, balance['token'], token, rm)
                # print('chain', chain)
                if chain is not None:
                    minimal_amount = trade_utils.get_trade_limits_fast_chain(exchange, chain)
                    # coeff = trade_utils.calc_coeff_chain(exchange, chain)
                    # print('coeff chain', chain, coeff)

                    # print('minimal_amount', minimal_amount)
                    # print('float(balance[balance]) > minimal_amount', float(balance['balance']), minimal_amount)
                    if minimal_amount is not None and float(balance['balance']) > minimal_amount:
                        # print(float(balance['balance']), balance['token'], 'min', minimal_amount, chain[0])
                        # print('chain', len(chain), chain)
                        fee = float(trade_utils.get_trading_fees(exchange)['buyFees'])
                        fees = 1 + ((len(chain) - 1) * fee / 100)
                        # print(fees)
                        xamount = round(amount * fees, 6)
                        # print('xamount', xamount)
                        # print('xamount coeff', round(xamount, 6))
                        # print(min(balance['balance{}'.format(token)], xamount))
                        sum = sum + min(balance['balance{}'.format(token)], xamount)
                        res_chains.append({'exchange': exchange,
                                           'amount': utils.round_dwn(min(balance['balance{}'.format(token)], xamount),
                                                                     6),
                                           'chain': chain})
                        if sum < xamount:
                            print('SUM not enough yet', sum, xamount, token)
                        if sum >= xamount:
                            print('SUM enough', sum, xamount, token)
                            flag = True
                            break
        if sum < xamount:
            mark = 'Pre transfer failed. {} Required amount: {} {}'.format(exchange, round(xamount, 6), token)
            print_arb_log(['Reason: {}'.format('Not enough balance'),
                           'Available amount: {} {}'.format(sum, token)], mark)
        if flag:
            return res_chains


def calc_param(history, token, type):
    refreshes = 0
    sum = 0
    number_of_chains = 0
    for item in history:
        flag = False
        for x in item['chains']:
            chain = item['chains'][x]
            nodes = []
            for e in chain:
                if e != 'profit':
                    nodes.append(chain[e][0])
                    nodes.append(chain[e][-1])
            number_of_chains = number_of_chains + 1
            if token in nodes:
                sum = sum + 1
                if len(chain) - 1 == type:
                    flag = True
        if flag:
            refreshes = refreshes + 1
    if number_of_chains > 0:
        return [round((sum / number_of_chains) * 100, 1), refreshes]


def check_condition(client, require_amounts, history):
    result = []
    for ra in require_amounts:
        if ra['balance'] < 0:
            params = calc_param(history, ra['token'], len(require_amounts))
            if params is not None:
                density = params[0]
                frequency = params[1]
                # print(params)
                # print('N params:', n_density, n_frequency, n_time)
                if density >= n_density and frequency >= n_frequency:
                    amount = math.sqrt(math.pow(ra['balance'], 2))
                    limits = trade_utils.get_amount_limits(ra['exchange'], ra['token'])
                    # min_lim = utils.get_trade_limits_fast(ra['exchange'])
                    amount = max(amount, float(limits['amount_min']))
                    chains = find_coin(client, ra['exchange'], ra['token'], amount)
                    # print('chains', chains, ra['exchange'], ra['token'], amount)
                    if chains is not None:
                        result.append(chains)
                    else:
                        return
                else:
                    print('conditions not meet')
                    return
            else:
                return
    return result


def execute_chain(data):
    slippage = 2  # %
    # print('execute_chains', data)
    exchange = data['exchange']
    chain = data['chain']
    coeff = trade_utils.calc_coeff_chain(exchange, chain)
    if coeff is not None and coeff > 0:
        amount = data['amount'] / coeff

        minimum = trade_utils.get_trade_limits_fast_chain(exchange, chain) * ((slippage + 100) / 100)
        # print('minimum', minimum)
        cur_balance = float(utils.get_balance_fast(exchange, chain[0]))
        # print('cur_balance', cur_balance)
        amount = min(max(minimum, amount), cur_balance)
        # print('amount', amount)

        description = ('Pre transfer. "' + exchange + '" Chain = ' + str(chain)).replace("'", "").replace('[',
                                                                                                          '').replace(
            ']', '').replace(', ', ' ')
        trades = []
        result = [True]

        # print(exchange, chain, 'amount', amount, fee)
        # return []

        for i in range(len(chain) - 1):
            pair = utils.normalize_pair(chain[i], chain[i + 1])
            symbol = pair[1] + pair[2]
            side = pair[0]
            balance = float(utils.get_balance_fast(exchange, chain[i + 1]))
            print('wp.Transfer', exchange, amount, chain[i], chain[i + 1])

            info = wp.transfer(exchange, amount, chain[i], chain[i + 1])
            print('info', info)
            # {'symbol': 'LTC_ETH', 'balances':
            # {'ETH': 0.0012141218362616, 'LTC': 0.0468210985},
            # 'status': 'filled', 'order_id': 1891924085835776, 'side': 'buy'}

            orderTime = utils.get_time()
            if info is not None and info['status'] != 'failed':
                orderId = info['order_id']
                print_arb_log(['Order {} is opened'.format(side.upper()), 'ID {}'.format(orderId)], description)

                res = False
                status = info['status']
                trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': orderId, 'amount': round(amount, 6),
                               'side': side.upper(), 'status': status, 'time': orderTime})

                if status == 'filled':
                    res = True
                    if chain[i + 1] in info['balances']:
                        new_balance = float(info['balances'][chain[i + 1]])
                    else:
                        new_balance = float(utils.get_balance_fast(exchange, chain[i + 1]))
                    print('premoving Balance', balance, 'new balance', new_balance)
                    balance = new_balance - balance
                    amount = balance
                if res is False:
                    result[0] = False
                    print_arb_log(['Pre transfer failed', 'Reason: status {}'.format(status)], description)
                    return trades
            else:
                trades.append({'exchange': exchange, 'symbol': symbol, 'orderId': '-', 'amount': round(amount, 6),
                               'side': side.upper(), 'status': 'failed', 'time': orderTime})
                result[0] = False
                print_arb_log(
                    ['Pre transfer failed', 'Reason: order failed'.format(str(info))],
                    description)
                return trades
        return trades


def execute(data):
    print('execute', data)
    data1st = data[0]
    trades1 = []
    trades2 = []
    for x in data1st:
        tt = execute_chain(x)
        if tt is not None:
            for _t in tt:
                trades1.append(_t)
    if len(data) == 2:
        data2nd = data[1]
        for x in data2nd:
            tt = execute_chain(x)
            if tt is not None:
                for _t in tt:
                    trades2.append(_t)
    # print(trades1, trades2)
    return trades1 + trades2


def get_require_amounts(data, amounts):
    require_amounts = []
    for exchange in list(data.keys())[:-1]:
        chain = data[exchange]
        balance = utils.get_balance_fast(exchange, chain[0])
        if balance is None:
            return
        require_amounts.append({'exchange': exchange, 'token': chain[0],
                                'balance': utils.round_dwn(float(balance) - amounts[exchange], 6)})
    return require_amounts


def pre_moving(data, amounts):
    # print(data)
    client = Client(('127.0.0.1', 11211))
    history = client.get('UpdatesHistory')
    require_amounts = get_require_amounts(data, amounts)
    print('require_amounts', require_amounts)

    if history is not None:
        # print('history')
        history = json.loads(history)
        result = check_condition(client, require_amounts, history)
        print('check_condition', result)
        if result is None:
            print('Pre moving is not possible')
            return
        if not result:
            print('Pre moving is not necessary.')
            return result
        if result is not None:
            print('execution')
            # return result
            # tt = time.time()
            res = execute(result)
            # print('execution time', time.time() - tt)
            return res

# data = {'Okex': ['LTC', 'ETH', 'USDT', 'NEO', 'BTC', 'LTC'], 'profit': '1.00691'}
# data = {'Okex': ['ETH', 'LTC', 'USDT', 'NEO', 'BTC', 'LSK', 'ETH'], 'profit': '1.00691'}
# data = {'Okex': ['ETH', 'VEE'], 'Bittrex': ['VEE', 'BTC', 'GNT', 'ETH'], 'profit': '1.05130'}
# data = {'Okex': ['ETH', 'VEE'], 'Bittrex': ['VEE', 'BTC'], 'Binance': ['BTC', 'CVC', 'ETH'], 'profit': '1.05139'}
# data = {'Binance': ['ETH', 'PIVX'], 'profit': '1.01028'}
# amounts = {'Okex': 0.05}  # LTC
# amounts = {'Okex': 0.012}  # ETH
# t = time.time()
# print(pre_moving(data, amounts))  # return list of trades
# print('premoving time', time.time() - t)
